---
linkTitle: REST API
title: Quickstart for GitCode REST API
weight: 10
sidebar:
  open: false
---

{{< cards >}}
  {{< card url="guide" title="Quickstart" icon="cursor-arrow-ripple" >}}
  {{< card url="users" title="Users" icon="user-circle" >}}
  {{< card url="orgs" title="Organizations" icon="user-group" >}}
  {{< card url="repos/" title="Repositories" icon="briefcase" >}}
  {{< card url="repos/issues" title="Issues" icon="question-mark-circle" >}}
  {{< card url="repos/pulls" title="Pull Requests" icon="arrow-path-rounded-square" >}}
  {{< card url="repos/labels" title="Labels" icon="tag" >}}
  {{< card url="repos/milestons" title="Milestones" icon="map" >}}
  {{< card url="repos/webhooks" title="Webhooks" icon="share" >}}
  {{< card url="oauth" title="OAuth2.0" icon="key" >}}
{{< /cards >}}
