---
title: 'GitCode Help Docs'
date: 2024-12-02
type: landing

design:
  # Default section spacing
  spacing: "6rem"

sections:
  - block: hero
    content:
      title: GitCode Help Docs
      text: We are always here to assist you and address any questions you may have while using GitCode.
      primary_action:
        text: Api Docs
        url: /en/docs
        icon: rocket-launch
    #   secondary_action:
    #     text: API 文档
    #     url: /docs/openapi/
      # announcement:
      #   text: "Announcing the release of version 2."
      #   link:
      #     text: "Read more"
      #     url: "/blog/"
    design:
      spacing:
        padding: [0, 0, 0, 0]
        margin: [0, 0, 0, 0]
      # For full-screen, add `min-h-screen` below
      css_class: ""
      background:
        color: ""
        image:
          # Add your image background to `assets/media/`.
          filename: ""
          filters:
            brightness: 0.5
  - block: stats
    content:
      items:
        - statistic: "3,750 k+"
          description: |
            Users  
        - statistic: "285,000+"
          description: |
            OpenSource Repo  
        - statistic: "4,200+"
          description: |
            Organization
    design:
      # Section background color (CSS class)
      css_class: "bg-gray-100 dark:bg-gray-800"
      # Reduce spacing
      spacing:
        padding: ["1rem", 0, "1rem", 0]
  - block: features
    id: features
    content:
      title: GitCode — Home for your Code
      text: Whether you are a beginner or an experienced developer, no matter the scale of your project, GitCode is your ideal choice—offering an efficient, convenient, and user-friendly home for your code.
      items:
        - name: Publish and Share Code
          icon: magnifying-glass
          description: Create a new repository on the website, submit your code to it, and share the link to allow others to access your code.
        - name: Engage in Discussions and Collaboration
          icon: bolt
          description: Provide feedback on code issues or ideas, stay updated on the latest activities of open-source organizations, and explore recommended content.
        - name: Collaborative Programming
          icon: sparkles
          description: Contribute to others’ projects, work together to solve problems or develop new features, and submit your changes and suggestions through Pull Requests.
        # - name: No-Code
        #   icon: code-bracket
        #   description: Edit and design your site just using rich text (Markdown) and configurable YAML parameters.
        - name: Read and Learn
          icon: star
          description: Search for projects or repositories of interest, explore their source code, documentation, and contributor information.
        # - name: Swappable Blocks
        #   icon: rectangle-group
        #   description: Build your pages with blocks - no coding required!
---
