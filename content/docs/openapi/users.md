---
linkTitle: Users
title: 用户账号API文档
weight: 2
sidebar:
  open: false
---

## 1. 获取一个用户

### 请求

`GET https://api.gitcode.com/api/v5/users/{username}`

### 参数

| 参数名       | 描述             | 类型  | 数据类型 |
| ------------ | ---------------- | ----- | -------- |
| access_token | 用户授权码       | query | string   |
| username\*   | 用户名(username) | path  | string   |

### 响应

```json
{
  "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
  "followers_url": "https://api.gitcode.com/api/v5/users/dengmengmian/followers",
  "html_url": "https://gitcode.com/dengmengmian",
  "id": "650d67fbae6d795139b49b41",
  "login": "dengmengmian",
  "name": "麻凡",
  "type": "User",
  "url": "https://api.gitcode.com/api/v5/dengmengmian",
  "bio": "Nacos是由阿里巴巴开源的服务治理中间件，集成了动态服务发现、配置管理和服务元数据管理功能，广泛应用于微服务架构中，简化服务治理过程。",
  "blog": "https://www.dengmengmian.com",
  "company": "开发者",
  "email": "my@dengmengmian.com",
  "followers": 0,
  "following": 6,
  "top_languages": ["Python", "Shell"]
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/users/dengmengmian' \
--header 'Authorization: Bearer {your-token}'
```

## 2. 获取授权用户的资料

### 请求

`GET https://api.gitcode.com/api/v5/user`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |

### 响应

```json
{
  "avatar_url": "https://cdn-img.gitcode.com/fa/be/2fa2be6d3ffd01599dbc0a3c71ee9ec4cadb82f63a7a8489187645064ad95e59.png?time=1694709764757",
  "followers_url": "https://api.gitcode.com/api/v5/users/gitcode-xxm/followers",
  "html_url": "https://gitcode.com/gitcode-xxm",
  "id": "64e5ed8f7e20aa73efcbc302",
  "login": "gitcode-xxm",
  "name": "xxm",
  "type": "User",
  "url": "https://api.gitcode.com/api/v5/gitcode-xxm",
  "bio": "a PM ",
  "blog": "https://gitcode.com",
  "company": "",
  "email": "xiongjiamu@163.com",
  "followers": 8,
  "following": 35,
  "top_languages": ["Python", "Markdown", "C++", "C", "HTML"]
}
```

### DEMO

```bash
curl --location 'https://api.gitcode.com/api/v5/user' \
--header 'Authorization: Bearer {your-token}'
```

## 3. 获取授权用户的全部邮箱

### 请求

`GET https://api.gitcode.com/api/v5/emails`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |

### 响应

```json
[
  {
    "email": "my@dengmengmian.com",
    "state": "confirmed"
  },
  {
    "email": "xxxx@qq.com",
    "state": "confirmed"
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/emails?access_token={your-token}' \
```

## 4. 获取用户个人动态

### 请求

`GET https://api.gitcode.com/api/v5/users/{username}/events`

### 参数

| 参数名         | 描述           | 类型  | 数据类型 |
| -------------- | -------------- | ----- | -------- |
| username       | 用户名         | path  | string   |
| access_token\* | 用户授权码     | query | string   |
| year           | 开始年（2024） | query | string   |
| next           | 结束日期       | query | string   |

### 响应

```json
{
  "events": {
    "2024-08-27": [
      {
        "action": 5,
        "action_name": "pushed to",
        "author": {
          "id": 704,
          "iam_id": "5c340cab034d455992541f00f9936fb4",
          "username": "dengmengmian",
          "state": "active",
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
          "email": "",
          "name": "dengmengmian",
          "name_cn": "dengmengmian",
          "web_url": "https://test.gitcode.net/dengmengmian"
        },
        "author_id": 704,
        "author_username": "dengmengmian",
        "created_at": "2024-08-27T10:34:05.093Z",
        "project": {
          "main_repository_language": [null, null],
          "star_count": 0,
          "forks_count": 0,
          "develop_mode": "normal",
          "stared": false
        },
        "project_id": 507167,
        "project_name": "mactribe/midsommarcartoon",
        "push_data": {
          "commit_count": 1,
          "action": "pushed",
          "ref_type": "branch",
          "commit_from": "2ce472fec073f77804c3480ccf128219a6172e54",
          "commit_to": "14b742fe434797fb073ba536804011f735f2f430",
          "ref": "main",
          "commit_title": "文件title"
        },
        "_links": {
          "project": "https://test.gitcode.net/mactribe/midsommarcartoon",
          "action_type": ""
        }
      },
      {
        "action": 5,
        "action_name": "pushed to",
        "author": {
          "id": 704,
          "iam_id": "5c340cab034d455992541f00f9936fb4",
          "username": "dengmengmian",
          "state": "active",
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
          "email": "",
          "name": "dengmengmian",
          "name_cn": "dengmengmian",
          "web_url": "https://test.gitcode.net/dengmengmian"
        },
        "author_id": 704,
        "author_username": "dengmengmian",
        "created_at": "2024-08-27T10:31:17.494Z",
        "project": {
          "main_repository_language": [null, null],
          "star_count": 0,
          "forks_count": 0,
          "develop_mode": "normal",
          "stared": false
        },
        "project_id": 507167,
        "project_name": "mactribe/midsommarcartoon",
        "push_data": {
          "commit_count": 1,
          "action": "pushed",
          "ref_type": "branch",
          "commit_from": "ee25b0353dae9bf19f5e3e733e651e7870020386",
          "commit_to": "2ce472fec073f77804c3480ccf128219a6172e54",
          "ref": "main",
          "commit_title": "文件title"
        },
        "_links": {
          "project": "https://test.gitcode.net/mactribe/midsommarcartoon",
          "action_type": ""
        }
      }
    ]
  },
  "next": "2024-08-01T10:10:40.370Z"
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/users/dengmengmian/events?access_token={your-token}&year=2024&next=2024-09-05T13%3A48%3A47.370Z'
```

## 5. 获取某个用户的公开仓库

### 请求

`GET https://api.gitcode.com/api/v5/users/{username}/repos`

### 参数

| 参数名         | 描述                                                                                                             | 类型  | 数据类型 |
| -------------- | ---------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                                       | query | string   |
| username\*     | 用户名(username/login)                                                                                           | path  | string   |
| type           | 用户创建的仓库(owner)，用户个人仓库(personal)，用户为仓库成员(member)，所有(all)。默认: 所有(all)                | query | string   |
| sort           | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name | query | string   |
| direction      | 如果sort参数为full_name，用升序(asc)。否则降序(desc)                                                             | query | string   |
| page           | 当前的页码                                                                                                       | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                                                                  | query | int      |

### 响应

```json
[
  {
    "id": 2734882,
    "full_name": "dengmengmian/manifest",
    "human_name": "dengmengmian / manifest",
    "url": "https://api.gitcode.com/api/v5/repos/dengmengmian/manifest",
    "namespace": {
      "id": 199940,
      "type": "user",
      "name": "dengmengmian",
      "path": "dengmengmian",
      "html_url": "https://gitcode.com/dengmengmian"
    },
    "path": "manifest",
    "name": "manifest",
    "description": "manifest",
    "status": "开始",
    "ssh_url_to_repo": "git@gitcode.com:dengmengmian/manifest.git",
    "http_url_to_repo": "https://gitcode.com/dengmengmian/manifest.git",
    "web_url": "https://gitcode.com/dengmengmian/manifest",
    "homepage": "https://gitcode.com/dengmengmian/manifest",
    "members": ["dengmengmian"],
    "assignee": [
      {
        "id": "268",
        "login": "dengmengmian",
        "name": "麻凡",
        "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
        "html_url": "https://gitcode.com/dengmengmian",
        "type": "User"
      }
    ],
    "forks_count": 0,
    "stargazers_count": 0,
    "project_labels": [],
    "relation": "master",
    "permission": {
      "pull": true,
      "push": true,
      "admin": true
    },
    "internal": false,
    "open_issues_count": 0,
    "has_issue": false,
    "watched": false,
    "watchers_count": 0,
    "assignees_number": 1,
    "enterprise": {
      "id": 199940,
      "path": "dengmengmian",
      "html_url": "https://gitcode.com/dengmengmian",
      "type": "user"
    },
    "default_branch": "master",
    "fork": false,
    "owner": {
      "id": "268",
      "login": "dengmengmian",
      "name": "麻凡",
      "type": "User"
    },
    "assigner": {
      "id": "268",
      "login": "dengmengmian",
      "name": "麻凡",
      "type": "User"
    },
    "issue_template_source": "project",
    "private": false,
    "public": true,
    "gitee": {
      "star": 10,
      "fork": 15,
      "watch": 1
    }
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/users/dengmengmian/repos?access_token=yuBy&type=all&sort=full_name&page=1&pre_page=20'
```

## 6. 创建个人项目仓库

### 请求

`POST https://api.gitcode.com/api/v5/user/repos`

### 参数

| 参数名             | 描述                                                    | 类型     | 数据类型 |
| ------------------ | ------------------------------------------------------- | -------- | -------- |
| access_token\*     | 用户授权码                                              | query    | string   |
| name\*             | 仓库名称                                                | formData | string   |
| description        | 仓库描述                                                | formData | string   |
| has_issues         | 允许提Issue与否。默认: 允许(true)                       | formData | boolean  |
| has_wiki           | 提供Wiki与否。默认: 提供(true)                          | formData | boolean  |
| can_comment        | 允许用户对仓库进行评论。默认：允许(true)                | formData | boolean  |
| auto_init          | 值为true时则会用README初始化仓库。默认: 不初始化(false) | formData | boolean  |
| gitignore_template | Git Ignore模版                                          | formData | string   |
| license_template   | License模版                                             | formData | string   |
| path               | 仓库路径                                                | formData | string   |
| private            | 是否私有                                                | formData | boolean  |
| default_branch     | 初始化项目时的默认分支名称。默认: main                  | formData | string   |

\*表示必填项。

### 响应

返回 "success" 表示成功，其他为失败

```json
{
  "id": 4106383,
  "full_name": "dengmengmian/wunian-prj",
  "human_name": "dengmengmian / wunian-prj",
  "url": "https://api.gitcode.com/api/v5/user/repos",
  "namespace": {
    "id": 199940,
    "name": "dengmengmian",
    "path": "dengmengmian",
    "develop_mode": "normal",
    "kind": "user",
    "full_path": "dengmengmian",
    "full_name": "dengmengmian",
    "visibility_level": 20,
    "enable_file_control": false,
    "owner_id": 268
  },
  "path": "wunian-prj",
  "name": "wunian-prj",
  "description": "wunian-prj",
  "private": true,
  "public": false,
  "visibility": "private"
}
```

### Demo

```bash
curl --location -g --request POST 'https://api.gitcode.com/api/v5/user/repos?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "wunian-prj",
  "description": "wunian-prj",
  "has_issues": true,
  "has_wiki": true,
  "can_comment": true,
  "public": 0,
  "private": true,
  "auto_init": true,
  "gitignore_template": "string",
  "license_template": "string",
  "path": "wunian-prj"
}'
```

## 7. 列出授权用户所有仓库

### 请求

`GET https://api.gitcode.com/api/v5/user/repos`

### 参数

| 参数名         | 描述                                                                                                                                                                                                                                                                                                                                                     | 类型  | 数据类型 |
| -------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                                                                                                                                                                                                                                                                               | query | string   |
| visibility     | 公开(public)、私有(private)或者所有(all)，默认: 所有(all)                                                                                                                                                                                                                                                                                                | query | string   |
| affiliation    | owner(授权用户拥有的仓库)、collaborator(授权用户为仓库成员)、organization_member(授权用户为仓库所在组织并有访问仓库权限)、enterprise_member(授权用户所在企业并有访问仓库权限)、admin(所有有权限的，包括所管理的组织中所有仓库、所管理的企业的所有仓库)。可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | query | string   |
| type           | 筛选用户仓库: 其创建(owner)、个人(personal)、其为成员(member)、公开(public)、私有(private)，不能与 affiliation 参数一并使用，否则会报 422 错误，与visibility参数一起使用，visibility参数拥有更高的优先级                                                                                                                                                 | query | string   |
| sort           | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name                                                                                                                                                                                                                                         | query | string   |
| direction      | 如果sort参数为full_name，用升序(asc)。否则降序(desc)                                                                                                                                                                                                                                                                                                     | query | string   |
| q              | 搜索关键字                                                                                                                                                                                                                                                                                                                                               | query | string   |
| page           | 当前的页码                                                                                                                                                                                                                                                                                                                                               | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                                                                                                                                                                                                                                                                                                          | query | int      |

\*表示必填项。

### 响应

返回 "success" 表示成功，其他为失败

```json
[
  {
    "id": 4028329,
    "full_name": "tiandi/test_yf_repo",
    "human_name": "洪门 / test_yf_repo",
    "url": "https://api.gitcode.com/api/v5/repos/tiandi/test_yf_repo",
    "namespace": {
      "id": 1420034,
      "type": "enterprise",
      "name": "洪门",
      "path": "tiandi",
      "html_url": "https://gitcode.com/tiandi"
    },
    "path": "test_yf_repo",
    "name": "test_yf_repo",
    "description": "",
    "status": "开始",
    "ssh_url_to_repo": "git@gitcode.com:tiandi/test_yf_repo.git",
    "http_url_to_repo": "https://gitcode.com/tiandi/test_yf_repo.git",
    "web_url": "https://gitcode.com/tiandi/test_yf_repo",
    "homepage": "https://gitcode.com/tiandi/test_yf_repo",
    "members": ["aron1"],
    "assignee": [
      {
        "id": "332008",
        "login": "aron1",
        "name": "yanfan",
        "avatar_url": "https://cdn-img.gitcode.com/bd/ca/0115343247b338d0c53589a145501e84a58464272f2fb09b372cc3d2311b2b39.png?time=1722525295285",
        "html_url": "https://gitcode.com/aron1",
        "type": "User"
      }
    ],
    "forks_count": 0,
    "stargazers_count": 0,
    "project_labels": [],
    "relation": "master",
    "permission": {
      "pull": true,
      "push": true,
      "admin": true
    },
    "internal": false,
    "open_issues_count": 0,
    "has_issue": false,
    "watched": false,
    "watchers_count": 0,
    "assignees_number": 1,
    "enterprise": {
      "id": 1420034,
      "path": "tiandi",
      "html_url": "https://gitcode.com/tiandi",
      "type": "enterprise"
    },
    "default_branch": "main",
    "fork": false,
    "owner": {
      "id": "444601",
      "login": "yanfan",
      "name": "yanfan是随时随地送达啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊实打实",
      "type": "User"
    },
    "assigner": {
      "id": "444601",
      "login": "yanfan",
      "name": "yanfan是随时随地送达啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊实打实",
      "type": "User"
    },
    "private": true,
    "public": false
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/user/repos?access_token={your-token}&visibility=private&affiliation=owner%2Ccollaborator%2Corganization_member&sort=created&direction=desc&q=yf&page=1&per_page=2'
```

## 8. 获取用户的某个仓库

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |

### 响应

```json
{
    "id": 4250980,
    "full_name": "aron1/Model10123001",
    "human_name": "aron1 / Model10123001tewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtew",
    "url": "https://api.gitcode.com/api/v5/repos/aron1/Model10123001",
    "namespace": {
        "id": 1364544,
        "name": "aron1",
        "path": "aron1",
        "html_url": "https://gitcode.com/aron1"
    },
    "path": "Model10123001",
    "name": "Model10123001tewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtewteewtew",
    "description": "",
    "status": "关闭",
    "ssh_url_to_repo": "git@gitcode.com:aron1/Model10123001.git",
    "http_url_to_repo": "https://gitcode.com/aron1/Model10123001.git",
    "web_url": "https://gitcode.com/aron1/Model10123001",
    "readme_url": "https://gitcode.com/aron1/Model10123001/blob/main/README.md",
    "created_at": "2024-10-22T22:14:06.922+08:00",
    "updated_at": "2024-12-02T18:37:23.426+08:00",
    "creator": {
        "id": "660ba866683c570b25be06c8",
        "arts_id": "332008",
        "username": "aron1",
        "nickname": "yanfan",
        "email": "aron1@noreply.gitcode.com",
        "photo": "https://cdn-img.gitcode.com/bd/ca/0115343247b338d0c53589a145501e84a58464272f2fb09b372cc3d2311b2b39.png?time=1722525295285"
    },
    "members": [
        "aron1"
    ],
    "forks_count": 0,
    "stargazers_count": 1,
    "project_labels": [],
    "license": "Apache_License_v2.0",
    "internal": false,
    "open_issues_count": 0,
    "watchers_count": 0,
    "assignees_number": 0,
    "enterprise": {
        "id": 1364544,
        "path": "aron1",
        "html_url": "https://gitcode.com/aron1",
        "type": "user"
    },
    "default_branch": "main",
    "fork": false,
    "owner": {
        "id": "332008",
        "login": "aron1",
        "name": "yanfan",
        "type": "User"
    },
    "assigner": {
        "id": "332008",
        "login": "aron1",
        "name": "yanfan",
        "type": "User"
    },
    "issue_template_source": "project",
    "private": true,
    "public": false
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang/test?access_token=?'
```

## 9. 添加一个公钥

### 请求

`POST https://api.gitcode.com/api/v5/user/keys`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |
| key\*          | 公钥内容   | body  | string   |
| title\*        | 公钥名称   | body  | string   |

### 响应

```json
{
  "id": 311915,
  "title": "555555",
  "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIa6IyTGuI8V5wrhANDFyezQqL73dY9ctLGHgpOggp7E Gitee",
  "created_at": "2024-11-14T03:34:40.318+00:00",
  "url": "https://api.gitcode.com/v5/user/keys/311915"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/user/keys?access_token=?' \
--header 'Content-Type: application/json;charset=UTF-8' \
--data '{
    "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIa6IyTGuI8V5wrhANDFyezQqL73dY9ctLGHgpOggp7E Gitee SSH Key",
    "title": "555555"
}'
```

## 10. 列出授权用户的所有公钥

### 请求

`GET https://api.gitcode.com/api/v5/user/keys`

### 参数

| 参数名         | 描述                            | 类型  | 数据类型 |
| -------------- | ------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                      | query | string   |
| page           | 当前的页码                      | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20 | query | int      |

### 响应

```json
[
  {
    "id": 308357,
    "title": "xiaogang@csdn.net\r\n",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwT9UXXtGfLa16tbxV+0RQ6m+BaAG2wJvqApr+juVNEmnM0lKNt1tyxY/V9SsCRf38UprPLTp71+btRpFIH9TLrGhkvT3tJOouYDXVUpSaigi7OO+6eLc+Cn0TZSLj4RmwVe/w93kmsCUzgqkeHk14K3S+2oCCm1rbpBAvpPhSKHhAH9LcTBecDoZ+NA2dsEDyfsloVH5cMJQO9n2W1QYduMuuaVHHpehSdDohN7cDI799Rwofaqqyz6ZJrc6eBjSVi1W+JPDTT6NW0+eFBYXo3KWybffixH4cAWdbS1Ms5Pe9Xh+G4WqFuhFh9zCoXlRUUrArLo5pYfpy5gv4iUVmniM0Pb0/Y5x8RJyGaPdS/2c68s8LQsm/9Ees8aeE5TcT5isDEvh+wy7jp1xi5nONk9QvOy7EdYYeHQtkw/0rklsz7UvAIjjHObNNYpY6RLQRT+dqN/lAb7stT047FSxqcNMCX/cybapLygs1y2ClcgU42p16RfgCH0NKA5emRhM= xiaogang@csdn.net",
    "created_at": "2024-07-23T10:29:42.119+00:00",
    "url": "https://api.gitcode.com/v5/user/keys/308357"
  },
  {
    "id": 311915,
    "title": "555555",
    "key": "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIa6IyTGuI8V5wrhANDFyezQqL73dY9ctLGHgpOggp7E Gitee",
    "created_at": "2024-11-14T03:34:40.318+00:00",
    "url": "https://api.gitcode.com/v5/user/keys/311915"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/user/keys?access_token=?'
```

## 11. 删除一个公钥

### 请求

`DELETE https://api.gitcode.com/api/v5/user/keys/{id}`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |
| id\*           | 公钥 ID    | path  | string   |

### 响应

无

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/user/keys/311914?access_token=?'
```

## 12. 获取一个公钥

### 请求

`GET https://api.gitcode.com/api/v5/user/keys/{id}`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |
| id\*           | 公钥 ID    | path  | string   |

### 响应

```json
[
  {
    "id": 308357,
    "title": "xiaogang@csdn.net\r\n",
    "key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwT9UXXtGfLa16tbxV+0RQ6m+BaAG2wJvqApr+juVNEmnM0lKNt1tyxY/V9SsCRf38UprPLTp71+btRpFIH9TLrGhkvT3tJOouYDXVUpSaigi7OO+6eLc+Cn0TZSLj4RmwVe/w93kmsCUzgqkeHk14K3S+2oCCm1rbpBAvpPhSKHhAH9LcTBecDoZ+NA2dsEDyfsloVH5cMJQO9n2W1QYduMuuaVHHpehSdDohN7cDI799Rwofaqqyz6ZJrc6eBjSVi1W+JPDTT6NW0+eFBYXo3KWybffixH4cAWdbS1Ms5Pe9Xh+G4WqFuhFh9zCoXlRUUrArLo5pYfpy5gv4iUVmniM0Pb0/Y5x8RJyGaPdS/2c68s8LQsm/9Ees8aeE5TcT5isDEvh+wy7jp1xi5nONk9QvOy7EdYYeHQtkw/0rklsz7UvAIjjHObNNYpY6RLQRT+dqN/lAb7stT047FSxqcNMCX/cybapLygs1y2ClcgU42p16RfgCH0NKA5emRhM= xiaogang@csdn.net",
    "created_at": "2024-07-23T10:29:42.119+00:00",
    "url": "https://api.gitcode.com/v5/user/keys/308357"
  }
]
```

### Demo

```bash
curl --location  'https://api.gitcode.com/api/v5/user/keys/311914?access_token=?'
```

## 13. 获取授权用户的一个 Namespace

### 请求

`GET https://api.gitcode.com/api/v5/user/namespace?access_token=?`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |
| path\*         | Namespace path  | query  | string   |

### 响应

```json
{
    "id": 138108,
    "path": "xiaogang_test",
    "name": "aaasfd/sda/fsdaf/sdfsa",
    "html_url": "https://gitcode.com/xiaogang_test",
    "type": "group"
}
```

#### 响应字段说明

| 字段                             | 类型    | 说明                                          |
| -----------------------------   | ------- | --------------------------------------------- |
| `id`                        | integer  | Namespace Id                        |
| `path`                 | string  | Namespace path                           |
| `name`                 | string  | Namespace 名称        |
| `html_url`            | string | Namespace 访问地址                              |
| `type`                 | string | Namespace类型，目前只有group                              |

### Demo

```bash
curl --location  'GET https://api.gitcode.com/api/v5/user/namespace?access_token=?&path=xiaogang_test'
```

## 14. 列出授权用户 star 了的仓库

### 请求

`GET https://api.gitcode.com/api/v5/user/starred??access_token=?`

### 参数

| 参数名         | 描述       | 类型  | 数据类型 |
| -------------- | ---------- | ----- | -------- |
| access_token\* | 用户授权码 | query | string   |
| sort         | created/last_push 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间  | query  | string   |
| direction         | asc/desc 排序方向，默认：降序  | query  | string   |
| page           | 当前的页码                                                                        | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                                      | query | int      |

### 响应

```json
[
    {
        "id": 777621,
        "full_name": "xiaogang/private-new2",
        "human_name": "xiaogang / private-new2",
        "url": "https://api.gitcode.com/api/v5/repos/xiaogang/private-new2",
        "namespace": {
            "id": 137117,
            "type": "user",
            "name": "xiaogang",
            "path": "xiaogang",
            "html_url": "https://gitcode.com/xiaogang"
        },
        "path": "private-new2",
        "name": "private-new2",
        "parentfull_name": "xiaogang_test/private-new",
        "description": "特朗普",
        "status": "开始",
        "ssh_url_to_repo": "ssh://git@gitcode.com:2222/xiaogang/private-new2.git",
        "http_url_to_repo": "https://gitcode.com/xiaogang/private-new2.git",
        "web_url": "https://gitcode.com/xiaogang/private-new2",
        "created_at": "2024-12-11T17:41:14.536+08:00",
        "updated_at": "2024-12-11T17:41:14.536+08:00",
        "homepage": "https://gitcode.com/xiaogang/private-new2",
        "members": [
            "xiaogang"
        ],
        "parent": {
            "full_name": "xiaogang_test/private-new",
            "human_name": "xiaogang_test / private-new"
        },
        "forks_count": 0,
        "stargazers_count": 1,
        "relation": "master",
        "permission": {
            "pull": true,
            "push": true,
            "admin": true
        },
        "internal": false,
        "open_issues_count": 0,
        "has_issue": false,
        "has_issues": false,
        "watchers_count": 0,
        "enterprise": {
            "id": 137117,
            "path": "xiaogang",
            "html_url": "https://gitcode.com/xiaogang",
            "type": "user"
        },
        "default_branch": "main",
        "fork": true,
        "pushed_at": "2024-12-20T19:14:34.979+08:00",
        "owner": {
            "id": "496",
            "login": "xiaogang",
            "name": "肖刚",
            "type": "User"
        },
        "issue_template_source": "project",
        "project_creator": "xiaogang",
        "private": false,
        "public": true
    }
]
```

#### 响应字段说明

| 字段                             | 类型    | 说明                                          |
| -----------------------------   | ------- | --------------------------------------------- |
| `id`                        | integer  | 仓库 Id                        |
| `full_name`                 | string  | 仓库全路径                           |
| `human_name`                | string  | 仓库全名称        |
| `url`                       | string | 仓库详情访问地址                              |
| `path`                      | string | 仓库路径                              |
| `name`                      | string | 仓库名称                              |
| `parentfull_name`           | string | 父仓库名称                              |
| `description`               | string | 仓库描述                              |
| `status`                    | string | 仓库状态                              |
| `ssh_url_to_repo`           | string | 仓库ssh git地址                              |
| `http_url_to_repo`          | string | 仓库http git地址                              |
| `web_url`                   | string | 仓库web访问地址                              |
| `created_at`                | string | 创建时间                              |
| `updated_at`                | string | 更新时间                              |
| `homepage`                  | string | 仓库主页                              |
| `members`                   | array | 成员                              |
| `forks_count`               | integer | fork数量                              |
| `stargazers_count`          | integer | star数量                              |
| `relation`                  | string | 当前用户相对于仓库的角色                              |
| `permission`                | object | 操作权限                              |
| `internal`                  | boolean | 是否内部开源                              |
| `open_issues_count`         | integer | 开启的issue数量                              |
| `has_issues`                | boolean | 是否开启issue功能                              |
| `watchers_count`            | integer | watch数量                              |
| `default_branch`            | string | 默认分支                              |
| `fork`                      | boolean | 是否fork仓库                              |
| `pushed_at`                 | string | 最近一次代码推送时间                              |
| `issue_template_source`     | string | Issue 模版来源 project: 使用仓库 Issue Template 作为模版； enterprise: 使用企业工作项作为模版                              |
| `project_creator`           | string | 仓库创建人                              |
| `private`                   | boolean | 是否私有仓库                              |
| `public`                    | boolean | 是否公开仓库                              |
| `namespace`                 | object | 仓库所属namespace                              |
| `parent`                    | object | 父仓库                              |
| `enterprise`                | object | 仓库企业信息                              |
| `owner`                     | object | 仓库拥有者信息                              |


### Demo

```bash
curl --location  'GET https://api.gitcode.com/api/v5/user/namespace?access_token=?&path=xiaogang_test'
```
