---
linkTitle: Member
title: 成员接口文档
weight: 10
sidebar:
  open: false
---

## 1. 添加项目成员或更新项目成员权限

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/collaborators/{username}`

### 参数

| 参数名         | 描述                                                                                       | 类型     | 数据类型 |
| -------------- | ------------------------------------------------------------------------------------------ | -------- | -------- |
| access_token\* | 用户授权码                                                                                 | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                                     | path     | string   |
| repo\*         | 仓库路径(path)                                                                             | path     | string   |
| username\*     | 用户名(username/login)                                                                     | path     | string   |
| permission     | 成员权限: 拉代码(pull)，推代码(push)，仓库维护者(admin)， 自定义角色传入角色名。默认: push | formData | string   |

### 响应

```json
{
  "id": 7543745,
  "login": "centking",
  "name": "占分",
  "avatar_url": null,
  "html_url": "https://gitcode.com/centking",
  "remark": "",
  "type": "User",
  "permissions": {
    "pull": true,
    "push": true,
    "admin": false
  }
}
```

### Demo

```bash
curl --location --request PUT 'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/collaborators/user1?access_token={your-token}' \
--form 'permission="admin"'
```

## 2. 移除项目成员

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/collaborators/{username}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| username\*     | 用户名(username/login)                 | path  | string   |

### 响应

```html
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request DELETE 'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/collaborators/user1?access_token={your-token}' \
```

## 3. 获取仓库的所有成员

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/collaborators`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | formData | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |
| page           | 当前的页码:默认为 1                    | query    | int      |
| per_page       | 每页的数量，最大为 100，默认 20        | query    | int      |

### 响应

```json
[
  {
    "id": "708",
    "name": "Lzm_0916",
    "username": "Lzm_0916",
    "nick_name": null,
    "state": null,
    "avatar": null,
    "avatar_url": null,
    "email": null,
    "name_cn": null,
    "web_url": "https://test.gitcode.net/Lzm_0916",
    "access_level": null,
    "expires_at": null,
    "limited": null,
    "type": "ProjectMember",
    "last_owner": null,
    "is_current_source_member": null,
    "last_source_owner": null,
    "join_way": null,
    "source_name": null,
    "member_roles": null,
    "iam_id": null,
    "committer_system_from": null,
    "permissions": {
      "pull": null,
      "push": null,
      "admin": true
    }
  }
]
```

### Demo

```bash
curl --location  'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/collaborators?access_token={your-token}' \

```

## 4. 判断用户是否为仓库成员

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/collaborators/{username}`

### 参数

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| username       | 用户名(username/login)                         | path  | string   |

### 响应

```json
{
    "message": "404 Not Found"
}
or
{

}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/collaborators/dengmengmian1?access_token=yuBy'
```

## 5. 查看仓库成员的权限

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/collaborators/{username}/permission`

### 参数

| 参数名            | 描述                                           | 类型  | 数据类型 |
|----------------| ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| username\*     | 用户名(username/login)                         | path  | string   |

### 响应

```json
{
  "id": 268,
  "login": "dengmengmian",
  "permission": "admin"
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/collaborators/dengmengmian/permission?access_token=yuBy'
```
