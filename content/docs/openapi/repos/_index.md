---
linkTitle: Repositories
title: 仓库接口文档
weight: 1
sidebar:
  open: false
---

## 1. 获取仓库目录Tree

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/git/trees/{sha}`

### 参数

| 参数名         | 描述                                            | 类型  | 数据类型 |
| -------------- | ----------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                      | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)          | path  | string   |
| repo\*         | 仓库路径(path)                                  | path  | string   |
| sha\*          | 可以是分支名(如master)、Commit或者目录Tree的SHA | path  | string   |
| page           | 当前的页码                                      | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                 | query | int      |
| recursive      | 赋值为1递归获取目录                             | query | int      |

### 响应

```json
{
  "tree": [
    {
      "sha": "5259c4b24f015ffdc3663e81837a730c2a108e1f",
      "name": "b",
      "type": "tree",
      "path": "a/b",
      "mode": "040000",
      "md5": "a7e86136543b019d72468ceebf71fb8e"
    }
  ]
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/git/trees/main?access_token=?'
```

## 2. 获取仓库具体路径下的内容

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contents/{path}`

### 参数

| 参数名         | 描述                                          | 类型  | 数据类型 |
| -------------- | --------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                    | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)        | path  | string   |
| repo\*         | 仓库路径(path)                                | path  | string   |
| path\*         | 文件的路径                                    | path  | string   |
| ref            | 分支、tag或commit。默认: 仓库的默认分支(main) | query | string   |

### 响应

```json
{
  "type": "file",
  "encoding": "base64",
  "size": 19,
  "name": "Note2.md",
  "path": "Note2.md",
  "content": "JXU2RDRCJXU4QkQ1d2ViaG9vaw==",
  "sha": "e5699fe1b360d6c799ee58b24fb5a670b1e14851",
  "url": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/contents/Note2.md",
  "html_url": "https://gitcode.com/daming_1/zhu_di/blob/master/Note2.md",
  "download_url": "https://gitcode.com/daming_1/zhu_di/raw/master/Note2.md",
  "_links": {
    "self": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/contents/Note2.md",
    "html": "https://gitcode.com/daming_1/zhu_di/blob/master/Note2.md"
  }
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/contents/1.txt?access_token=?&ref=main'
```

## 3. 获取文件列表

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/file_list`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| ref_name       | ref(分支、tag、commit)                 | query | string   |
| file_name      | 要查找的文件名称                       | query | string   |

### 响应

```json
["abc/test.rs", "bcd/test.rs"]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/owner-test/secure-issue/file_list?access_token=******&file_name=%2Ftest.rs&ref_name=main'
```

## 4. 新建文件

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contents/{path}`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| path\*         | 文件路径                               | path  | string   |
| content\*      | 文件内容, 要用 base64 编码             | body  | string   |
| message\*      | 提交的 commit 信息                     | body  | string   |
| branch         | 提交的分支                             | body  | string   |
| author[name]   | Author的名字，默认为当前用户的名字     | body  | string   |
| author[email]  | Author的邮箱，默认为当前用户的邮箱     | body  | string   |

### 响应

```json
{
  "commit": {
    "sha": "668cb104692b30d537b07f3721df9956d073d343",
    "author": {
      "name": "GitCode2023",
      "email": "13328943+gitcode_admin@user.noreply.gitcode.com",
      "date": "2024-04-11T09:15:20+00:00"
    },
    "committer": {
      "name": "Gitee",
      "email": "noreply@gitcode.com",
      "date": "2024-04-11T09:15:20+00:00"
    },
    "message": "22222",
    "parents": [
      {
        "sha": "0117aa5c6bc8e33d18ad8911afa3cbb54a1faabe"
      }
    ]
  }
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang/test/contents/xg8.txt?access_token=?' \
--header 'Content-Type: application/json;charset=UTF-8' \
--data-raw '{
    "content":"你好你好",
    "message":"api提交",
    "author[name]":"xg1",
    "author[email]":"xg1@qq.com",
    "committer[name]":"xg2",
    "committer[email]":"xg2@qq.com"
}'
```

## 5. 更新文件

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contents/{path}`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| path\*         | 文件的路径                             | path  | string   |
| content\*      | 文件内容, 要用 base64 编码             | body  | string   |
| sha\*          | 文件的 Blob SHA                        | body  | string   |
| branch         | 分支名称。默认为仓库对默认分支         | body  | string   |
| message\*      | 提交信息                               | body  | string   |
| author[name]   | Author的名字，默认为当前用户的名字     | body  | string   |
| author[email]  | Author的邮箱，默认为当前用户的邮箱     | body  | string   |

### 响应

```json
{
  "content": {
    "name": "Note2.md",
    "path": "Note2.md",
    "url": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/contents/Note2.md",
    "html_url": "https://gitcode.com/daming_1/zhu_di/blob/master/Note2.md",
    "download_url": "https://gitcode.com/daming_1/zhu_di/raw/master/Note2.md",
    "_links": {
      "self": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/contents/Note2.md",
      "html": "https://gitcode.com/daming_1/zhu_di/blob/master/Note2.md"
    }
  }
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang/test/contents/xg8.txt?access_token=?' \
--header 'Content-Type: application/json;charset=UTF-8' \
--data '{
    "content":"你好你好",
    "message":"api提交",
    "sha":"e69de29bb2d1d6434b8b29ae775ad8c2e48c5391"
}'
```

## 6. 删除文件

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contents/{path}`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| path\*         | 文件的路径                             | path  | string   |
| sha\*          | 文件的 Blob SHA                        | body  | string   |
| branch         | 分支名称。默认为仓库对默认分支         | body  | string   |
| message\*      | 提交信息                               | body  | string   |

### 响应

```json
{
  "commit": {
    "sha": "2a90c33ede2c1eafc5943727fd57129d870ad2e4",
    "author": {
      "name": "xiaogang",
      "email": "1150456356@qq.com",
      "date": "2024-11-25T02:11:56.000Z"
    },
    "committer": {
      "name": "xiaogang",
      "email": "1150456356@qq.com",
      "date": "2024-11-25T02:11:56.000Z"
    },
    "message": "api删除文件",
    "tree": "791f24a0da8c8458e40da3243bde183d59773514",
    "parents": ["33e9ee7ccd32835a0fb9f2af99264931c06fbe11"]
  }
}
```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/testyl001/test001/contents/1/2/3/4/5/test.md?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "sha": "52cfcc97f43188d16050e6395d456fc61f085eb9",
    "message": "删除文件",
    "branch": "branch01"
}'
```

## 7. 获取文件Blob

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/git/blobs/{sha}`

| 参数名         | 描述                                                        | 类型  | 数据类型 |
| -------------- | ----------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                  | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                      | path  | string   |
| repo\*         | 仓库路径(path)                                              | path  | string   |
| sha\*          | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 | path  | string   |

### 返回

```json
{
  "sha": "e5699fe1b360d6c799ee58b24fb5a670b1e14851",
  "size": 19,
  "url": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/git/blobs/e5699fe1b360d6c799ee58b24fb5a670b1e14851",
  "content": "JXU2RDRCJXU4QkQ1d2ViaG9vaw==",
  "encoding": "base64"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/git/blobs/e69de29bb2d1d6434b8b29ae775ad8c2e48c5391?access_token=?' \
```

## 8. 获取仓库的语言

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/languages`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | formData | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |

### 响应

Map`<语言名称,  百分比>`

```json
{
  "Shell": 49.77,
  "Python": 50.23
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/languages?access_token=?' \
```

## 9. 获取仓库贡献者

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contributors`

### 参数

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| type           | 贡献者类型(committers/authors)                 | query | string   |

### 响应

```json
[
  {
    "name": "dengmengmian",
    "contributions": 3,
    "email": "my@dengmengmian.com"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/contributors?access_token=xxxx&type=committers'
```

## 10. 设置项目模块

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/module/setting`

### 参数

| 参数名            | 描述                                   | 类型     | 数据类型 |
| ----------------- | -------------------------------------- | -------- | -------- |
| access_token\*    | 用户授权码                             | formData | string   |
| owner\*           | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*            | 仓库路径(path)                         | path     | string   |
| has_wiki          | 是否设置wiki                           | body     | boolean  |
| has_issue         | 是否设置issue                          | body     | boolean  |
| has_security      | 是否设置安全配置                       | body     | boolean  |
| has_merge_request | 是否设置merge request                  | body     | boolean  |
| has_fork          | 是否设置fork                           | body     | boolean  |
| has_analysis      | 是否设置分析                           | body     | boolean  |
| has_discussion    | 是否设置讨论                           | body     | boolean  |

### 响应

返回 "success" 表示成功，其他为失败

```json
{
  "msg": "success",
  "code": 1
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/testyl001/test001/module/setting?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "has_wiki": true,
}'
```

## 11. 更新仓库设置

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| name\*         | 仓库名称                               | body  | string   |
| description    | 仓库描述                               | body  | string   |
| homepage       | 主页(eg: https://gitcode.com)          | body  | string   |
| path           | 更新仓库路径                           | body  | string   |
| private        | 仓库公开或私有。（true/false）         | body  | boolean  |
| default_branch | 默认分支                               | body  | string   |
| lfs_enabled    | 是否启动 lfs                           | body  | boolean  |

### 返回

```json
{
  "id": 34171993,
  "full_name": "daming_1/test_create_project_2",
  "human_name": "daming/test_create_project_2",
  "url": "https://gitcode.com/api/v5/repos/daming_1/test_create_project_2",

  "path": "test_create_project_2",
  "name": "test_create_project_2",

  "description": "描述",
  "private": false,
  "public": true,
  "namespace": {
    "id": 74962,
    "name": "group1111",
    "path": "group11111",
    "develop_mode": "normal",
    "region": null,
    "cell": "default",
    "kind": "group",
    "full_path": "group11111",
    "full_name": "group1111",
    "parent_id": null,
    "visibility_level": 20,
    "enable_file_control": false,
    "owner_id": null
  },
  "empty_repo": null,
  "starred": null,
  "visibility": "public",
  "owner": null,
  "creator": null,
  "forked_from_project": null,
  "item_type": null,
  "main_repository_language": null,
  "homepage": "http://www.baidi.com"
}
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/testyl001/test001?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "name": "test002",
    "description": "1"
}'
```

## 12. 修改项目代码审查设置

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/reviewer`

### 参数

| 参数名            | 描述                                                              | 类型  | 数据类型 |
| -------------- | ----------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                        | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                            | path  | string   |
| repo\*         | 仓库路径(path)                                                    | path  | string   |
| assignees      | 审查人员username，可多个，半角逗号分隔，如：(username1,username2) | body  | string   |
| testers        | 测试人员username，可多个，半角逗号分隔，如：(username1,username2) | body  | string   |
| assignees_number | 最少审查人数                                                      | body  | int      |
| testers_number | 最少测试人数                                                      | body  | int      |

**注意：assignees, testers, assignees_number, testers_number 至少设置一项**

### 响应

```json
{
  "id": 7543745,
  "created_at": "2024-01-24T14:33:44+08:00",
  "updated_at": "2024-04-07T21:23:08+08:00"
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/testyl001/test001/reviewer?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "assignees": "username1,username2",
    "testers": "username1,username2",
    "assignees_number": 1,
    "testers_number": 1
}'
```

## 13. 仓库归档

### 请求

`PUT https://api.gitcode.com/api/v5/org/{org}/repo/{repo}/status`

| 参数名         | 描述                       | 类型  | 数据类型 |
| -------------- | -------------------------- | ----- | -------- |
| access_token\* | 用户授权码                 | query | string   |
| org\*          | 仓库所属组织               | path  | string   |
| repo\*         | 仓库路径(path)             | path  | string   |
| status\*       | 仓库状态，0：开始，2：关闭 | body  | int      |
| password\*     | 用户密码                   | body  | string   |

### 返回

```json
{
  "code": 1,
  "msg": "success"
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/testyl001/test001/reviewer?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "assignees": "username1,username2",
    "testers": "username1,username2",
    "assignees_number": 1,
    "testers_number": 1
}'
```

## 14. 转移仓

### 请求

`POST https://api.gitcode.com/api/v5/org/{org}/projects/{repo}/transfer`

| 参数名         | 描述               | 类型  | 数据类型 |
| -------------- | ------------------ | ----- | -------- |
| access_token\* | 用户授权码         | query | string   |
| org\*          | 仓库所属组织       | path  | string   |
| repo\*         | 仓库路径           | path  | string   |
| transfer_to\*  | 要转移到的目标组织 | body  | string   |
| password\*     | 用户密码           | body  | string   |

### 返回

```json
{
  "code": 1,
  "msg": "success"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/org/xiaogang/projects/test/transfer?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "transfer_to": "xiaogang_test",
    "password": "password"
}'
```

## 15. 获取项目的权限模式

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/transition`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | formData | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |

### 响应

权限模式`1、2`，其中 1是继承模式，2是独立模式

```json
{
  "memberMgntMode": 1
}
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/xiaogang/test/transition?access_token={your-token}'
```

## 16. 更新仓库的权限模式

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/transition`

### 参数

| 参数名                       | 描述                                     | 类型    | 数据类型 |
|---------------------------|----------------------------------------|-------| -------- |
| access_token\*            | 用户授权码                                  | query | string   |
| owner\*                   | 仓库所属空间地址(组织或个人的地址path)                 | path  | string   |
| repo\*                    | 仓库路径(path)                             | path  | string   |
| mode\*                    | 成员权限管理模式: 1 (继承模式)，2 (是独立模式) 不接受设置混合模式 | body  | int      |

### 响应

返回 "success" 表示成功，其他为失败

```json
{
  "msg": "success",
  "code": 1
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang/test/transition?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "mode": 1
}'
```

## 17. 设置项目推送规则

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/push_config`

### 参数

| 参数名                   | 描述                                   | 类型     | 数据类型 |
| ------------------------ | -------------------------------------- | -------- | -------- |
| access_token\*           | 用户授权码                             | formData | string   |
| owner\*                  | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*                   | 仓库路径(path)                         | path     | string   |
| reject_not_signed_by_gpg | 只允许带有验证的签名提交               | body     | boolean  |
| commit_message_regex     | 提交信息校验                           | body     | string   |
| max_file_size            | 提交文件限制 （单位 MB）               | body     | Integer  |
| skip_rule_for_owner      | 项目管理员的提交不受上述规则限制       | body     | boolean  |
| deny_force_push          | 禁止强推（包括管理员）                 | body     | boolean  |

### 响应

返回参数含义参考 body

```json
{
  "reject_not_signed_by_gpg": false,
  "deny_force_push": true,
  "max_file_size": 10,
  "skip_rule_for_owner": false
}
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/gitcode-dev/euler-api-list/push_config' \
--header 'PRIVATE-TOKEN: {your-token}' \
--header 'Cookie: GitCodeUserName=bond007; HWWAFSESID=ac1f8735c7a0f5acb6; HWWAFSESTIME=1730276856784'
```

## 18. 获取项目推送规则

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/push_config`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | formData | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |

### 响应

返回参数含义：参考设置项目推送规则 body

```json
{
  "reject_not_signed_by_gpg": false,
  "deny_force_push": true,
  "max_file_size": 10,
  "skip_rule_for_owner": false
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/gitcode-dev/euler-api-list/push_config' \
--header 'PRIVATE-TOKEN: {your-token}' \
--form 'deny_force_push="true"'
```

## 19. 删除一个仓库

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | formData | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |

### 响应

无

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/xiaogang/test?access_token={your-token}' \
--header 'Content-Type: application/json'
```

## 20. 创建组织仓库

### 请求

`POST https://api.gitcode.com/api/v5/orgs/{org}/repos`

### 参数

| 参数名             | 描述                                                                           | 类型  | 数据类型 |
| ------------------ | ------------------------------------------------------------------------------ | ----- | -------- |
| access_token\*     | 用户授权码                                                                     | query | string   |
| name\*             | 仓库名称                                                                       | body  | string   |
| description        | 仓库描述                                                                       | body  | string   |
| homepage           | 主页                                                                           | body  | string   |
| has_issues         | TRUE 允许提Issue与否。默认: 允许(true)                                         | body  | boolean  |
| has_wiki           | TRUE 提供Wiki与否。默认: 提供(true)                                            | body  | boolean  |
| auto_init          | 值为true时则会用README初始化仓库。默认: 不初始化(false)                        | body  | boolean  |
| gitignore_template | Git Ignore模版                                                                 | body  | string   |
| license_template   | License模版                                                                    | body  | string   |
| path               | 仓库路径                                                                       | body  | string   |
| private            | private: true/false                                                            | body  | boolean  |
| import_url         | 导入项目的 URL，需以 `.git` 结尾, 例如： `https://github.com/apache/kafka.git` | body  | string   |

### 响应

返回参数含义：参考设置项目推送规则 body

```json
{
  "id": 729293,
  "full_name": "xiaogang/4",
  "human_name": "xiaogang / 4",
  "url": "https://api.gitcode.com/api/v5/repos/xiaogang/4",
  "namespace": {
    "id": 137117,
    "name": "xiaogang",
    "path": "xiaogang",
    "develop_mode": "normal",
    "kind": "user",
    "full_path": "xiaogang",
    "full_name": "xiaogang",
    "visibility_level": 20,
    "enable_file_control": false,
    "owner_id": 496
  },
  "path": "4",
  "name": "4",
  "private": true,
  "public": false,
  "visibility": "private"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/repos?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "name": "4",
    "has_issues": "true",
    "has_wiki": "true",
    "can_comment": "true",
    "private": "true"
}'
```

## 21. Fork一个仓库

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/forks`

### 参数

| 参数名         | 描述                                               | 类型  | 数据类型 |
| -------------- | -------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                         | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path)       | path  | string   |
| repo\*         | 仓库路径(path)                                     | path  | string   |
| organization   | 组织空间完整地址，不填写默认Fork到用户个人空间地址 | body  | string   |
| name           | fork 后仓库名称。默认: 源仓库名称                  | body  | string   |
| path           | fork 后仓库地址。默认: 源仓库地址                  | body  | string   |

### 响应

返回参数含义：参考设置项目推送规则 body

```json
{
  "id": 729292,
  "full_name": "xiaogang_test/fork2",
  "human_name": "测试组织 / fork2",
  "url": "https://api.gitcode.com/api/v5/repos/xiaogang_test/fork2",
  "namespace": {
    "id": 138108,
    "name": "测试组织",
    "path": "xiaogang_test",
    "develop_mode": "normal",
    "cell": "default",
    "kind": "group",
    "full_path": "xiaogang_test",
    "full_name": "测试组织",
    "visibility_level": 20,
    "enable_file_control": false
  }
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/nevins/LessConsole/forks?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data '{
    "name":"fork2",
    "path":"fork2",
    "organization":"xiaogang_test"
}'
```

## 22. 查看仓库的Forks

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/forks`

### 参数

| 参数名         | 描述                                                         | 类型  | 数据类型 |
| -------------- | ------------------------------------------------------------ | ----- | -------- |
| access_token\* | 用户授权码                                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path)                 | path  | string   |
| repo\*         | 仓库路径(path)                                               | path  | string   |
| sort           | 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | query | string   |
| page           | 当前的页码                                                   | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                              | query | int      |
| created_after  | 在此之后创建的                                               | query | string   |
| created_before | 在此之前创建的                                               | query | string   |

### 响应

返回参数含义：参考设置项目推送规则 body

```json
[
  {
    "id": 567682,
    "full_name": "wangwt/RuoYi",
    "human_name": "wangwt / RuoYi",
    "url": "https://api.gitcode.com/api/v5/repos/wangwt/RuoYi",
    "namespace": {
      "id": 153748,
      "type": "personal",
      "name": "wangwt",
      "path": "wangwt",
      "html_url": "https://test.gitcode.net/wangwt"
    },
    "description": "",
    "status": "",
    "created_at": "2024-07-29T15:42:45.149+08:00",
    "updated_at": "2024-07-29T15:42:45.149+08:00",
    "owner": {
      "id": 970,
      "login": "wangwt",
      "name": "wangwt"
    },
    "pushed_at": "2024-11-08T16:24:10.576+08:00",
    "parent": {
      "id": 517092,
      "full_name": "xiaogang/RuoYi",
      "human_name": "xiaogang / RuoYi",
      "url": "https://api.gitcode.com/api/v5/repos/xiaogang/RuoYi",
      "namespace": {
        "id": 137117,
        "type": "personal",
        "name": "xiaogang",
        "path": "xiaogang",
        "html_url": "https://test.gitcode.net/xiaogang"
      }
    },
    "private": false,
    "public": true
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/nevins/LessConsole/forks?access_token={your-token}'
```

## 23. 上传图片

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/img/upload`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |
| body\*         | 文件内容 base64形式（限制20M）               | body  | string   |
| file_name\*    | 文件名                                       | body  | string   |

### 响应

```json
{
  "success": true,
  "path": "uploads/4c98ad75-729f-49ce-82ab-b41c1f7ffc90/test3.txt",
  "full_path": "https://gitcode.com/xiaogang/test/attachment/uploads/4c98ad75-729f-49ce-82ab-b41c1f7ffc90/test3.txt"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/tiandi/YanF4/img/upload?access_token=******' \
--header 'Content-Type: application/json' \
--data '{
"body":"1",
"file_name":"2.png"
}'
```

## 24. 上传文件

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/file/upload`

### 参数

| 参数名         | 描述                                         | 类型     | 数据类型 |
| -------------- | -------------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                                   | query    | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                               | path     | string   |
| file\*         | 文件（限制20M）                              | formData | file     |

### 响应

```json
{
  "success": true,
  "path": "uploads/4c98ad75-729f-49ce-82ab-b41c1f7ffc90/test3.txt",
  "full_path": "https://gitcode.com/xiaogang/test/attachment/uploads/4c98ad75-729f-49ce-82ab-b41c1f7ffc90/test3.txt"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/file/upload' \
--header 'Content-Type: application/json;charset=UTF-8' \
--header 'PRIVATE-TOKEN: token' \
--form 'file=@"/Users/xiaogang/Downloads/test3.txt"'
```

## 25. 列出 watch 了仓库的用户

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/subscribers`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |
| page           | 当前的页码                                   | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20              | query | int      |
| watched_after  | 在此之后watch的                              | query | String   |
| watched_before | 在此之前watch的                              | query | String   |

### 响应

```json
[
  {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/bc/cd/6bc422546cdf276c147f267030d83a43e927fec67ca66f0b22f7e03556206fa3.jpg",
    "watch_at": "2024-11-13T16:15:53.287+08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/subscribers' \
--header 'PRIVATE-TOKEN: token'
```

## 26. 列出 star 了仓库的用户

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/stargazers`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |
| page           | 当前的页码                                   | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20              | query | int      |
| starred_after  | 在此之后star的                               | query | string   |
| starred_before | 在此之前star的                               | query | string   |

### 响应

```json
[
  {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/bc/cd/6bc422546cdf276c147f267030d83a43e927fec67ca66f0b22f7e03556206fa3.jpg",
    "starred_at": "2024-11-13T16:15:53.287+08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/stargazers' \
--header 'PRIVATE-TOKEN: token'
```

## 27. 更新仓库设置

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/repo_settings`

### 参数

| 参数名                                     | 描述                                         | 类型  | 数据类型 |
| ------------------------------------------ | -------------------------------------------- | ----- | -------- |
| access_token\*                             | 用户授权码                                   | query | string   |
| owner\*                                    | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*                                     | 仓库路径(path)                               | path  | string   |
| disable_fork                               | 禁止派生项目（禁止Fork项目）                 | body  | Boolean  |
| forbidden_developer_create_branch          | 禁止开发者创建分支                           | body  | Boolean  |
| forbidden_developer_create_tag             | 禁止开发者创建标签                           | body  | Boolean  |
| forbidden_committer_create_branch          | 禁止提交者创建分支                           | body  | Boolean  |
| forbidden_developer_create_branch_user_ids | 禁止开发者创建分支的白名单用户ID             | body  | String   |
| branch_name_regex                          | 分支名称正则表达式                           | body  | String   |
| tag_name_regex                             | 标签名称正则表达式                           | body  | String   |
| generate_pre_merge_ref                     | 生成MergeRequest的Pre-Merge引用              | body  | Boolean  |
| rebase_disable_trigger_webhook             | MR rebase不触发Webhook事件                   | body  | Boolean  |
| open_gpg_verified                          | 公开GPG公钥验证                              | body  | Boolean  |
| include_lfs_objects                        | 压缩包下载包含LFS对象                        | body  | Boolean  |

### 响应

```json
{
  "disable_fork": true,
  "forbidden_developer_create_branch": false,
  "forbidden_developer_create_tag": false,
  "forbidden_committer_create_branch": false,
  "generate_pre_merge_ref": false,
  "forbidden_gitlab_access": true,
  "rebase_disable_trigger_webhook": false,
  "include_lfs_objects": false
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/repo_settings?access_token=token' \
--header 'Content-Type: application/json' \
--data '{"disable_fork": true}'
```

## 28. 获取仓库设置

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/repo_settings`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |

### 响应

```json
{
  "disable_fork": true,
  "forbidden_developer_create_branch": false,
  "forbidden_developer_create_tag": false,
  "forbidden_committer_create_branch": false,
  "generate_pre_merge_ref": false,
  "forbidden_gitlab_access": true,
  "rebase_disable_trigger_webhook": false,
  "include_lfs_objects": false
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/repo_settings?access_token=token'
```

## 29. 获取 Pull Request 设置

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pull_request_settings`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |

### 响应

```json
{
  "merge_request_setting": {
    "id": 1431,
    "project_id": 2745002,
    "disable_merge_by_self": false,
    "created_at": "2024-03-28T10:04:13.523+08:00",
    "updated_at": "2024-11-13T21:30:45.261+08:00",
    "can_force_merge": false,
    "disable_squash_merge": true,
    "approval_required_reviewers": 0,
    "approval_required_approvers": 0,
    "add_notes_after_merged": false,
    "merged_commit_author": "merged_by",
    "mark_auto_merged_mr_as_closed": false,
    "delete_source_branch_when_merged": false,
    "auto_squash_merge": false,
    "squash_merge_with_no_merge_commit": false,
    "close_issue_when_mr_merged": false,
    "can_reopen": true,
    "is_check_cla": false,
    "approval_approvers": [],
    "approval_testers": [],
    "approval_required_testers": 0,
    "is_allow_lite_merge_request": false
  },
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "only_allow_merge_if_pipeline_succeeds": false,
  "merge_method": "merge"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/pull_request_settings?access_token=token'
```

## 30. 更新 Pull Request 设置

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pull_request_settings`

### 参数

| 参数名                                           | 描述                                                                                                                                     | 类型  | 数据类型 |
| ------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\*                                   | 用户授权码                                                                                                                               | query | string   |
| owner\*                                          | 仓库所属空间地址(企业、组织或个人的地址path)                                                                                             | path  | string   |
| repo\*                                           | 仓库路径(path)                                                                                                                           | path  | string   |
| approval_required_reviewers_enable               | 是否启用审批必需的评审者功能                                                                                                             | body  | Boolean  |
| approval_required_reviewers                      | 需要的审批者数量(最小评审人数【选择的数字：1~5, 如果取消评审人功能传入0】)                                                               | body  | Integer  |
| only_allow_merge_if_all_discussions_are_resolved | 评审问题全部解决才能合入                                                                                                                 | body  | Boolean  |
| only_allow_merge_if_pipeline_succeeds            | 是否仅在流水线成功后才允许合并                                                                                                           | body  | Boolean  |
| disable_merge_by_self                            | 禁止合入自己创建的合并请求                                                                                                               | body  | Boolean  |
| can_force_merge                                  | 允许管理员强制合入                                                                                                                       | body  | Boolean  |
| add_notes_after_merged                           | 允许合并请求合并后继续做代码检视和评论                                                                                                   | body  | Boolean  |
| mark_auto_merged_mr_as_closed                    | 是否将自动合并的MR状态标记为关闭状态                                                                                                     | body  | Boolean  |
| can_reopen                                       | 是否可以重新打开一个已经关闭的合并请求                                                                                                   | body  | Boolean  |
| delete_source_branch_when_merged                 | 合并时是否删除源分支，默认删除原分支                                                                                                     | body  | Boolean  |
| disable_squash_merge                             | 禁止 Squash 合并                                                                                                                         | body  | Boolean  |
| auto_squash_merge                                | 新建合并请求，默认开启 Squash 合并                                                                                                       | body  | Boolean  |
| merge_method                                     | 合并模式三选一(通过 merge commit 合并：merge；通过 merge commit 合并 （记录半线性历史）：rebase_merge；fast - forward 合并：ff)          | body  | String   |
| squash_merge_with_no_merge_commit                | Squash 合并不产生 Merge 节点                                                                                                             | body  | Boolean  |
| merged_commit_author                             | 使用 MR (合入/创建) 者生成 Merge Commit（使用 PR 合入者生成 Merge Commit：传 merged_by; 使用 PR 创建者生成 Merge Commit：传 created_by） | body  | String   |
| approval_required_approvers                      | 需要审批的批准者数量                                                                                                                     | body  | Integer  |
| approval_approver_ids                            | 项目审查人, user_id 以逗号分隔                                                                                                           | body  | String   |
| approval_tester_ids                              | 项目测试人，user_id以逗号分隔                                                                                                            | body  | String   |
| approval_required_testers                        | 测试最小通过人数                                                                                                                         | body  | Integer  |
| is_check_cla                                     | 是否校验CLA                                                                                                                              | body  | Boolean  |
| is_allow_lite_merge_request                      | 是否启用轻量级 Pull Request                                                                                                              | body  | Boolean  |
| lite_merge_request_prefix_title                  | 轻量级 pr 的标题前缀                                                                                                                     | body  | String   |
| close_issue_when_mr_merged                       | 创建 Pull Request 时,默认选中 “合并后关闭已关联的 Issue”                                                                                 | body  | Boolean  |

### 响应

```json
{
  "merge_request_setting": {
    "id": 1431,
    "project_id": 2745002,
    "disable_merge_by_self": false,
    "created_at": "2024-03-28T10:04:13.523+08:00",
    "updated_at": "2024-11-13T21:30:45.261+08:00",
    "can_force_merge": false,
    "disable_squash_merge": true,
    "approval_required_reviewers": 0,
    "approval_required_approvers": 0,
    "add_notes_after_merged": false,
    "merged_commit_author": "merged_by",
    "mark_auto_merged_mr_as_closed": false,
    "delete_source_branch_when_merged": false,
    "auto_squash_merge": false,
    "squash_merge_with_no_merge_commit": false,
    "close_issue_when_mr_merged": false,
    "can_reopen": true,
    "is_check_cla": false,
    "approval_approvers": [],
    "approval_testers": [],
    "approval_required_testers": 0,
    "is_allow_lite_merge_request": false
  },
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "only_allow_merge_if_pipeline_succeeds": false,
  "merge_method": "merge"
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/pull_request_settings?access_token=token' \
--header 'Content-Type: application/json' \
--data '{"is_check_cla": false}'
```

## 31. 创建个人仓库

### 请求

`POST https://api.gitcode.com/api/v5/user/repos`

### 参数

| 参数名             | 描述                                                                           | 类型  | 数据类型 |
| ------------------ | ------------------------------------------------------------------------------ | ----- | -------- |
| access_token\*     | 用户授权码                                                                     | query | string   |
| name\*             | 仓库名称                                                                       | body  | string   |
| description        | 仓库描述                                                                       | body  | string   |
| homepage           | 主页                                                                           | body  | string   |
| has_issues         | TRUE 允许提Issue与否。默认: 允许(true)                                         | body  | boolean  |
| has_wiki           | TRUE 提供Wiki与否。默认: 提供(true)                                            | body  | boolean  |
| auto_init          | 值为true时则会用README初始化仓库。默认: 不初始化(false)                        | body  | boolean  |
| gitignore_template | Git Ignore模版                                                                 | body  | string   |
| license_template   | License模版                                                                    | body  | string   |
| path               | 仓库路径                                                                       | body  | string   |
| private            | true/false                                                                     | body  | boolean  |
| import_url         | 导入项目的 URL，需以 `.git` 结尾, 例如： `https://github.com/apache/kafka.git` | body  | string   |

### 响应

返回参数含义：参考设置项目推送规则 body

```json
{
  "id": 729293,
  "full_name": "xiaogang/4",
  "human_name": "xiaogang / 4",
  "url": "https://api.gitcode.com/api/v5/repos/xiaogang/4",
  "namespace": {
    "id": 137117,
    "name": "xiaogang",
    "path": "xiaogang",
    "develop_mode": "normal",
    "kind": "user",
    "full_path": "xiaogang",
    "full_name": "xiaogang",
    "visibility_level": 20,
    "enable_file_control": false,
    "owner_id": 496
  },
  "path": "4",
  "name": "4",
  "private": true,
  "public": false,
  "visibility": "private"
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/user/repos?access_token=token' \
--header 'Content-Type: application/json' \
--data '{"name": "test2"}'
```

## 32. 更新项目成员角色

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/members/{username}`

### 参数

| 参数名         | 描述                                                                                    | 类型  | 数据类型 |
| -------------- | --------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                              | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path)                                            | path  | string   |
| repo\*         | 仓库路径(path)                                                                          | path  | string   |
| username\*     | 项目成员用户名                                                                          | path  | string   |
| permission     | 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push, customized(自定义角色) | body  | string   |
| role_id        | 角色ID, 如果permission传入了custom则需要传入角色id                                      | body  | string   |

### 响应

```json
{
  "followers_url": "https://api-test.gitcode.com/api/v5/users/xiaogang2/followers",
  "html_url": "https://test.gitcode.net/xiaogang2",
  "id": "65ffca965079ba0d1c00f6f2",
  "login": "xiaogang2",
  "name": "肖刚2",
  "type": "User",
  "url": "https://api-test.gitcode.com/api/v5/xiaogang2",
  "permissions": {
    "admin": false,
    "push": false,
    "customized": true,
    "pull": false
  }
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang/test/members/xiaogang2?access_token=token' \
--header 'Content-Type: application/json' \
--data '{
    "permission": "admin"
}'
```

## 33. 仓库转移

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/transfer`

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| new_owner\*    | 要转移到的目标组织                     | body  | string   |

### 返回

```json
{
  "new_owner": "tiandi",
  "new_name": "new_repo_name"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/RealMadrid/repo_yf99999/transfer?access_token=your_token' \
--form 'new_owner="tiandi"' \
--form 'new_name="new_repo_name"'
```

## 34. 获取项目自定义角色

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/customized_roles`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |

### 响应

```json
[
  {
    "role_id": "e6cd76d6c82f46c78c71fd7f67eaf3bf",
    "access_level": 15,
    "role_name": "测试角色",
    "role_chinese_name": "测试角色",
    "role_description": "测试角色",
    "role_type": "project-customized",
    "member_count": 1,
    "created_at": "2024-04-17 08:00",
    "updated_at": "2024-04-17 08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/customized_roles?access_token=?'
```

## 35. 下载次数统计

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/download_statistics`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |
| start_date     | 统计起始日期包含当前日期（eg:2024-01-06)     | query | string   |
| end_date       | 统计截止日期包含当前日期 (eg: 2024-12-06)    | query | string   |
| direction      | 排序方式: 升序(asc)，降序(desc)。默认: desc  | query | string   |

### 响应

| 字段名                            | 描述                             | 数据类型 |
| --------------------------------- | -------------------------------- | -------- |
| pdate                             | 日期                             | string   |
| repo_id                           | 仓库 id                          | string   |
| total_dl_cnt                      | 从第一天到当前日期的累加下载总数 | Long     |
| today_dl_cnt                      | 当天的下载数                     | Long     |
| download_statistics_total         | 查询时间区间内的下载总数         | Long     |
| download_statistics_history_total | 到截止日期的下载总数             | Long     |

```json
{
  "download_statistics_detail": [
    {
      "pdate": "2024-11-13",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 38
    },
    {
      "pdate": "2024-11-14",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 38
    },
    {
      "pdate": "2024-11-15",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 38
    },
    {
      "pdate": "2024-11-16",
      "repo_id": "625513",
      "today_dl_cnt": 5,
      "total_dl_cnt": 43
    },
    {
      "pdate": "2024-11-17",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 43
    },
    {
      "pdate": "2024-11-18",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 43
    },
    {
      "pdate": "2024-11-19",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 43
    },
    {
      "pdate": "2024-11-20",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 43
    },
    {
      "pdate": "2024-11-21",
      "repo_id": "625513",
      "today_dl_cnt": 2,
      "total_dl_cnt": 45
    },
    {
      "pdate": "2024-11-22",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 45
    },
    {
      "pdate": "2024-11-23",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 45
    },
    {
      "pdate": "2024-11-24",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 45
    },
    {
      "pdate": "2024-11-25",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 45
    },
    {
      "pdate": "2024-11-26",
      "repo_id": "625513",
      "today_dl_cnt": 0,
      "total_dl_cnt": 45
    }
  ],
  "download_statistics_total": 7,
  "download_statistics_history_total": 45
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/openharmony/applications_app_samples/download_statistics?direction=asc&access_token=your_token&start_date=2024-01-11&end_date=2024-12-11'
```

## 36. 获取 raw 文件

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/raw/{path}`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                               | path  | string   |
| path\*         | 文件的路径                                   | path  | string   |
| ref            | 分支、tag 或 commit(默认: 仓库的默认分支)    | query | string   |

### 响应

```text
raw file content
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/org-test/repo-test/raw/test/test.py?access_token=your_token'
```

## 37. 获取仓库贡献者统计信息

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/contributors/statistic?access_token=your_token`

### 参数

| 参数名         | 描述                                                                                                                                            | IN    | 类型    |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | ----- | ------- |
| access_token\* | 用户授权码                                                                                                                                      | query | string  |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path)                                                                                                    | path  | string  |
| repo\*         | 仓库路径(path)                                                                                                                                  | path  | string  |
| author         | 过滤贡献者的用户名。支持指定用户名以获取该用户的贡献数据。默认情况下，返回所有用户的贡献数据。                                                  | query | string  |
| current_user   | 是否只返回当前用户的数据。`true` 表示只返回当前用户的贡献数据，`false` 或者省略则返回所有用户的贡献数据，当为`true`时，优先级高于`author`参数。 | query | boolean |
| since          | 起始日期，格式为 `YYYY-MM-DD`或者`YYYY-MM-DD HH:mm:ss`。用来限定返回的贡献数据的起始时间。                                                      | query | string  |
| until          | 结束日期，格式为 `YYYY-MM-DD`或者`YYYY-MM-DD HH:mm:ss`。用来限定返回的贡献数据的结束时间。                                                      | query | string  |
| ref_name       | 指定要获取贡献数据的 ref_name(分支名称、commit id、tag 名称)，不传或者为空时使用`默认分支`。                                                    | query | string  |

### 响应

```json
[
  {
    "name": "test",
    "email": "aa@ss.com",
    "overview": {
      "additions": 762,
      "deletions": 0,
      "total_changes": 0,
      "commit_count": 8
    },
    "contributions": [
      {
        "date": "2024-12-05",
        "additions": 759,
        "deletions": 0,
        "total_changes": 759,
        "commit_count": 5
      },
      {
        "date": "2024-12-08",
        "additions": 2,
        "deletions": 0,
        "total_changes": 2,
        "commit_count": 2
      },
      {
        "date": "2024-12-09",
        "additions": 1,
        "deletions": 0,
        "total_changes": 1,
        "commit_count": 1
      }
    ]
  }
]
```

#### 响应字段说明

| 字段                          | 类型    | 说明                                          |
| ----------------------------- | ------- | --------------------------------------------- |
| `name`                        | string  | 贡献者的姓名或用户名。                        |
| `email`                       | string  | 贡献者的邮箱地址。                            |
| `overview`                    | object  | 该贡献者的整体统计数据，包含以下字段：        |
| `overview.additions`          | integer | 总共增加的行数。                              |
| `overview.deletions`          | integer | 总共删除的行数。                              |
| `overview.total_changes`      | integer | 总共更改的行数（`additions` + `deletions`）。 |
| `overview.commit_count`       | integer | 总共的提交次数。                              |
| `contributions`               | array   | 贡献记录数组，包含每个日期的贡献数据。        |
| `contributions.date`          | string  | 贡献日期，格式为 `YYYY-MM-DD`。               |
| `contributions.additions`     | integer | 该日期增加的行数。                            |
| `contributions.deletions`     | integer | 该日期删除的行数。                            |
| `contributions.total_changes` | integer | 该日期更改的行数。                            |
| `contributions.commit_count`  | integer | 该日期提交的次数。                            |

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/org-test/myrepo/contributors/statistic?access_token=your_token'
```

## 38. 获取仓库动态

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/events?access_token=your_token`

### 参数

| 参数名         | 描述                                                                                                                                            | IN    | 类型    |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | ----- | ------- |
| access_token\* | 用户授权码                                                                                                                                      | query | string  |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path)                                                                                                    | path  | string  |
| repo\*         | 仓库路径(path)                                                                                                                                  | path  | string  |
| filter         | 筛选条件，传入all（所有的）, push（push事件）, merged（merged事件）, issue（issue事件）, comments（评论事件）, team（团队事件）, project（项目事件）                                                 | query | string  |
| author         | 事件触发人，传入用户名 | query | string |
| before          | 起始日期，格式为 `YYYY-MM-DD`。用来限定返回的事件的起始时间。                                                      | query | string  |
| after          | 结束日期，格式为 `YYYY-MM-DD`。用来限定返回的事件的结束时间。                                                      | query | string  |
| page           | 当前的页码                                   | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20              | query | int      |

### 响应

```json
{
    "events": [
        {
            "action": 22,
            "action_name": "opened",
            "author": {
                "id": 607,
                "iam_id": "f09ca8d721eb49e4add47b67c0dbef81",
                "username": "xiaogang",
                "state": "active",
                "email": "",
                "name": "xiaogang",
                "name_cn": "",
                "web_url": "https://gitcode.com/xiaogang"
            },
            "author_id": 607,
            "author_username": "xiaogang2",
            "created_at": "2024-11-19T07:01:02.714Z",
            "project_id": 249609,
            "title": "{\"userid\":[496],\"username\":[\"xiaogang\"]}",
            "filter_sensitive": true
        }
    ],
    "has_next_page": false
}
```

#### 响应字段说明

| 字段                             | 类型    | 说明                                          |
| -----------------------------   | ------- | --------------------------------------------- |
| `events`                        | array  | 事件列表                        |
| `has_next_page`                 | boolean  | 是否还存在下一页                           |
| `events.action`                 | integer  | 事件类型标识，每一种事件都有一个唯一的标识        |
| `events.action_name`            | string | 事件标识名称。                              |
| `events.author`                 | object | 事件的触发人                              |
| `events.author_id`              | integer | 事件的触发人ID |
| `events.author_username`        | string | 事件触发人账号                              |
| `events.created_at`             | string   | 事件触发时间。        |
| `events.project_id`             | integer  | 仓库id               |
| `events.title`                  | string | 事件标题                            |

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/events?access_token=your_token'
```
