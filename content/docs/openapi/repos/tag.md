---
linkTitle: Tag
title: tag接口文档
weight: 3
sidebar:
  open: false
---

## 1. 列出项目所有的tags

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/tags`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| page           | 当前的页码:默认为 1,默认： 1           | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "name": "v1.0",
    "message": "111",
    "commit": {
      "sha": "3e43581d16bc456802a1fee673b9a2a9b9618f0f",
      "date": "2024-04-14T02:59:22+00:00"
    },
    "tagger": {
      "name": "占分",
      "email": "7543745+centking@user.noreply.gitcode.com",
      "date": "2024-04-14T06:18:54+00:00"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/tags?access_token=xxxx'
```

## 2. 创建一个仓库的Tag

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/tags`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| refs\*         | 起点名称，默认main                     | body  | string   |
| tag_name\*     | 新创建的标签名称                       | body  | string   |
| tag_message    | Tag 描述,默认为空                      | body  | string   |
| access_token\* | 用户授权码                             | query | string   |

### 响应

```json
{
  "name": "tag2",
  "message": "",
  "commit": {
    "sha": "5d165dae3b073d3e92ca91c3edcb21994361462c",
    "date": "2024-04-08T13:13:33+00:00"
  },
  "tagger": null
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/tags?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "refs": "main",
    "tag_name": "tag",
}'
```

## 3. 列出项目保护 tags

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protected_tags`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| page           | 当前的页码:默认为 1:默认为 1           | query | int      |
| per_page       | 每页的数量：最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "name": "tag_name",
    "create_access_level": 30,
    "create_access_level_desc": "开发者、维护者、管理员"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protected_tags?access_token=your-token'
```

## 4. 删除项目保护 tag

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protected_tags/{tag_name}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| tag_name\*     | 标签名称                               | path  | String   |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protected_tags/your_tag?access_token=your-token'
```

## 5. 获取项目保护 tag 详情

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protected_tags/{tag_name}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| tag_name\*     | 标签名称                               | path  | String   |

### 响应

```json
{
  "name": "tag_name",
  "create_access_level": 30,
  "create_access_level_desc": "开发者、维护者、管理员"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protected_tags/your_tag?access_token=your-token'
```

## 6. 创建项目保护 tag

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protected_tags`

### 参数

| 参数名              | 描述                                                                                   | 类型  | 数据类型 |
| ------------------- | -------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\*      | 用户授权码                                                                             | query | string   |
| owner\*             | 仓库所属空间地址(组织或个人的地址path)                                                 | path  | string   |
| repo\*              | 仓库路径(path)                                                                         | path  | string   |
| name\*              | 标签名称                                                                               | body  | String   |
| create_access_level | 允许创建的访问级别(0:不允许任何人;30:开发者、维护者、管理员;40:维护者、管理员),默认:40 | body  | int      |

### 响应

```json
{
  "name": "your_tag_name",
  "create_access_level": 30,
  "create_access_level_desc": "开发者、维护者、管理员"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protected_tags?access_token=your-token' \
--header 'Content-Type: application/json' \
--data '{"name": "your_tag_name", "create_access_level": 30}'
```

## 7. 修改项目保护 tag

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protected_tags`

### 参数

| 参数名                | 描述                                                                           | 类型  | 数据类型 |
| --------------------- | ------------------------------------------------------------------------------ | ----- | -------- |
| access_token\*        | 用户授权码                                                                     | query | string   |
| owner\*               | 仓库所属空间地址(组织或个人的地址path)                                         | path  | string   |
| repo\*                | 仓库路径(path)                                                                 | path  | string   |
| name\*                | 标签名称                                                                       | body  | String   |
| create_access_level\* | 允许创建的访问级别(0:不允许任何人;30:开发者、维护者、管理员;40:维护者、管理员) | body  | int      |

### 响应

```json
{
  "name": "your_tag_name",
  "create_access_level": 30,
  "create_access_level_desc": "开发者、维护者、管理员"
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protected_tags?access_token=your-token' \
--header 'Content-Type: application/json' \
--data '{"name": "your_tag_name", "create_access_level": 30}'
```
