---
linkTitle: Branch
title: 分支接口文档
weight: 2
sidebar:
  open: false
---

## 1. 获取项目所有分支

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/branches`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| sort           | 排序字段 name/updated                  | query | string   |
| direction      | 排序方向 asc/desc                      | quey  | string   |
| page           | 当前的页码                             | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "name": "main",
    "commit": {
      "commit": {
        "author": {
          "name": "Lzm_0916",
          "date": "2024-04-16T08:41:20.000Z",
          "email": "Lzm_0916@noreply.gitcode.com"
        },
        "committer": {
          "name": "Lzm_0916",
          "date": "2024-04-16T08:41:20.000Z",
          "email": "Lzm_0916@noreply.gitcode.com"
        },
        "message": "提交测试类"
      },
      "sha": "1d45587145552af003cd32cc6fde9ac9d9e5fd42",
      "url": "https://test.gitcode.net/api/v5/repos/Lzm_0916/test/commits/1d45587145552af003cd32cc6fde9ac9d9e5fd42"
    },
    "protected": true
  }
]
```

### Demo

```bash
curl --location --request GET 'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/branches?access_token={your-token}' \
```

## 2. 创建分支

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/branches`

### 参数

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| refs\*         | 起点名称，默认：master                         | body  | string   |
| branch_name\*  | 新创建的分支名称                               | body  | string   |

### 响应

```json
{
  "name": "gitcode",
  "commit": {
    "commit": {
      "author": {
        "name": "dengmengmian",
        "date": "2024-09-10T08:29:18Z",
        "email": "dengmengmian@noreply.gitcode.com"
      },
      "committer": {
        "name": "dengmengmian",
        "date": "2024-09-10T08:29:18Z",
        "email": "dengmengmian@noreply.gitcode.com"
      },
      "message": "test"
    },
    "sha": "1af35823b8bbcaf68776cd6cb0ecbf88cfdec905",
    "url": "https://test.gitcode.net/api/v5/repos/dengmengmian/test03/commits/1af35823b8bbcaf68776cd6cb0ecbf88cfdec905"
  },
  "protected": false
}
```

### Demo

```bash
curl --location --request POST 'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/branches?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "refs": "main",
    "branch_name": "gitcode"
}'
```

## 3. 获取单个分支

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/branches/{branch}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| branch\*       | 分支名称                               | path  | string   |

### 响应

```json
{
  "name": "dev",
  "commit": {
    "id": "b6d44deb0ca73d7a50916d0fea02c72edd6c924e",
    "message": "\ndev新增文件\n\nCreated-by: csdntest13\nAuthor-id: 494\nMR-id: 68629\nCommit-by: csdntest13\nMerged-by: csdntest13\nE2E-issues: \nDescription: 提交信息\n\nSee merge request: One/One!56",
    "parent_ids": [
      "13956ffeb5e5e1ce60c2ed91d3e579fc50b1f167",
      "3e42dcb9c09b39972c65536dad71651322470f28"
    ],
    "authored_date": "2024-04-15T14:38:50.000Z",
    "author_name": "csdntest13",
    "author_iam_id": null,
    "author_email": "csdntest13@noreply.gitcode.com",
    "author_user_name": null,
    "committed_date": "2024-04-15T14:38:50.000Z",
    "committer_name": "csdntest13",
    "committer_email": "csdntest13@noreply.gitcode.com",
    "committer_user_name": null,
    "open_gpg_verified": null,
    "verification_status": null,
    "gpg_primary_key_id": null,
    "short_id": "b6d44deb",
    "created_at": "2024-04-15T14:38:50.000Z",
    "title": "merge refs/merge-requests/56/head into dev",
    "author_avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
    "committer_avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
    "relate_url": null
  },
  "merged": false,
  "protected": false,
  "developers_can_push": false,
  "developers_can_merge": false,
  "can_push": true,
  "default": false
}
```

### Demo

```bash
curl --location --request GET 'http://api.gitcode.com/api/v5/repos/dengmengmian/test03/branches/main?access_token={your-token}' \
```

## 4. 新建保护分支规则

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/branches/setting/new`

### 参数

| 参数名         | 描述                                                                                                 | 类型  | 数据类型 |
| -------------- | ---------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                           | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                                               | path  | string   |
| repo\*         | 仓库路径(path)                                                                                       | path  | string   |
| wildcard\*     | 分支/通配符                                                                                          | body  | string   |
| pusher\*       | 可推送代码成员。develop：仓库管理员和开发者；admin：仓库管理员；空字符串：禁止任何人推送             | body  | string   |
| merger\*       | 可合并 Pull Request 成员。developer：仓库管理员和开发者；admin：仓库管理员；空字符串：禁止任何人合并 | body  | string   |

### 响应

```html
HTTP status 200 No Content
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/branches/setting/new'?access_token={your-token} \
--header 'Content-Type: application/json' \
--data '{
    "wildcard":"dev12",
    "pusher":"zhanghq2;xiaogang;admin",
    "merger":"zhanghq2;xiaogang;admin"
}'
```

## 5. 删除保护分支规则

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/branches/{wildcard}/setting'`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| wildcard\*     | 分支名                                 | path  | string   |

```html
HTTP status 200 No Content
```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/tiandi/YanF4/branches/test/setting?access_token={your-token}' \
```

## 6. 获取保护分支规则列表

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/protect_branches`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |

### 响应

```json
[
  {
    "name": "main",
    "updated_at": "2024-09-18T10:15:48.471+08:00",
    "push_users": [],
    "merge_users": [],
    "merged": false,
    "developers_can_push": false,
    "developers_can_merge": false,
    "committer_can_push": false,
    "committer_can_merge": false,
    "master_can_push": true,
    "master_can_merge": true,
    "maintainer_can_push": true,
    "maintainer_can_merge": true,
    "owner_can_push": true,
    "owner_can_merge": true,
    "no_one_can_push": false,
    "no_one_can_merge": false
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/test-org/test-repo/protect_branches?access_token=token'
```

## 7. 更新保护分支规则

### 请求

`PUT /repos/{owner}/{repo}/branches/{wildcard}/setting`

### 参数

| 参数名         | 描述                                                                                                 | 类型  | 数据类型 |
| -------------- | ---------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                           | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                                               | path  | string   |
| repo\*         | 仓库路径(path)                                                                                       | path  | string   |
| wildcard\*     | 保护分支名                                                                                           | path  | string   |
| pusher\*       | 可推送代码成员。develop：仓库管理员和开发者；admin：仓库管理员；空字符串: 禁止任何人推送             | body  | string   |
| merger\*       | 可合并 Pull Request 成员。developer：仓库管理员和开发者；admin：仓库管理员；空字符串: 禁止任何人合并 | body  | string   |

### 响应

无

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/branches/dev4/setting?access_token=token' \
--header 'Content-Type: application/json' \
--data '{
    "pusher":"develop",
    "merger":"develop"
}'
```
