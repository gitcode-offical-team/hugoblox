---
linkTitle: Issues
title: Issues 接口文档
weight: 5
sidebar:
  open: false
---

## 1. 创建Issue

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/issues`

### 参数

| 参数名         | 描述                                                                        | 类型     | 数据类型 |
| -------------- | --------------------------------------------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                                                                  | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                      | path     | string   |
| repo\*         | 仓库路径                                                                    | formData | string   |
| title\*        | Issue标题                                                                   | formData | string   |
| body           | Issue描述                                                                   | formData | string   |
| assignee       | Issue负责人的username                                                       | formData | string   |
| milestone      | 里程碑序号                                                                  | formData | int      |
| labels         | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance | formData | string   |
| security_hole  | 是否是私有issue(默认为false)                                                | formData | string   |

### 响应

```json
{
  "id": 152642,
  "html_url": "https://test.gitcode.net/dengmengmian/test01/issues/15",
  "number": 15,
  "state": "opened",
  "title": "半月据",
  "body": "节油料被引系活力级少本化段维家住实。常气前步证时第样日所阶效温界到量。个导土机技亲布接增论始高世收圆流级集。此般区才听党机达两收文斗公加白。代军前分写第图美市与道及间。",
  "user": {
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
    "events_url": null,
    "followers_url": null,
    "following_url": null,
    "gists_url": null,
    "html_url": "https://test.gitcode.net/dengmengmian",
    "id": "661ce4eab470b1430d456154",
    "login": "dengmengmian",
    "member_role": null,
    "name": "麻凡_",
    "organizations_url": null,
    "received_events_url": null,
    "remark": null,
    "repos_url": null,
    "starred_url": null,
    "subscriptions_url": null,
    "type": null,
    "url": null
  },
  "assignee": null,
  "repository": {
    "id": 152642,
    "full_name": "dengmengmian/test01",
    "path": "test01",
    "name": "test01",
    "description": "",
    "created_at": "2024-04-18T14:35:15.479+08:00",
    "updated_at": "2024-04-18T14:35:15.479+08:00"
  },
  "created_at": "2024-04-18T14:35:15.479+08:00",
  "updated_at": "2024-04-18T14:35:15.479+08:00",
  "finished_at": null,
  "labels": [
    {
      "id": 382379,
      "name": "enim",
      "color": "#428BCA"
    },
    {
      "id": 382378,
      "name": "proident",
      "color": "#428BCA"
    },
    {
      "id": 382377,
      "name": "qui",
      "color": "#428BCA"
    }
  ],
  "stage": "New",
  "severity": "Major"
}
```

### Demo

```bash
curl --location --request 'https://api.gitcode.com/api/v5/repos/xiaogang_test/issues?access_token=token' \
--form 'title="title"' \
--form 'body="body"' \
--form 'repo="test222"'
```

## 2. 更新Issue

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/issues/{number}`

### 参数

| 参数名         | 描述                                                                        | 类型     | 数据类型 |
| -------------- | --------------------------------------------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                                                                  | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                      | path     | string   |
| repo\*         | 仓库路径                                                                    | formData | string   |
| number\*       | 第几个 issue，即本仓库 issue 的序数                                         | path     | string   |
| title\*        | Issue标题                                                                   | formData | string   |
| body           | Issue描述                                                                   | formData | string   |
| state          | Issue 状态，reopen（开启的）、close（关闭的）                               | formData | string   |
| assignee       | Issue负责人的 username                                                      | formData | string   |
| milestone      | 里程碑序号                                                                  | formData | int      |
| labels         | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance | formData | string   |
| security_hole  | 是否是私有issue(默认为false)                                                | formData | string   |

### 响应

```json
{
  "id": 152467,
  "html_url": "https://test.gitcode.net/dengmengmian/test01/issues/14",
  "number": 14,
  "state": "closed",
  "title": "取属且阶",
  "body": "速军间问备题意自系建技至速。那照与受证们老则使六么信。联不格决白转数特先到接单备心样本及。比论受感此中成要则片会受争里领周局。",
  "user": {
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
    "events_url": null,
    "followers_url": null,
    "following_url": null,
    "gists_url": null,
    "html_url": "https://test.gitcode.net/dengmengmian",
    "id": "661ce4eab470b1430d456154",
    "login": "dengmengmian",
    "member_role": null,
    "name": "麻凡_",
    "organizations_url": null,
    "received_events_url": null,
    "remark": null,
    "repos_url": null,
    "starred_url": null,
    "subscriptions_url": null,
    "type": null,
    "url": null
  },
  "assignee": {
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
    "events_url": null,
    "followers_url": null,
    "following_url": null,
    "gists_url": null,
    "html_url": "https://test.gitcode.net/dengmengmian",
    "id": "661ce4eab470b1430d456154",
    "login": "dengmengmian",
    "member_role": null,
    "name": "麻凡_",
    "organizations_url": null,
    "received_events_url": null,
    "remark": null,
    "repos_url": null,
    "starred_url": null,
    "subscriptions_url": null,
    "type": null,
    "url": null
  },
  "repository": {
    "id": 152467,
    "full_name": "dengmengmian/test01",
    "path": "test01",
    "name": "test01",
    "description": "",
    "created_at": "2024-04-16T14:38:43.464+08:00",
    "updated_at": "2024-04-18T18:27:21.955+08:00"
  },
  "created_at": "2024-04-16T14:38:43.464+08:00",
  "updated_at": "2024-04-18T18:27:21.955+08:00",
  "finished_at": "2024-04-16T14:49:45.166+08:00",
  "labels": [
    {
      "id": 382389,
      "name": "ad",
      "color": "#428BCA"
    },
    {
      "id": 382388,
      "name": "id",
      "color": "#428BCA"
    }
  ],
  "stage": "New",
  "severity": "Major"
}
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/xiaogang_test/issues/1?access_token=token' \
--form 'title="title"' \
--form 'body="body"' \
--form 'repo="test222"'
```

## 3. 获取仓库的某个Issue

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | Issue 编号(区分大小写，无需添加 # 号)  | path  | string   |

### 响应

```json
{
  "id": 152212,
  "html_url": "https://test.gitcode.net/dengmengmian/test01/issues/3",
  "number": 3,
  "state": "opened",
  "title": "查员种金交片",
  "body": "而很资七图数指反系并物众示易今高。运边月发红条亲才调二心点上米面世其分。由众计比维选作小指件每酸一见基历。向九又中国层合感内两米或自很转的。",
  "user": {
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
    "events_url": null,
    "followers_url": null,
    "following_url": null,
    "gists_url": null,
    "html_url": "https://test.gitcode.net/dengmengmian",
    "id": "661ce4eab470b1430d456154",
    "login": "dengmengmian",
    "member_role": null,
    "name": "麻凡_",
    "organizations_url": null,
    "received_events_url": null,
    "remark": null,
    "repos_url": null,
    "starred_url": null,
    "subscriptions_url": null,
    "type": null,
    "url": null
  },
  "assignee": null,
  "repository": {
    "id": 280713,
    "full_name": "dengmengmian / test01",
    "path": "test01",
    "name": "test01",
    "description": "",
    "created_at": "2024-04-15T16:27:45.090+08:00",
    "updated_at": "2024-04-15T16:27:45.090+08:00",
    "assigner": null,
    "pushed_at": null,
    "paas": null,
    "assignees_number": null,
    "testers_number": null,
    "assignee": null,
    "testers": null
  },
  "created_at": "2024-04-15T21:58:21.188+08:00",
  "updated_at": "2024-04-15T21:58:21.188+08:00",
  "finished_at": null,
  "labels": [],
  "priority": null,
  "issue_type": null,
  "issue_state": "opened",
  "issue_state_detail": null,
  "stage": "New",
  "severity": "Major"
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1?access_token=token'
```

## 4. 获取仓库所有 issues

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues`

### 参数

| 参数名         | 描述                                                            | 类型  | 数据类型 |
| -------------- |---------------------------------------------------------------| ----- | -------- |
| access_token\* | 用户授权码                                                         | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                        | path  | string   |
| repo\*         | 仓库路径(path)                                                    | path  | string   |
| state          | Issue的状态: open（开启的）, closed（关闭的）， all (所有)。 默认: all           | query | string   |
| labels         | 用逗号分开的标签。如: bug,performance                                   | query | string   |
| sort           | 排序依据: 创建时间(created)，更新时间(updated)。默认: created                 | query | string   |
| direction      | 排序方式: 升序(asc)，降序(desc)。默认: desc                               | query | string   |
| since          | 起始的更新时间，要求时间格式为 2024-11-10T08:10:30.000+08:00（注意+号要url编码为%2B） | query | string   |
| page           | 当前的页码                                                         | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                           | query | int      |
| created_at     | 任务创建时间，例如：2024-11-20T13:00:21+08:00                           | query | string   |
| milestone      | 根据里程碑标题。none为没里程碑的                                            | query | string   |
| assignee       | Issue指派人ID                                                    | query | string   |
| creator        | 创建Issues的用户username                                           | query | string   |
| created_after  | 返回在指定时间之后创建的问题，例如：2024-11-20T13:00:21+08:00                   | query | string   |
| created_before | 返回在指定时间之前创建的问题，例如：2024-11-20T13:00:21+08:00                   | query | string   |
| updated_after  | 返回在指定时间之后更新的问题，例如：2024-11-20T13:00:21+08:00                   | query | string   |
| updated_before | 返回在指定时间之前更新的问题，例如：2024-11-20T13:00:21+08:00                   | query | string   |

### 响应

##### header

| 头部名       | 描述                | 数据类型 |
| ------------ | ------------------- | -------- |
| total_count  | issue 总数          | Integer  |
| total_page   | 分页总数            | Integer  |
| all_issues   | 所有状态 issue 数   | Integer  |
| open_issues  | open 状态 issue 数  | Integer  |
| close_issues | close 状态 issue 数 | Integer  |

##### response body

```json
[
  {
    "id": 152642,
    "html_url": "https://test.gitcode.net/dengmengmian/test01/issues/15",
    "number": "15",
    "state": "opened",
    "title": "半月据",
    "body": "节油料被引系活力级少本化段维家住实。常气前步证时第样日所阶效温界到量。个导土机技亲布接增论始高世收圆流级集。此般区才听党机达两收文斗公加白。代军前分写第图美市与道及间。",
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "assignee": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "repository": {
      "id": 280713,
      "full_name": "dengmengmian / test01",
      "path": "test01",
      "name": "test01",
      "description": "",
      "created_at": "2024-04-15T16:27:45.090+08:00",
      "updated_at": "2024-04-15T16:27:45.090+08:00",
      "assigner": null,
      "pushed_at": null,
      "paas": null,
      "assignees_number": null,
      "testers_number": null,
      "assignee": null,
      "testers": null
    },
    "created_at": "2024-04-18T14:35:15.479+08:00",
    "updated_at": "2024-04-20T15:20:30.111+08:00",
    "finished_at": null,
    "labels": [
      {
        "id": 382379,
        "name": "enim",
        "color": "#428BCA"
      },
      {
        "id": 382378,
        "name": "proident",
        "color": "#428BCA"
      },
      {
        "id": 382377,
        "name": "qui",
        "color": "#428BCA"
      }
    ],
    "priority": null,
    "issue_type": null,
    "issue_state": "opened",
    "issue_state_detail": null
  },
  {
    "id": 152467,
    "html_url": "https://test.gitcode.net/dengmengmian/test01/issues/14",
    "number": "14",
    "state": "closed",
    "title": "取属且阶",
    "body": "速军间问备题意自系建技至速。那照与受证们老则使六么信。联不格决白转数特先到接单备心样本及。比论受感此中成要则片会受争里领周局。",
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "assignee": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "repository": {
      "id": 280713,
      "full_name": "dengmengmian / test01",
      "path": "test01",
      "name": "test01",
      "description": "",
      "created_at": "2024-04-15T16:27:45.090+08:00",
      "updated_at": "2024-04-15T16:27:45.090+08:00",
      "assigner": null,
      "pushed_at": null,
      "paas": null,
      "assignees_number": null,
      "testers_number": null,
      "assignee": null,
      "testers": null
    },
    "created_at": "2024-04-16T14:38:43.464+08:00",
    "updated_at": "2024-04-18T18:27:21.955+08:00",
    "finished_at": "2024-04-16T14:49:45.166+08:00",
    "labels": [
      {
        "id": 382389,
        "name": "ad",
        "color": "#428BCA"
      },
      {
        "id": 382388,
        "name": "id",
        "color": "#428BCA"
      }
    ],
    "priority": null,
    "issue_type": null,
    "issue_state": "closed",
    "issue_state_detail": null
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues?access_token=token'
```

## 5. 获取仓库某个Issue所有的评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/comments`

### 参数

| 参数名       | 描述                                                                                  | 类型  | 数据类型 |
| ------------ | ------------------------------------------------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                                                            | query | string   |
| owner\*      | 仓库所属空间地址(企业、组织或个人的地址path)                                          | path  | string   |
| repo\*       | 仓库路径(path)                                                                        | path  | string   |
| number\*     | Issue 编号(区分大小写，无需添加 # 号)                                                 | path  | string   |
| page         | 当前的页码                                                                            | query | int      |
| per_page     | 每页的数量，最大为 100，默认 20                                                       | query | int      |
| order        | 排序顺序: asc(default),desc                                                           | query | string   |
| since        | 起始的更新时间，要求时间格式为 2024-11-10T08:10:30.000+08:00（注意+号要url编码为%2B） | query | string   |

### 响应

```json
[
  {
    "id": 271624,
    "body": "评论内容。",
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "target": {
      "issue": {
        "id": 152134,
        "title": "",
        "nubmer": 1
      }
    },
    "created_at": "2024-04-19T17:50:18.199+08:00",
    "updated_at": "2024-04-19T17:50:18.199+08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1/comments?access_token=token'
```

## 6. 获取仓库所有 Iusse 评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/comments `

### 参数

| 参数名       | 描述                                                                                                                   | 类型  | 数据类型 |
| ------------ | ---------------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                                                                                             | query | string   |
| owner\*      | 仓库所属空间地址(企业、组织或个人的地址path)                                                                           | path  | string   |
| repo\*       | 仓库路径(path)                                                                                                         | path  | string   |
| sort         | Either created or updated. Default: created                                                                            | query | string   |
| direction    | Either asc or desc. Ignored without the sort parameter.                                                                | query | string   |
| since        | Only comments updated at or after this time are returned. This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | query | string   |
| page         | 当前的页码                                                                                                             | query | int      |
| per_page     | 每页的数量，最大为 100，默认 20                                                                                        | query | int      |

### 响应

```json
[
  {
    "id": 272201,
    "body": "daetete",
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "target": {
      "issue": {
        "id": 152642,
        "title": "半月据",
        "nubmer": 15
      }
    },
    "created_at": "2024-04-20T15:20:30.104+08:00",
    "updated_at": null
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/comments?access_token=token'
```

## 7. 获取 issue 关联的 pull requests

### 请求

`GET  https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/pull_requests`

### 参数

| 参数名       | 描述                                                                                | 类型    | 数据类型 |
| ------------ | ----------------------------------------------------------------------------------- |-------| -------- |
| access_token | 用户授权码                                                                          | query | string   |
| owner\*      | 仓库所属空间地址(企业、组织或个人的地址path)                                        | path  | string   |
| repo\*       | 仓库路径(path)                                                                      | path  | string   |
| number\*     | Issue 编号(区分大小写，无需添加 # 号)                                               | path  | string   |
| mode         | 1 (增强模式，传上述参数，返回 pr 的 mergeable 状态）; 0(默认，不返回mergeable 状态) | query | Integer  |

### 响应

```json
[
  {
    "id": 67585,
    "html_url": "https://api.gitcode.net/test/test/merge_requests/1",
    "diff_url": "https://api.gitcode.net/test/test/merge_requests/1/diffs",
    "number": 1,
    "state": "opened",
    "title": "1",
    "body": "new: 新增文件 test.txt 1",
    "created_at": "2024-04-12T17:50:55.253+08:00",
    "updated_at": "2024-04-20T15:58:30.657+08:00",
    "merged_at": null,
    "closed_at": null,
    "head": {
      "ref": "develop",
      "sha": "061c446d55aae78c7a0f096b2d2dd0d6a1afb170",
      "repo": {
        "path": "paopao1",
        "name": "paopao1"
      },
      "assigner": {
        "login": "test",
        "name": "test"
      }
    },
    "base": {
      "ref": "main",
      "sha": "667d4ac032b2faa13d019753ac218b4f78338273",
      "repo": {
        "path": "paopao1",
        "name": "paopao1"
      },
      "assigner": null
    },
    "assignees": [
      {
        "id": "65803cddcf1e2d1aa3d2e99f",
        "login": "test",
        "name": null,
        "avatar_url": null,
        "html_url": "https://api.gitcode.net/test"
      }
    ],
    "testers": [
      {
        "id": "65803cddcf1e2d1aa3d2e99f",
        "login": "test",
        "name": "test",
        "avatar_url": null,
        "html_url": "https://api.gitcode.net/test"
      }
    ],
    "labels": [
      {
        "id": 383707,
        "color": "#CCCCCC",
        "name": "wontfix",
        "repository_id": null,
        "url": null,
        "created_at": "2024-04-19",
        "updated_at": "2024-04-19",
        "text_color": "#333333"
      }
    ],
    "can_merge_check": true
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1/pull_requests?access_token=token'
```

## 8.获取企业某个Issue所有标签

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/issues/{issue_id}/labels`

### 参数

| 参数名       | 描述                            | 类型  | 数据类型 |
| ------------ | ------------------------------- | ----- | -------- |
| access_token | 用户授权码                      | query | string   |
| enterprise\* | 企业名(path)                    | path  | string   |
| issue_id\*   | issue 全局唯一 id               | path  | string   |
| page         | 当前的页码                      | query | int      |
| per_page     | 每页的数量，最大为 100，默认 20 | query | int      |

### 响应

```json
[
  {
    "color": "#008672",
    "name": "help wanted",
    "id": 381445,
    "url": ""
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/xiaogang_test/issues/1/labels?access_token=token'
```

## 9. 创建Issue标签

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | issue编号                              | path  | string   |
| labels\*       | 添加的标签 如: ["feat", "bug"]         | body  | array    |

### 响应

```json
[
  {
    "color": "#008672",
    "name": "help wanted",
    "id": 381445,
    "title": "help wanted",
    "type": null,
    "textColor": "#FFFFFF"
  }
]
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1/labels?access_token=token' \
--header 'Content-Type: application/json' \
--data '["bug","feat"]'
```

## 10. 删除Issue标签

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels/{name}`

### 参数

| 参数名         | 描述                                              | 类型  | 数据类型 |
| -------------- | ------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                        | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)            | path  | string   |
| repo\*         | 仓库路径(path)                                    | path  | string   |
| number\*       | 第几个Issue，即本仓库Issue的序数                  | path  | string   |
| name\*         | 标签名称(批量删除用英文逗号分隔，如: bug,feature) | path  | string   |

### 响应

`204`

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1/labels/bug?access_token=token'
```

## 11. 创建Issue评论

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/comments`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |
| number\*       | issue编号                              | path     | string   |
| body\*         | The contents of the comment.           | formdata | string   |

### 响应

```json
{
  "id": 271624,
  "body": "评论内容。",
  "user": {
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
    "events_url": null,
    "followers_url": null,
    "following_url": null,
    "gists_url": null,
    "html_url": "https://test.gitcode.net/dengmengmian",
    "id": "661ce4eab470b1430d456154",
    "login": "dengmengmian",
    "member_role": null,
    "name": "麻凡_",
    "organizations_url": null,
    "received_events_url": null,
    "remark": null,
    "repos_url": null,
    "starred_url": null,
    "subscriptions_url": null,
    "type": null,
    "url": null
  },
  "target": {
    "issue": {
      "id": 152134,
      "title": "",
      "nubmer": 1
    }
  },
  "created_at": null,
  "updated_at": null
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/issues/1/comments?access_token=token' \
--form 'body="1"'
```

## 12. 获取某个issue下的操作日志

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/issues/{number}/operate_logs`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | query | string   |
| number\*       | issue编号                              | path  | string   |

### 响应

```json
[
  {
    "id": 272199,
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "content": "Create issue mr links: **new: 新增文件 1.text** #1",
    "created_at": "2024-04-20T15:20:24.009+08:00",
    "action_type": "add_issue_mr_link",
    "update_at": "2024-04-20T15:20:24.009+08:00",
    "title": "new: 新增文件 1.text",
    "body": "new: 新增文件 1.text ",
    "head": {
      "ref": "develop",
      "sha": "dd954d3a779edc86dae5b4b60c7f24dd0f195bf4",
      "repo": {
        "path": "test01",
        "name": "test01"
      },
      "assigner": {
        "login": "dengmengmian",
        "name": "麻凡_"
      }
    },
    "base": {
      "ref": "main",
      "sha": "32cff0d8faaa0c044d0f94957e656051986e8403",
      "repo": {
        "path": "test01",
        "name": "test01"
      },
      "assigner": null
    },
    "issue_id": "152642"
  },
  {
    "id": 272198,
    "user": {
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/fa/fe/f32a9fecc53e890afbd48fd098b0f6c5f20f062581400c76c85e5baab3f0d5b2.png",
      "events_url": null,
      "followers_url": null,
      "following_url": null,
      "gists_url": null,
      "html_url": "https://test.gitcode.net/dengmengmian",
      "id": "661ce4eab470b1430d456154",
      "login": "dengmengmian",
      "member_role": null,
      "name": "麻凡_",
      "organizations_url": null,
      "received_events_url": null,
      "remark": null,
      "repos_url": null,
      "starred_url": null,
      "subscriptions_url": null,
      "type": null,
      "url": null
    },
    "content": "changed milestone to testew",
    "created_at": "2024-04-20T15:20:09.305+08:00",
    "action_type": "milestone",
    "update_at": "2024-04-20T15:20:09.305+08:00",
    "title": null,
    "body": null,
    "head": null,
    "base": null,
    "issue_id": "152642"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/xiaogang_test/issues/1/operate_logs?access_token=token'
```

## 13. 获取某个企业的所有Issues

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/issues`

### 参数

| 参数名         | 描述                                                                  | 类型  | 数据类型 |
| -------------- | --------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                            | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path）                        | path  | string   |
| enterprise\*   | 企业的路径(path/login)                                                | path  | string   |
| state          | Issue的状态: open（开启的）, closed（关闭的）, all（所有） 默认: open | query | string   |
| labels         | 用逗号分开的标签。如: bug,performance                                 | query | string   |
| sort           | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at   | query | string   |
| direction      | 排序方式: 升序(asc)，降序(desc)。默认: desc                           | query | string   |
| since          | 起始的更新时间，要求时间格式为 ISO 8601                               | query | string   |
| page           | 当前的页码                                                            | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                       | query | int      |
| milestone      | 根据里程碑标题。none为没里程碑的，\*为所有带里程碑的                  | query | string   |
| assignee       | 用户的username。 none为没指派者, \*为所有带有指派者的                 | query | string   |
| creator        | 创建Issues的用户username                                              | query | string   |
| program        | 所属项目名称。none为没所属有项目的，\*为所有带所属项目的              | query | string   |
| created_at     | 任务创建日期，格式2024-11-09                                          | query | string   |
| created_before | 任务创建截止时间，格式2024-11-09                                      | query | string   |

### 响应

```json
[
  {
    "id": 340035,
    "html_url": "https://gitcode.com/xiaogang_test/test222/issues/7",
    "number": "7",
    "state": "open",
    "title": "2222",
    "body": "2222",
    "user": {
      "id": "65f96506b3a9e65264980447",
      "login": "xiaogang",
      "name": "xiaogang"
    },
    "repository": {
      "id": 249609,
      "full_name": "xiaogang_test/test222",
      "human_name": "测试组织 / test222",
      "path": "test222",
      "name": "test222",
      "url": "https://gitcode.com/xiaogang_test/test222",
      "assigner": {},
      "paas": ""
    },
    "created_at": "2024-11-20T15:40:35+08:00",
    "updated_at": "2024-11-20T15:40:35+08:00",
    "labels": [],
    "issue_state": "未提单",
    "priority": 0,
    "issue_type": "需求",
    "issue_state_detail": {
      "title": "未提单",
      "serial": 1,
      "id": 222
    },
    "issue_type_detail": {
      "title": "需求",
      "id": 629,
      "is_system": false
    },
    "comments": 0,
    "parent_id": 0,
    "url": "https://gitcode.com/api/v5/repos/xiaogang_test/test222/issues/7"
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/enterprises/Hello_worldsss/issues?state=open&direction=desc&page=1&per_page=20&access_token=xxxx&sort=created_at' \
```

## 14. 获取授权用户的所有Issues

### 请求

`GET https://api.gitcode.com/api/v5/user/issues`

### 参数

| 参数名         | 描述                                                                                                                                                                                                        | 类型  | 数据类型 |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                                                                                                                                  | query | string   |
| filter         | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned                                                                                                              | query | string   |
| state          | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。默认: open                                                                                                          | query | string   |
| labels         | 用逗号分开的标签。如: bug,performance                                                                                                                                                                       | query | string   |
| sort           | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at                                                                                                                                         | query | string   |
| direction      | 排序方式: 升序(asc)，降序(desc)。默认: desc                                                                                                                                                                 | query | string   |
| since          | 起始的更新时间，要求时间格式为 ISO 8601                                                                                                                                                                     | query | string   |
| page           | 当前的页码                                                                                                                                                                                                  | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                                                                                                                                                             | query | int      |
| schedule       | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | query | string   |
| deadline       | 计划截止日期，格式同上                                                                                                                                                                                      | query | string   |
| created_at     | 任务创建时间，格式同上                                                                                                                                                                                      | query | string   |
| finished_at    | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上                                                                                                                                                | query | string   |

### 响应

```json
[
  {
    "id": 490786,
    "html_url": "https://gitcode.com/gitcode-dev/gitcode-TestTask/issues/319",
    "number": "319",
    "state": "open",
    "title": "评论中附件图片的大小从 2M 放开到 10M，允许用户上传更大尺寸的图片附件",
    "body": "### 提测内容\n            \n如题： https://gitcode.com/gitcode-dev/releases-log/issues/617\n### 需求文档地址\n            \nhttps://gitcode.com/gitcode-dev/releases-log/issues/617\n### UI设计图地址\n            \n无\n### 技术方案地址\n            \n无\n### 影响范围\n            \n\n### 涉及上线服务\n            \ngitcode-fe\n### 研发人员\n            \n刘奥林\n### pr文件改动diffs\n            \nhttps://gitcode.com/gitcode-dev/gitcode-fe/merge_requests/3661/diffs\n### 依赖项\n            \n\n### API调整\n            \n\n### 数据库调整\n            \n\n### nacos配置调整\n            \n\n### 运维调整\n            \n\n",
    "assignee": {
      "avatar_url": "https://cdn-img.gitcode.com/db/cf/dbc07f37245cab6693ef7a3ba7eb101634f480c263fa294f6b366e4a0fe60a45.png?time=1720429708203",
      "html_url": "https://gitcode.com/yinlin",
      "id": "303745",
      "login": "yinlin",
      "name": "yinlin-昵称",
      "type": "User"
    }
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/issues?access_token=xxx=created_at&direction=desc&page=1&per_page=20' \
```

## 15. 更新Issue某条评论

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |
| id\*           | 评论 ID                                | path     | string   |
| body\*         |                                        | formDate | string   |

### 响应

无

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/Hello_worldsss/IK_001_01/pulls/comments/1478704?access_token=xxx' \
--header 'Content-Type: application/json' \
--data '{
    "body":"0913更新评论"
}'
```

## 16. 删除Issue某条评论

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论 ID                                | path  | string   |

### 响应

```json

```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/Hello_worldsss/IK_001_01/issues/comments/1486664?access_token=xxx'
```

## 17. 获取仓库Issue某条评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论 ID                                | path  | string   |

### 响应

```json
{
  "id": 1495484,
  "body": "测试 issue 评论",
  "comment_type": "DiscussionNote",
  "user": {
    "id": "268",
    "login": "dengmengmian",
    "name": "麻凡",
    "type": "User"
  },
  "target": {
    "issue": {
      "id": 494561,
      "title": "测试 issue 评论",
      "number": "494561"
    }
  },
  "created_at": "2024-10-08T19:52:19+08:00",
  "updated_at": "2024-10-08T19:52:19+08:00"
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/issues/comments/1495484?access_token=yuBy'
```

## 18. 获取当前用户某个组织的Issues

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}/issues`

### 参数

| 参数名         | 描述                                                                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                     | query | string   |
| filter         | 筛选参数：授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认：assigned | query | string   |
| state          | Issue的状态：open（开启的）, closed（关闭的）,默认：open                                       | query | string   |
| labels         | 用逗号分开的标签。如：bug,performance                                                          | query | string   |
| sort           | 排序依据：创建时间(created)，更新时间(updated_at)。默认：created_at                            | query | string   |
| direction      | 排序方式：升序(asc)，降序(desc)。默认：desc                                                    | query | string   |
| page           | 当前的页码                                                                                     | query | integer  |
| per_page       | 每页的数量，最大为 100，默认 20                                                                | query | integer  |
| created_at     | 任务创建时间，格式同上                                                                         | query | string   |
| org\*          | 组织的路径(path/login)                                                                         | path  | string   |

### 响应

```json
[
  {
    "id": 495900,
    "html_url": "https://gitcode.com/Go-Tribe/test01/issues/1",
    "number": "1",
    "state": "open",
    "title": "1",
    "body": "1",
    "repository": {
      "id": 4016571,
      "full_name": "Go-Tribe/test01",
      "human_name": "gotribe / test01",
      "path": "test01",
      "name": "test01",
      "url": "https://gitcode.com/Go-Tribe/test01",
      "owner": {
        "id": "650d67fbae6d795139b49b41",
        "login": "dengmengmian",
        "name": "麻凡"
      }
    },
    "created_at": "2024-10-12T18:27:27+08:00",
    "updated_at": "2024-10-12T18:27:27+08:00",
    "labels": [],
    "priority": 0,
    "comments": 0,
    "parent_id": 0
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/orgs/go-tribe/issues?filter=all&page=1&per_page=2&state=all&sort=created_at&access_token=token' \
--header 'Content-Type: application/json' \
--data-raw '{}'
```

## 19. 获取企业某个Issue所有评论

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/issues/{number}/comments`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| enterprise\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| number\*       | issue 全局唯一 id                      | path  | int      |
| page           | 当前的页码                             | query | int      |
| per_page       | 每页的数量：最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "body": "etst",
    "created_at": "2024-12-10T16:02:21+08:00",
    "id": 1535981,
    "target": {
      "issue": {
        "id": 471521,
        "iid": 1,
        "title": "bbbbb"
      }
    },
    "updated_at": "2024-12-10T16:02:21+08:00",
    "user": {
      "id": 287,
      "login": "csdn_fenglh",
      "name": "fenglh",
      "type": "User"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/owner-test/issues/471521/comments?access_token=your_token'
```

## 20. 获取企业的某个Issue

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/issues/{number}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| enterprise\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| number\*       | issue 全局唯一 id                      | path  | int      |


### 响应

```json
{
    "id": 471521,
    "html_url": "https://test.gitcode.net/owner-test/wonderful1/issues/1",
    "number": "1",
    "state": "open",
    "title": "bbbbb",
    "body": "sdfsdf",
    "user": {
        "html_url": "https://test.gitcode.net/csdn_fenglh",
        "id": "654c61e5560ed95fd216cf31",
        "login": "csdn_fenglh",
        "name": "fenglh"
    },
    "repository": {
        "id": 686738,
        "full_name": "owner-test/wonderful1",
        "path": "wonderful1",
        "name": "wonderful1",
        "description": "我的测试代码仓库",
        "created_at": "2024-10-16T15:51:35+08:00",
        "updated_at": "2024-10-16T15:51:35+08:00",
        "assigner": {},
        "paas": ""
    },
    "created_at": "2024-12-10T16:02:12+08:00",
    "updated_at": "2024-12-10T16:02:21+08:00",
    "finished_at": "",
    "labels": [],
    "issue_state": "待办的",
    "priority": 0,
    "issue_state_detail": {
        "title": "待办的",
        "serial": 0
    }
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/owner-test/issues/471521?access_token=your_token'
```
