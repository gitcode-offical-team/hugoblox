---
linkTitle: Commit
title: 提交接口文档
weight: 3
sidebar:
  open: false
---

## 1. 获取仓库所有提交

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/commits`

### 参数

| 参数名       | 描述                                               | 类型  | 数据类型 |
| ------------ | -------------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                         | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path）     | path  | string   |
| repo\*       | 仓库路径(path)                                     | path  | string   |
| sha          | 提交起始的SHA值                                    | query | string   |
| path         | 包含该文件的提交                                   | query | string   |
| author       | 提交作者的邮箱或个人空间地址(username/login)       | query | string   |
| since        | 提交的起始时间，时间格式（2024-11-08T16:25:44Z）   | query | string   |
| until        | 提交的最后时间，时间格式为（2024-11-08T16:25:44Z） | query | string   |

### 响应

```json
[
  {
    "url": "https://api.gitcode.com/api/v5/repos/mactribe/test02/commits/66a12f572722ebe2cad44f44d48e253a0f027e09",
    "sha": "66a12f572722ebe2cad44f44d48e253a0f027e09",
    "html_url": "https://test.gitcode.net/mactribe/test02/commits/detail/66a12f572722ebe2cad44f44d48e253a0f027e09",
    "comments_url": "https://api.gitcode.com/api/v5/repos/mactribe/test02/commits/66a12f572722ebe2cad44f44d48e253a0f027e09/comments",
    "commit": {
      "author": {
        "date": "2024-09-25T08:14:59+08:00",
        "email": "my@dengmengmian.com"
      },
      "committer": {
        "date": "2024-09-25T08:14:59+08:00",
        "email": "my@dengmengmian.com"
      },
      "tree": {
        "sha": "6214a5921eda7f5148f249587b74201d5946a6d4",
        "url": "https://api.gitcode.com/api/v5/repos/mactribe/test02/git/trees/6214a5921eda7f5148f249587b74201d5946a6d4"
      },
      "message": "feat:所有issue number 都改为 string 类型"
    },
    "author": {
      "email": "my@dengmengmian.com",
      "login": "dengmengmian(麻凡)",
      "type": "User"
    },
    "committer": {
      "date": "2024-09-25T08:14:59+08:00",
      "email": "my@dengmengmian.com",
      "type": "User"
    },
    "parents": [
      {
        "sha": "0c41318014c472534a2abc2e0aa498fd49d046f1",
        "url": "https://api.gitcode.com/api/v5/repos/mactribe/test02/commits/0c41318014c472534a2abc2e0aa498fd49d046f1"
      }
    ]
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/mactribe/test02/commits?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data-raw '{}'
```

## 2. 仓库的某个提交

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/commits/{sha}`

| 参数名         | 描述                                                                                    | 类型  | 数据类型 |
| -------------- | --------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                              | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                                  | path  | string   |
| repo\*         | 仓库路径(path)                                                                          | path  | string   |
| sha\*          | 提交的SHA值或者分支名                                                                   | path  | string   |
| show_diff      | 默认为 false; 为 true 时，返回files字段, files字段中包含本地提交中的至多100个变更文件名 | query | boolean  |

### 返回

```json
{
  "url": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/commits/7ffc0a0deb709143f6be12a55e218fab9233ca37",
  "sha": "7ffc0a0deb709143f6be12a55e218fab9233ca37",
  "html_url": "https://gitcode.com/daming_1/zhu_di/commit/7ffc0a0deb709143f6be12a55e218fab9233ca37",
  "comments_url": "https://gitcode.com/api/v5/repos/daming_1/zhu_di/commits/7ffc0a0deb709143f6be12a55e218fab9233ca37/comments",
  "commit": {
    "author": {
      "name": "占分",
      "date": "2024-04-14T07:25:11+00:00",
      "email": "7543745+centking@user.noreply.gitcode.com"
    },
    "committer": {
      "name": "Gitee",
      "date": "2024-04-14T07:25:11+00:00",
      "email": "noreply@gitcode.com"
    },
    "message": "提交信息测试"
  },
  "stats": {
    "additions": 1,
    "deletions": 1,
    "total": 2
  },
  "files": [
    {
      "filename": "pom.xml",
      "raw_url": "https://raw.gitcode.com/test-owner/test-repo/raw/b0e267d8bbed9b568623528c216f8f8489689a61/pom.xml",
      "content_url": "https://gitcode.com/test-owner/test-repo/blob/b0e267d8bbed9b568623528c216f8f8489689a61/pom.xml"
    }
  ]
}
```

### Demo

```bash

curl --location 'https://api.gitcode.com/api/v5/repos/test-owner/test-repo/commits/786eae93d33ca08fae6962b2b04e54f40bbe3b3c?access_token=token&show_diff=true'

```

## 3. Commits对比

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/compare/{base}...{head}`

| 参数名       | 描述                                            | 类型  | 数据类型 |
| ------------ | ----------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                      | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path）  | path  | string   |
| repo\*       | 仓库路径(path)                                  | path  | string   |
| base\*       | 对比的起点。Commit SHA、分支名或标签名          | path  | string   |
| head\*       | 对比的终点。Commit SHA、分支名或标签名          | path  | string   |
| straight     | 是否直对比。默认 false                          | query | boolean  |
| suffix       | 按照文件后缀过滤文件，如 `.txt`。只影响 `files` | query | string   |

### 响应

```json
{
  "base_commit": {
    "url": "https://api.gitcode.com/api/v5/repos/owner-test/secure-issue/commits/17922544024484084e2b6218eb0d46d76f354ffa",
    "sha": "17922544024484084e2b6218eb0d46d76f354ffa",
    "html_url": "https://test.gitcode.net/owner-test/secure-issue/commit/17922544024484084e2b6218eb0d46d76f354ffa",
    "comments_url": "https://test.gitcode.net/api/v5/repos/owner-test/secure-issue/commits/17922544024484084e2b6218eb0d46d76f354ffa/comments",
    "commit": {
      "author": {
        "name": "malongge5",
        "date": "2024-08-13T11:34:35Z",
        "email": "malongge5@noreply.gitcode.com"
      },
      "committer": {
        "name": "malongge5",
        "date": "2024-08-13T11:34:35Z",
        "email": "malongge5@noreply.gitcode.com"
      },
      "tree": {
        "sha": "4e12f62b7fe2f78cb7c1b088e8f7e797a8c898a3",
        "url": "https://api.gitcode.com/repos/owner-test/secure-issue/git/trees/4e12f62b7fe2f78cb7c1b088e8f7e797a8c898a3"
      },
      "message": "new: 新增文件 test.py \n "
    },
    "author": {
      "name": "malongge5",
      "id": 953,
      "login": "malongge5",
      "type": "User"
    },
    "committer": {
      "name": "malongge5",
      "id": 953,
      "login": "malongge5",
      "type": "User"
    },
    "parents": [
      {
        "sha": "7ea40eef5f96c3f263f17438b294a6ea9b771cc5",
        "url": "https://api.gitcode.com/api/v5/repos/owner-test/secure-issue/commits/7ea40eef5f96c3f263f17438b294a6ea9b771cc5"
      }
    ]
  },
  "merge_base_commit": {
    "url": "https://api.gitcode.com/api/v5/repos/owner-test/secure-issue/commits/17922544024484084e2b6218eb0d46d76f354ffa",
    "sha": "17922544024484084e2b6218eb0d46d76f354ffa",
    "html_url": "https://test.gitcode.net/owner-test/secure-issue/commit/17922544024484084e2b6218eb0d46d76f354ffa",
    "comments_url": "https://test.gitcode.net/api/v5/repos/owner-test/secure-issue/commits/17922544024484084e2b6218eb0d46d76f354ffa/comments",
    "commit": {
      "author": {
        "name": "malongge5",
        "date": "2024-08-13T11:34:35Z",
        "email": "malongge5@noreply.gitcode.com"
      },
      "committer": {
        "name": "malongge5",
        "date": "2024-08-13T11:34:35Z",
        "email": "malongge5@noreply.gitcode.com"
      },
      "tree": {
        "sha": "4e12f62b7fe2f78cb7c1b088e8f7e797a8c898a3",
        "url": "https://api.gitcode.com/repos/owner-test/secure-issue/git/trees/4e12f62b7fe2f78cb7c1b088e8f7e797a8c898a3"
      },
      "message": "new: 新增文件 test.py \n "
    },
    "author": {
      "name": "malongge5",
      "id": 953,
      "login": "malongge5",
      "type": "User"
    },
    "committer": {
      "name": "malongge5",
      "id": 953,
      "login": "malongge5",
      "type": "User"
    },
    "parents": [
      {
        "sha": "7ea40eef5f96c3f263f17438b294a6ea9b771cc5",
        "url": "https://api.gitcode.com/api/v5/repos/owner-test/secure-issue/commits/7ea40eef5f96c3f263f17438b294a6ea9b771cc5"
      }
    ]
  },
  "commits": [
    {
      "sha": "97fd5a05e18bcd5b633a279fd7d395784d272321",
      "commit": {
        "author": {
          "name": "malongge5",
          "date": "2024-09-09T07:29:23Z",
          "email": "malongge5@noreply.gitcode.com"
        },
        "committer": {
          "name": "malongge5",
          "date": "2024-09-09T07:29:23Z",
          "email": "malongge5@noreply.gitcode.com"
        },
        "message": "new: 新增文件 bbb.rs \n "
      },
      "author": {
        "name": "malongge5",
        "id": 953,
        "login": "malongge5"
      },
      "committer": {
        "name": "malongge5",
        "id": 953,
        "login": "malongge5"
      }
    }
  ],
  "files": [
    {
      "sha": "6533e5c4585eb91faa9331b8de6b22f9ff31d387",
      "filename": "bbb.rs",
      "status": "added",
      "additions": 3,
      "deletions": 0,
      "changes": 3,
      "blob_url": "https://test.gitcode.net/owner-test/secure-issue/blob/6533e5c4585eb91faa9331b8de6b22f9ff31d387/bbb.rs",
      "raw_url": "https://test.gitcode.net/owner-test/secure-issue/raw/6533e5c4585eb91faa9331b8de6b22f9ff31d387/bbb.rs",
      "patch": "@@ -0,0 +1,3 @@\n+\n+\n+println!(\"hello\")\n\\ No newline at end of file\n",
      "truncated": false
    }
  ],
  "truncated": false
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com//api/v5/repos/owner-test/secure-issue/compare/17922544024484084e2b6218eb0d46d76f354ffa...97fd5a05e18bcd5b633a279fd7d395784d272321?access_token=xxxx'
```

## 4. 创建commit评论

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/commits/{sha}/comments`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| sha\*          | commit的id                             | path  | string   |
| body\*         | 评论内容                               | body  | string   |

### 响应

```json
{
  "id": "12312sadsa",
  "body": "content",
  "created_at": "2024-03-28T11:19:33+08:00",
  "updated_at": "2024-03-28T11:19:33+08:00"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/commits/7b8ee8f1046519582a2765e55c2b91288dd197de/comments?access_token' \
--header 'Content-Type: application/json' \
--data '{
    "body":"1111"
}'
```

## 5. 删除commit评论

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论id                                 | path  | string   |

### 响应

无

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/comments/1492393?access_token=?'
```

## 6. 获取仓库的Commit评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/comments`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| page           | 当前的页码                             | query | integer  |
| per_page       | 每页的数量，最大为 100，默认 20        | query | integer  |
| order          | 排序顺序: asc(default),desc            | query | string   |

### 响应

```json
{
  "body": "",
  "created_at": "2024-11-06T09:43:23+08:00",
  "id": 1492393,
  "target": {},
  "updated_at": "2024-11-06T15:18:04+08:00",
  "user": {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "type": "User"
  }
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/comments?access_token=?'
```

## 7. 获取仓库的某条Commit评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论id                                 | path  | string   |

### 响应

```json
{
  "body": "阿福时间看风景",
  "created_at": "2024-11-06T09:43:23+08:00",
  "id": 1492393,
  "target": {},
  "updated_at": "2024-11-06T15:18:04+08:00",
  "user": {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "type": "User"
  }
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/comments/1507446?access_token=?'
```

## 8. 更新Commit评论

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论id                                 | path  | string   |
| body\*         | 评论内容                               | body  | string   |

### 响应

```json
{
  "body": "",
  "created_at": "2024-11-06T09:43:23+08:00",
  "id": 1492393,
  "target": {},
  "updated_at": "2024-11-14T18:44:53+08:00",
  "user": {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "type": "User"
  }
}
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/comments/1507446?access_token=?' \
--header 'Content-Type: application/json' \
--data '{
    "body":"body"
}'
```

## 9. 获取代码量贡献

### 请求

`GET https://api.gitcode.com/api/v5/{owner}/{repo}/repository/commit_statistics`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| branch_name\*  | 分支名                                 | query | string   |
| author         | 作者用户名                             | query | string   |
| only_self      | 查询个人                               | query | Boolean  |
| since          | 仅返回此日期及以后的提交               | query | string   |
| until          | 仅返回此日期及之前的提交               | query | string   |

** 注：author 优先级高于 only_self **

### 响应

```json
{
    "commits": [
        {
            "author_name": "tester",
            "date": "2024-11-12"
        },
        ... ...,
        {
            "author_name": "tester",
            "date": "2024-01-15"
        }
    ],
    "statistics": [
        {
            "project_id": 1359,
            "branch": "main",
            "user_name": "tester",
            "nick_name": "测试",
            "add_lines": 4370,
            "delete_lines": 351,
            "commit_count": 123
        }
    ],
    "total": 1
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/gitcode-repo/hub-service/repository/commit_statistics?branch_name=main&access_token=your_token&only_self=true&since=2024-11-11%2010%3A45%3A53'
```

## 10. 获取单个commit评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/commits/{ref}/comments`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| ref\*          | commit的sha                            | path  | string   |
| page           | 当前的页码                             | query | integer  |
| per_page       | 每页的数量，最大为 100，默认 20        | query | integer  |

### 响应

```json
[
  {
    "body": "11111",
    "created_at": "2024-11-19T11:22:50+08:00",
    "id": 13837749,
    "updated_at": "2024-11-19T11:22:50+08:00",
    "user": {
      "id": 173794,
      "login": "xiaogang",
      "name": "肖刚",
      "type": "User"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/comments/1507446?access_token=?'
```
