---
linkTitle: Labels
title: 标签接口文档
weight: 7
sidebar:
  open: false
---

## 1. 更新一个仓库的任务标签

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/labels/{original_name}`

| 参数名          | 描述                                           | 类型     | 数据类型 |
| --------------- | ---------------------------------------------- | -------- | -------- |
| access_token\*  | 用户授权码                                     | query    | string   |
| owner\*         | 仓库所属空间地址（企业、组织或个人的地址path） | path     | string   |
| repo\*          | 仓库路径(path)                                 | path     | string   |
| original_name\* | 标签原有名称                                   | path     | string   |
| name            | 标签新名称                                     | formData | string   |
| color           | 标签新颜色                                     | formData | string   |

### 响应

```json
{
  "id": 12738100,
  "name": "list",
  "color": "#ED4014"
}
```

### Demo

```bash
curl --location -g --request PATCH 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/labels/bug?access_token=xxxx' \
--form 'name="list"'
```

## 2. 获取仓库所有任务标签

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/labels`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |

### 响应

```json
[
  {
    "id": 12738107,
    "name": "wontfix",
    "color": "#CCCCCC",
    "repository_id": 4066481
  },
  {
    "id": 12738106,
    "name": "question",
    "color": "#D876E3",
    "repository_id": 4066481
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/labels?access_token=xxxxx'
```

## 3. 创建仓库任务标签

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/labels`

| 参数名         | 描述                                           | 类型     | 数据类型 |
| -------------- | ---------------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                                     | query    | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path     | string   |
| repo\*         | 仓库路径(path)                                 | path     | string   |
| name\*         | 标签新名称                                     | formData | string   |
| color\*        | 标签新颜色.如#fff                              | formData | string   |

### 响应

```json
{
  "id": 12738109,
  "name": "测试1",
  "color": "#fff"
}
```

### Demo

```bash
curl --location -g --request POST 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/labels?access_token=xxxx' \
--form 'name="测试"' \
--form 'color="#fff"'
```

## 4. 删除一个仓库任务标签

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/labels/{name}`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| name\*         | 标签名称                                       | path  | string   |

### 响应

无

### Demo

```bash
curl --location -g --request DELETE 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/labels/list?access_token=yuBy'
```

## 5. 删除Issue所有标签

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| number\*       | Issue 编号(区分大小写，无需添加 # 号)          | path  | string   |

### 响应

无

### Demo

```bash
curl --location -g --request DELETE 'https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels?access_token=token'
```

## 6. 替换Issue所有标签

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| owner\*        | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*         | 仓库路径(path)                                 | path  | string   |
| number\*       | Issue 编号(区分大小写，无需添加 # 号)          | path  | string   |
| body\*         | 标签名数组，如: ["feat", "bug"]                | body  | array    |

### 响应

无

### Demo

```bash
curl --location -g --request PUT 'https://api.gitcode.com/api/v5/repos/{owner}/{repo}/issues/{number}/labels?access_token=token'\
--header 'Content-Type: application/json' \
--data-raw '["feat", "bug"]'
```

## 7. 获取企业所有的标签

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/labels`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| enterprise\*   | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |

### 响应

```json
[
  {
    "id": 471,
    "name": "aaaaa",
    "color": "#2865E0",
    "created_at": "2024-11-22T11:25:36.769+08:00",
    "updated_at": "2024-11-22T11:25:36.769+08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/xiaogang_test/labels?access_token=token'
```

## 8. 获取标签列表

### 请求

`GET https://api.gitcode.com/api/v8/enterprises/{enterprise}/labels`

| 参数名         | 描述                                           | 类型  | 数据类型 |
| -------------- | ---------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                     | query | string   |
| enterprise\*   | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| search         | 关键词查询                                     | query | string   |
| direction      | 排序方向 asc/desc                              | query | string   |
| page           | 当前的页码                                     | query | string   |
| per_page       | 每页的数量，最大为 100，默认 20                | query | string   |

### 响应

```json
[
  {
    "id": 382218,
    "name": "bug",
    "color": "#e03529"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v8/enterprises/xiaogang_test/labels?access_token=token'
```
