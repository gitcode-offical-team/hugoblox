---
linkTitle: Webhooks
title: Webhook 接口文档
weight: 9
sidebar:
  open: false
---

## 1. 列出仓库的WebHooks

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks`

### 参数

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| page         | 当前的页码                                     | query | int      |
| per_page     | 每页的数量，最大为 100，默认 20                | query | int      |

### 响应

```json
[
  {
    "id": 9523,
    "url": "http://duxwsqdkyx.cu/pxssss",
    "password": "123445",
    "result": "not found",
    "project_id": 282463,
    "result_code": 503,
    "push_events": false,
    "tag_push_events": false,
    "issues_events": true,
    "note_events": false,
    "merge_requests_events": true,
    "created_at": "2024-09-18T17:51:44+08:00"
  }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks?access_token={your-token}'
```

## 2. 创建一个仓库的WebHooks

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks`

### 参数

| 参数名                | 描述                                           | 类型     | 数据类型 |
| --------------------- | ---------------------------------------------- | -------- | -------- |
| access_token          | 用户授权码                                     | formData | string   |
| owner\*               | 仓库所属空间地址（企业、组织或个人的地址path） | path     | string   |
| repo\*                | 仓库路径(path)                                 | path     | string   |
| url\*                 | 远程HTTP URL                                   | formData | string   |
| encryption_type       | 加密类型: 0: 密码, 1: 签名密钥                 | formData | int      |
| password              | 请求URL时会带上该密码，防止URL被恶意请求       | formData | string   |
| push_events           | Push代码到仓库                                 | formData | boolean  |
| tag_push_events       | 提交Tag到仓库                                  | formData | boolean  |
| issues_events         | 创建/关闭Issue                                 | formData | boolean  |
| note_events           | 评论了Issue/代码等等                           | formData | boolean  |
| merge_requests_events | 合并请求和合并后                               | formData | boolean  |

### 响应

```json
{
  "id": 9529,
  "url": "http://duxwsqdkyx.cu/pxddddd",
  "password": "123445",
  "result": null,
  "project_id": 282463,
  "result_code": 0,
  "push_events": false,
  "tag_push_events": false,
  "issues_events": true,
  "note_events": false,
  "merge_requests_events": true,
  "created_at": "2024-09-26T16:13:27+08:00"
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "url": "http://duxwsqdkyx.cu/px",
    "encryption_type": 0,
    "password": "123445",
    "push_events": false,
    "tag_push_events": false,
    "issues_events": true,
    "note_events": false,
    "merge_requests_events": true
}'
```

## 3. 获取仓库单个WebHook

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks/{id}`

### 参数

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| id\*         | Webhook的ID                                    | path  | string   |

### 响应

```json
{
  "id": 9529,
  "url": "http://duxwsqdkyx.cu/pxddddd",
  "password": "123445",
  "result": null,
  "project_id": 282463,
  "result_code": 0,
  "push_events": false,
  "tag_push_events": false,
  "issues_events": true,
  "note_events": false,
  "merge_requests_events": true,
  "created_at": "2024-09-26T16:13:27+08:00"
}
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks/9524?access_token={your-token}'
```

## 4. 更新一个仓库WebHook

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks/{id}`

### 参数

| 参数名                | 描述                                           | 类型     | 数据类型 |
| --------------------- | ---------------------------------------------- | -------- | -------- |
| access_token          | 用户授权码                                     | query    | string   |
| owner\*               | 仓库所属空间地址（企业、组织或个人的地址path） | path     | string   |
| repo\*                | 仓库路径(path)                                 | path     | string   |
| id\*                  | Webhook的ID                                    | path     | string   |
| url\*                 | 远程HTTP URL                                   | formData | string   |
| encryption_type       | 加密类型: 0: 密码, 1: 签名密钥                 | formData | int      |
| password              | 请求URL时会带上该密码，防止URL被恶意请求       | formData | string   |
| push_events           | Push代码到仓库                                 | formData | boolean  |
| tag_push_events       | 提交Tag到仓库                                  | formData | boolean  |
| issues_events         | 创建/关闭Issue                                 | formData | boolean  |
| note_events           | 评论了Issue/代码等等                           | formData | boolean  |
| merge_requests_events | 合并请求和合并后                               | formData | boolean  |

### 响应

```json
{
  "url": "http://duxwsqdkyx.cu/pxddddd",
  "password": "123445",
  "result": null,
  "project_id": 282463,
  "result_code": 0,
  "push_events": false,
  "tag_push_events": false,
  "issues_events": true,
  "note_events": false,
  "merge_requests_events": true,
  "created_at": "2024-09-26T16:13:27+08:00"
}
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks/9516?access_token={your-token}' \
--header 'Content-Type: application/json' \
--data-raw '{
    "url": "http://duxwsqdkyx.cu/dengmm",
    "encryption_type": 0,
    "password": "334455",
    "push_events": true,
    "tag_push_events": true,
    "issues_events": false,
    "note_events": true,
    "merge_requests_events": true
}'
```

## 5. 删除一个仓库WebHook

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks/{id}`

### 参数

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| id\*         | Webhook的ID                                    | path  | string   |

### 响应

Http Code: 204, No Content

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks/9516?access_token={your-token}'
```

## 6. 测试WebHook是否发送成功

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/hooks/{id}/tests`

### 参数

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| id\*         | Webhook的ID                                    | path  | string   |

### 响应

Http Code: 204, No Content

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/mactribe/test02/hooks/9516/tests?access_token={your-token}'
```
