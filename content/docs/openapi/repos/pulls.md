---
linkTitle: Pull Requests
title: PR接口文档
weight: 6
sidebar:
  open: false
---

## 1. 获取Pull Request列表

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls`

### 参数

| 参数名           | 描述                                                               | 类型  | 数据类型 |
| ---------------- |------------------------------------------------------------------| ----- | -------- |
| access_token\*   | 用户授权码                                                            | query | string   |
| owner\*          | 仓库所属空间地址(组织或个人的地址path)                                           | path  | string   |
| repo\*           | 仓库路径(path)                                                       | path  | string   |
| state            | 可选。Pull Request 状态: all、open、closed、locked、merged，默认：all         | query | string   |
| base             | 可选。Pull Request 提交目标分支的名称                                        | query | string   |
| since            | 可选。起始的更新时间，要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00`       | quey  | string   |
| direction        | 可选。升序/降序 默认：desc(asc 或者 desc)                                    | query | string   |
| sort             | 可选。排序字段: created、updated 默认：created                              | query | string   |
| milestone_number | 可选。里程碑序号(id)                                                     | quey  | int      |
| labels           | 以逗号分隔的标签名称列表                                                     | quey  | string   |
| page             | 当前的页码:默认为 1                                                      | query | int      |
| per_page         | 每页的数量，最大为 100，默认 20                                              | query | int      |
| author           | 可选。PR 创建者用户名                                                     | query | string   |
| assignee         | 可选。PR 负责人用户名                                                     | query | string   |
| reviewer         | 可选。PR 评审人用户名                                                     | query | string   |
| merged_after     | 返回在指定时间之后合并的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |
| merged_before    | 返回在指定时间之前合并的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |
| only_count       | 如果为true，则仅返回合并请求的计数，默认为 false                                    | query | boolean  |
| created_after    | 返回在指定时间之后创建的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |
| created_before   | 返回在指定时间之前创建的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |
| updated_before   | 返回在指定时间之前更新的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |
| updated_after    | 返回在指定时间之后更新的合并请求,要求时间格式为 ISO 8601 例如：`2024-11-20T13:00:21+08:00` | query | string   |

> **注意: ISO 8601 时间在 query 参数中需要进行 url 编码处理，例如：`2024-11-20T13:00:21+08:00` url 编码后为 `2024-11-20T13%3A00%3A21%2B08%3A00`**

### 响应

**only_count 模式（only_count 为 True）**

```json
{
  "all": 1,
  "opened": 1,
  "closed": 0,
  "merged": 0,
  "locked": 0
}
```

> 注： state 参数在 only_count 模式是不生效的

**非 only_count 模式 (only_count 为 None 或者 False)**

```json
[
  {
    "number": 63,
    "html_url": "https://test.gitcode.net/One/One/merge_requests/63",
    "close_related_issue": null,
    "prune_branch": false,
    "draft": false,
    "url": "https://api.gitcode.net/api/v5/repos/One/One/pulls/63",
    "labels": [
      {
        "id": 381445,
        "color": "#008672",
        "name": "help wanted",
        "title": "help wanted",
        "repository_id": 243377,
        "type": null,
        "text_color": null
      },
      {
        "id": 381446,
        "color": "#CFD240",
        "name": "invalid",
        "title": "invalid",
        "repository_id": 243377,
        "type": null,
        "text_color": null
      },
      {
        "id": 381447,
        "color": "#D876E3",
        "name": "question",
        "title": "question",
        "repository_id": 243377,
        "type": null,
        "text_color": null
      }
    ],
    "user": {
      "id": "65f94ab6f21fa3084fc04823",
      "login": "csdntest13",
      "name": "csdntest13_gitcode",
      "state": "active",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
      "avatar_path": null,
      "email": "",
      "name_cn": "csdntest13",
      "html_url": "https://test.gitcode.net/csdntest13",
      "tenant_name": null,
      "is_member": null
    },
    "assignees": [
      {
        "id": "64c71c3d64037b4af1c7a93f",
        "login": "green",
        "name": "百里",
        "state": "optional",
        "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171",
        "avatar_path": null,
        "email": null,
        "name_cn": "green",
        "html_url": "https://test.gitcode.net/green",
        "assignee": true,
        "code_owner": false,
        "accept": false
      }
    ],
    "head": {
      "label": "test_b12",
      "ref": "test_b12",
      "sha": "fb6495834d1bf7a39dfdb44ad25e6f83c7136310",
      "user": {
        "id": "65f94ab6f21fa3084fc04823",
        "login": "csdntest13",
        "name": "csdntest13_gitcode",
        "state": "active",
        "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
        "avatar_path": null,
        "email": "",
        "name_cn": "csdntest13",
        "html_url": "https://test.gitcode.net/csdntest13",
        "tenant_name": null,
        "is_member": null
      },
      "repo": {
        "id": 243377,
        "full_path": "One/One",
        "human_name": "One / One",
        "name": "One",
        "path": "One",
        "description": "csdntest13的第一个项目(公开)",
        "namespace": {
          "id": 136909,
          "name": "One",
          "path": "One",
          "develop_mode": "normal",
          "region": null,
          "cell": "default",
          "kind": "group",
          "full_path": "One",
          "full_name": "One ",
          "parent_id": null,
          "visibility_level": 20,
          "enable_file_control": null,
          "owner_id": null
        },
        "owner": {
          "id": "65f94ab6f21fa3084fc04823",
          "login": "csdntest13",
          "name": "csdntest13_gitcode",
          "state": "active",
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
          "avatar_path": null,
          "email": "",
          "name_cn": "csdntest13",
          "html_url": "https://test.gitcode.net/csdntest13",
          "tenant_name": null,
          "is_member": null
        },
        "assigner": {
          "id": "64c71c3d64037b4af1c7a93f",
          "login": "green",
          "name": "百里",
          "state": null,
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171",
          "avatar_path": null,
          "email": null,
          "name_cn": null,
          "html_url": "https://test.gitcode.net/green",
          "tenant_name": null,
          "is_member": null
        },
        "private": null,
        "public": null,
        "internal": false
      }
    },
    "base": {
      "label": "dev",
      "ref": "dev",
      "sha": "0c02dd57f8945791460a141f155dd2f4bd5dea86",
      "user": {
        "id": "65f94ab6f21fa3084fc04823",
        "login": "csdntest13",
        "name": "csdntest13_gitcode",
        "state": "active",
        "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
        "avatar_path": null,
        "email": "",
        "name_cn": "csdntest13",
        "html_url": "https://test.gitcode.net/csdntest13",
        "tenant_name": null,
        "is_member": null
      },
      "repo": {
        "id": 243377,
        "full_path": "One/One",
        "human_name": "One / One",
        "name": "One",
        "path": "One",
        "description": "csdntest13的第一个项目(公开)",
        "namespace": {
          "id": 136909,
          "name": "One",
          "path": "One",
          "develop_mode": "normal",
          "region": null,
          "cell": "default",
          "kind": "group",
          "full_path": "One",
          "full_name": "One ",
          "parent_id": null,
          "visibility_level": 20,
          "enable_file_control": null,
          "owner_id": null
        },
        "owner": {
          "id": "65f94ab6f21fa3084fc04823",
          "login": "csdntest13",
          "name": "csdntest13_gitcode",
          "state": "active",
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
          "avatar_path": null,
          "email": "",
          "name_cn": "csdntest13",
          "html_url": "https://test.gitcode.net/csdntest13",
          "tenant_name": null,
          "is_member": null
        },
        "assigner": {
          "id": "64c71c3d64037b4af1c7a93f",
          "login": "green",
          "name": "百里",
          "state": null,
          "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171",
          "avatar_path": null,
          "email": null,
          "name_cn": null,
          "html_url": "https://test.gitcode.net/green",
          "tenant_name": null,
          "is_member": null
        },
        "private": null,
        "public": null,
        "internal": false
      }
    },
    "id": 70067,
    "iid": 63,
    "project_id": 243377,
    "title": "测试创建PR",
    "body": null,
    "state": "merged",
    "created_at": "2024-04-21T17:35:16.655+08:00",
    "updated_at": "2024-04-24T22:27:49.197+08:00",
    "merged_at": "2024-04-24T22:27:48.631+08:00",
    "closed_by": null,
    "closed_at": null,
    "title_html": null,
    "description_html": null,
    "target_branch": "dev",
    "source_branch": "test_b12",
    "squash_commit_message": null,
    "user_notes_count": 1,
    "upvotes": 0,
    "downvotes": 0,
    "source_project_id": 243377,
    "target_project_id": 243377,
    "work_in_progress": false,
    "milestone": null,
    "merge_when_pipeline_succeeds": false,
    "merge_status": "can_be_merged",
    "sha": "fb6495834d1bf7a39dfdb44ad25e6f83c7136310",
    "merge_commit_sha": "6c93b6e6fcf1ce1f0ce918d1a481f0500531ab72",
    "discussion_locked": null,
    "should_remove_source_branch": false,
    "force_remove_source_branch": false,
    "allow_collaboration": null,
    "allow_maintainer_to_push": null,
    "web_url": "https://test.gitcode.net/One/One/merge_requests/63",
    "time_stats": {
      "time_estimate": null,
      "total_time_spent": 0,
      "human_time_estimate": null,
      "human_total_time_spent": null
    },
    "squash": false,
    "merge_request_type": "MergeRequest",
    "has_pre_merge_ref": false,
    "review_mode": "approval",
    "is_source_branch_exist": true,
    "approval_merge_request_approvers": [
      {
        "id": 233,
        "username": "wunian2011",
        "name": "wunian2011",
        "nick_name": "测试吴",
        "name_cn": "wunian2011",
        "email": null,
        "state": "approve",
        "is_codeowner": false,
        "updated_at": "2024-04-24T21:40:11.095+08:00",
        "avatar_url": null
      },
      {
        "id": 277,
        "username": "renww",
        "name": "renww",
        "nick_name": "介简介简介简介简介简介简介简介简介",
        "name_cn": "renww",
        "email": null,
        "state": "optional",
        "is_codeowner": false,
        "updated_at": "2024-04-21T17:35:18.509+08:00",
        "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ee/dc/7602704ee7dcf13f4383a72d492b1813823afba729ae6e9115877a4a0128d990.jpg?time=1711447395118"
      }
    ],
    "approval_merge_request_testers": [],
    "added_lines": 1,
    "removed_lines": 0,
    "subscribed": true,
    "changes_count": "1",
    "latest_build_started_at": null,
    "latest_build_finished_at": null,
    "first_deployed_to_production_at": null,
    "pipeline": null,
    "diff_refs": {
      "base_sha": "0c02dd57f8945791460a141f155dd2f4bd5dea86",
      "head_sha": "fb6495834d1bf7a39dfdb44ad25e6f83c7136310",
      "start_sha": "b6d44deb0ca73d7a50916d0fea02c72edd6c924e"
    },
    "merge_error": null,
    "json_merge_error": null,
    "rebase_in_progress": null,
    "diverged_commits_count": null,
    "merge_request_reviewer_list": [],
    "merge_request_review_count": 0,
    "merge_request_reviewers_count": 0,
    "notes": 1,
    "unresolved_discussions_count": 0,
    "gate_check": true,
    "head_pipeline_id": null,
    "pipeline_status": "",
    "codequality_status": "success",
    "pipeline_status_with_code_quality": "",
    "from_forked_project": false,
    "forked_project_name": null,
    "can_delete_source_branch": true,
    "required_reviewers": [],
    "omega_mode": false,
    "root_mr_locked_detail": null,
    "source_git_url": "ssh://git@test.gitcode.net:2222/One/One.git",
    "auto_merge": false,
    "milestone": {
      "created_at": "2024-04-15T18:33:01+08:00",
      "description": "1",
      "due_on": "2024-04-29",
      "number": 73008,
      "repository_id": 249609,
      "state": "active",
      "title": "第二个里程碑",
      "updated_at": "2024-04-15T18:33:01+08:00",
      "url": "https://test.gitcode.net/xiaogang_test/test222/milestones/2"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls?access_token=xxxx' \
```

## 2. 合并Pull Request

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/merge`

### 参数

| 参数名         | 描述                                                                                                     | 类型  | 数据类型 |
| -------------- | -------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                                                               | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                                                                   | path  | string   |
| repo\*         | 仓库路径(path)                                                                                           | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                                                                               | path  | int      |
| merge_method   | 可选。合并PR的方法，merge（合并所有提交）、squash（扁平化分支合并）和rebase（变基并合并）。默认为merge。 | body  | string   |

### 响应

```json
{
  "sha": "c20ac9624d2811a9313af29769dcf581b60c3044",
  "merged": true,
  "message": "Pull Request 已成功合并"
}
```

### Demo

```bash
curl --location -g --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/merge?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "merge_method": "merge"
}'
```

## 3. 获取pr关联的issue

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/issues`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| page           | 当前的页码:默认为 1                    | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "number": "1",
    "title": "[bug] test",
    "state": "open",
    "title": "进行稳定性测试",
    "url": "https://api.gitcode.com/api/v5/repos/sytest/paopao/issues/1",
    "body": "发生什么问题了？",
    "user": {
      "id": "681",
      "login": "test",
      "name": "test"
    },
    "labels": [
      {
        "color": "#008672",
        "name": "help wanted",
        "id": 381445,
        "title": "help wanted",
        "type": null,
        "textColor": "#FFFFFF"
      }
    ]
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/issues?access_token=xxxx' \
```

## 4. 提交pull request 评论

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/comments`

### 参数

| 参数名          | 描述                                                                          | 类型  | 数据类型 |
| --------------- | ----------------------------------------------------------------------------- | ----- | -------- |
| access_token\*  | 用户授权码                                                                    | query | string   |
| owner\*         | 仓库所属空间地址(组织或个人的地址path)                                        | path  | string   |
| repo\*          | 仓库路径(path)                                                                | path  | string   |
| number\*        | 第几个PR，即本仓库PR的序数                                                    | path  | int      |
| body\*          | 评论内容                                                                      | body  | string   |
| path            | 文件的相对路径                                                                | body  | string   |
| position        | Diff的相对行数                                                                | body  | int      |
| need_to_resolve | 是否需要解决(true：评审意见需要解决，false：评审意见不需要解决, 默认为 false) | body  | boolean  |

### 响应

```json
{
  "id": "97219c08d421e55cfa841deca16a30f5d7269e10",
  "body": "22222"
}
```

### Demo

```bash
curl --location -g --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/comments?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "body": "body",
    "need_to_resolve": false
}'
```

## 5. Pull Request Commit文件列表

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/files`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
[
  {
    "sha": "45e1211262a0ed24eeb85ac37f7776259ef0e7e1",
    "filename": "README.md",
    "status": null,
    "additions": "3",
    "deletions": "1",
    "blob_url": "https://ra w.gitcode.com/zzero/demo/blob/45e1211262a0ed24eeb85ac37f7776259ef0e7e1/README.md",
    "raw_url": "https://ra w.gitcode.com/zzero/demo/raw/45e1211262a0ed24eeb85ac37f7776259ef0e7e1/README.md",
    "patch": {
      "diff": "@@ -13,4 +13,6 @@ demo\n \r\n > covid_19 一个模拟感染人群爆发的小动画\r\n \r\n-> leetcode 算法解答\n\\ No newline at end of file\n+> leetcode 算法解答\r\n+\r\n+> juc包测试\n\\ No newline at end of file\n",
      "new_path": "README.md",
      "old_path": "README.md",
      "a_mode": "100644",
      "b_mode": "100644",
      "new_file": false,
      "renamed_file": false,
      "deleted_file": false,
      "too_large": false
    }
  },
  {
    "sha": "45e1211262a0ed24eeb85ac37f7776259ef0e7e1",
    "filename": "src/main/java/com/zhzh/sc/demo/juc/lock/VolatileDemo.java",
    "status": null,
    "additions": "3",
    "deletions": "0",
    "blob_url": "https://ra w.gitcode.com/zzero/demo/blob/45e1211262a0ed24eeb85ac37f7776259ef0e7e1/src/main/java/com/zhzh/sc/demo/juc/lock/VolatileDemo.java",
    "raw_url": "https://ra w.gitcode.com/zzero/demo/raw/45e1211262a0ed24eeb85ac37f7776259ef0e7e1/src/main/java/com/zhzh/sc/demo/juc/lock/VolatileDemo.java",
    "patch": {
      "diff": "@@ -15,6 +15,9 @@ public class VolatileDemo {\n         System.out.println(\"service end\");\n     }\n \n+    /**\n+     * 测试方法入口\n+     */\n     public static void main(String[] args) throws InterruptedException {\n         VolatileDemo v = new VolatileDemo();\n         new Thread(v::service, \"thread-1\").start();\n",
      "new_path": "src/main/java/com/zhzh/sc/demo/juc/lock/VolatileDemo.java",
      "old_path": "src/main/java/com/zhzh/sc/demo/juc/lock/VolatileDemo.java",
      "a_mode": "100644",
      "b_mode": "100644",
      "new_file": false,
      "renamed_file": false,
      "deleted_file": false,
      "too_large": false
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/files?access_token=xxxx' \
```

## 6. 获取某个Pull Request的所有评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/comments`

### 参数

| 参数名         | 描述                                                              | 类型  | 数据类型 |
| -------------- | ----------------------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                                        | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)                            | path  | string   |
| repo\*         | 仓库路径(path)                                                    | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                                        | path  | int      |
| page           | 当前的页码:默认为 1                                               | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                                   | query | int      |
| direction      | 可选。升序/降序(asc/desc)                                         | query | int      |
| comment_type   | 可选。筛选评论类型。代码行评论/pr普通评论:diff_comment/pr_comment | query | string   |

### 响应

```json
[
  {
    "id": "de772738e6dab92174c0e86c052ccf9bed24f747",
    "body": "111",
    "created_at": "2024-04-19T07:48:59.755+00:00",
    "updated_at": "2024-04-19T07:48:59.755+00:00",
    "user": {
      "id": 708,
      "login": "Lzm_0916",
      "name": "Lzm_0916",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/cb/da/6cb18d9ae9f1a94b4f640d3b848351c352c7869f33d0cb68e7acad4f224c4e23.png",
      "html_url": "https://test.gitcode.net/Lzm_0916"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/comments?access_token=xxxx' \
```

## 7. 创建Pull Request

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls`

### 参数

| 参数名                | 描述                                                                                                                                  | 类型  | 数据类型 |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | ----- | -------- |
| access_token\*        | 用户授权码                                                                                                                            | query | string   |
| owner\*               | 仓库所属空间地址(组织或个人的地址path)                                                                                                | path  | string   |
| repo\*                | 仓库路径(path)                                                                                                                        | path  | string   |
| title\*               | 必填。Pull Request 标题                                                                                                               | body  | string   |
| head\*                | 必填。Pull Request 提交的源分支。格式：branch 如果跨仓PR传：username:branch                                                           | body  | string   |
| base\*                | 必填。Pull Request 提交目标分支的名称                                                                                                 | body  | string   |
| body                  | 可选。Pull Request 内容                                                                                                               | body  | string   |
| milestone_number      | 可选。里程碑序号(id)                                                                                                                  | body  | int      |
| labels                | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance                                                           | body  | string   |
| issue                 | 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充                                                                          | body  | string   |
| assignees             | 可选。审查人员username，可多个，半角逗号分隔，如：(username1,username2), 注意: 当仓库代码审查设置中已设置【指派审查人员】则此选项无效 | body  | string   |
| testers               | 可选。测试人员username，可多个，半角逗号分隔，如：(username1,username2), 注意: 当仓库代码审查设置中已设置【指派测试人员】则此选项无效 | body  | string   |
| prune_source_branch   | 可选。合并PR后是否删除源分支，默认false（不删除）                                                                                     | body  | boolean  |
| draft                 | 是否设置为草稿 默认false                                                                                                              | body  | boolean  |
| squash                | 接受 Pull Request 时使用扁平化（Squash）合并, 默认false                                                                               | body  | boolean  |
| squash_commit_message | squash提交信息                                                                                                                        | body  | string   |
| fork_path             | fork项目路径【owner/repo】，跨仓PR 必填。                                                                                             | body  | string   |

### 响应

```json
{
  "id": 11264998,
  "url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/6",
  "html_url": "https://gitcode.com/zzero/demo/pulls/6",
  "diff_url": "https://gitcode.com/zzero/demo/pulls/6.diff",
  "patch_url": "https://gitcode.com/zzero/demo/pulls/6.patch",
  "issue_url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/6/issues",
  "commits_url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/6/commits",
  "review_comments_url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/comments/{/number}",
  "review_comment_url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/comments",
  "comments_url": "https://gitcode.com/api/v5/repos/zzero/demo/pulls/6/comments",
  "number": 6,
  "title": "测试创建PR",
  "description": "update: 更新文件 dev_001.txt \nupdate: 更新文件 dev_001.txt ",
  "state": "opened",
  "created_at": "2024-04-14T20:53:13.185+08:00",
  "updated_at": "2024-04-14T20:53:22.634+08:00",
  "merged_by": null,
  "merged_at": null,
  "closed_by": null,
  "closed_at": null,
  "title_html": null,
  "description_html": null,
  "target_branch": "test_b5",
  "source_branch": "dev",
  "squash_commit_message": null,
  "user_notes_count": 0,
  "upvotes": 0,
  "downvotes": 0,
  "author": {
    "id": 494,
    "name": "csdntest13",
    "username": "csdntest13",
    "iam_id": "d8b3e018b2364546b946886a669d50fc",
    "nick_name": "csdntest13_gitcode",
    "state": "active",
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
    "avatar_path": null,
    "email": "csdntest13@noreply.gitcode.com",
    "name_cn": "csdntest13",
    "web_url": "https://gitcode.com/csdntest13",
    "tenant_name": null,
    "is_member": null
  },
  "assignee": null,
  "source_project_id": 243377,
  "target_project_id": 243377,
  "labels": [
    {
      "color": "#008672",
      "name": "help wanted",
      "id": 381445,
      "title": "help wanted",
      "type": null,
      "textColor": "#FFFFFF"
    },
    {
      "color": "#CFD240",
      "name": "invalid",
      "id": 381446,
      "title": "invalid",
      "type": null,
      "textColor": "#FFFFFF"
    },
    {
      "color": "#D876E3",
      "name": "question",
      "id": 381447,
      "title": "question",
      "type": null,
      "textColor": "#333333"
    }
  ],
  "work_in_progress": false,
  "milestone": null,
  "merge_when_pipeline_succeeds": false,
  "merge_status": "unchecked",
  "sha": "8da7a5c35e71deeb0bf1d9ecae70449c574749f2",
  "merge_commit_sha": null,
  "discussion_locked": null,
  "should_remove_source_branch": false,
  "force_remove_source_branch": false,
  "allow_collaboration": null,
  "allow_maintainer_to_push": null,
  "web_url": "https://gitcode.com/One/One/merge_requests/53",
  "time_stats": {
    "time_estimate": null,
    "total_time_spent": 0,
    "human_time_estimate": null,
    "human_total_time_spent": null
  },
  "squash": false,
  "merge_request_type": "MergeRequest",
  "has_pre_merge_ref": false,
  "review_mode": "approval",
  "is_source_branch_exist": true,
  "approval_merge_request_reviewers": [
    {
      "id": 43,
      "username": "green",
      "name": "green",
      "nick_name": null,
      "name_cn": "green",
      "email": null,
      "state": "optional",
      "is_codeowner": false,
      "updated_at": "2024-04-14T20:53:23.021+08:00",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171"
    }
  ],
  "approval_merge_request_approvers": [
    {
      "id": 277,
      "username": "renww",
      "name": "renww",
      "nick_name": null,
      "name_cn": "renww",
      "email": null,
      "state": "optional",
      "is_codeowner": false,
      "updated_at": "2024-04-14T20:53:23.751+08:00",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ee/dc/7602704ee7dcf13f4383a72d492b1813823afba729ae6e9115877a4a0128d990.jpg?time=1711447395118"
    }
  ],
  "approval_merge_request_testers": [
    {
      "id": 43,
      "username": "green",
      "name": "green",
      "nick_name": null,
      "name_cn": "green",
      "email": null,
      "state": "optional",
      "is_codeowner": false,
      "updated_at": "2024-04-14T20:53:23.755+08:00",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171"
    },
    {
      "id": 277,
      "username": "renww",
      "name": "renww",
      "nick_name": null,
      "name_cn": "renww",
      "email": null,
      "state": "optional",
      "is_codeowner": false,
      "updated_at": "2024-04-14T20:53:23.755+08:00",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ee/dc/7602704ee7dcf13f4383a72d492b1813823afba729ae6e9115877a4a0128d990.jpg?time=1711447395118"
    }
  ],
  "source_project": {
    "id": 243377,
    "description": "csdntest13的第一个项目(公开)",
    "name": "One",
    "name_with_namespace": "One / One",
    "path": "One",
    "path_with_namespace": "One/One",
    "develop_mode": "normal",
    "created_at": "2024-03-19T16:24:01.197+08:00",
    "updated_at": "2024-03-19T16:42:34.834+08:00",
    "archived": false,
    "is_kia": false,
    "ssh_url_to_repo": "ssh://git@gitcode.com:2222/One/One.git",
    "http_url_to_repo": "https://gitcode.com/One/One.git",
    "web_url": "https://gitcode.com/One/One",
    "readme_url": "https://gitcode.com/One/One/blob/main/README.md",
    "product_id": "28f96caf52004e81ab0bc38d60d11940",
    "product_name": null,
    "member_mgnt_mode": 3,
    "default_branch": "main",
    "tag_list": [],
    "license_url": null,
    "license": {
      "key": "Apache_License_v2.0",
      "name": null,
      "nickname": null,
      "html_url": null,
      "source_url": null
    },
    "avatar_url": null,
    "star_count": 1,
    "forks_count": 0,
    "open_issues_count": 108,
    "open_merge_requests_count": 32,
    "open_change_requests_count": null,
    "watch_count": 1,
    "last_activity_at": "2024-04-14T20:43:58.602+08:00",
    "namespace": {
      "id": 136909,
      "name": "One",
      "path": "One",
      "develop_mode": "normal",
      "region": null,
      "cell": "default",
      "kind": "group",
      "full_path": "One",
      "full_name": "One ",
      "parent_id": null,
      "visibility_level": 20,
      "enable_file_control": null,
      "owner_id": null
    },
    "empty_repo": false,
    "starred": false,
    "visibility": "public",
    "security": "internal",
    "has_updated_kia": false,
    "network_type": "green",
    "owner": null,
    "creator": {
      "id": 494,
      "name": "csdntest13",
      "username": "csdntest13",
      "iam_id": "d8b3e018b2364546b946886a669d50fc",
      "nick_name": "csdntest13_gitcode",
      "state": "active",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
      "avatar_path": null,
      "email": "csdntest13@noreply.gitcode.com",
      "name_cn": "csdntest13",
      "web_url": "https://gitcode.com/csdntest13",
      "tenant_name": null,
      "is_member": null
    },
    "creator_id": 494,
    "forked_from_project": null,
    "item_type": "Project",
    "main_repository_language": ["Text", "#cccccc"],
    "mirror_project_data": null,
    "statistics": null,
    "branch_count": null,
    "tag_count": null,
    "member_count": null,
    "repo_replica_urls": null,
    "open_external_wiki": true,
    "release_count": null
  },
  "target_project": {
    "id": 243377,
    "description": "csdntest13的第一个项目(公开)",
    "name": "One",
    "name_with_namespace": "One / One",
    "path": "One",
    "path_with_namespace": "One/One",
    "develop_mode": "normal",
    "created_at": "2024-03-19T16:24:01.197+08:00",
    "updated_at": "2024-03-19T16:42:34.834+08:00",
    "archived": false,
    "is_kia": false,
    "ssh_url_to_repo": "ssh://git@gitcode.com:2222/One/One.git",
    "http_url_to_repo": "https://gitcode.com/One/One.git",
    "web_url": "https://gitcode.com/One/One",
    "readme_url": "https://gitcode.com/One/One/blob/main/README.md",
    "product_id": "28f96caf52004e81ab0bc38d60d11940",
    "product_name": null,
    "member_mgnt_mode": 3,
    "default_branch": "main",
    "tag_list": [],
    "license_url": null,
    "license": {
      "key": "Apache_License_v2.0",
      "name": null,
      "nickname": null,
      "html_url": null,
      "source_url": null
    },
    "avatar_url": null,
    "star_count": 1,
    "forks_count": 0,
    "open_issues_count": 108,
    "open_merge_requests_count": 32,
    "open_change_requests_count": null,
    "watch_count": 1,
    "last_activity_at": "2024-04-14T20:43:58.602+08:00",
    "namespace": {
      "id": 136909,
      "name": "One",
      "path": "One",
      "develop_mode": "normal",
      "region": null,
      "cell": "default",
      "kind": "group",
      "full_path": "One",
      "full_name": "One ",
      "parent_id": null,
      "visibility_level": 20,
      "enable_file_control": null,
      "owner_id": null
    },
    "empty_repo": false,
    "starred": false,
    "visibility": "public",
    "security": "internal",
    "has_updated_kia": false,
    "network_type": "green",
    "owner": null,
    "creator": {
      "id": 494,
      "name": "csdntest13",
      "username": "csdntest13",
      "iam_id": "d8b3e018b2364546b946886a669d50fc",
      "nick_name": "csdntest13_gitcode",
      "state": "active",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
      "avatar_path": null,
      "email": "csdntest13@noreply.gitcode.com",
      "name_cn": "csdntest13",
      "web_url": "https://gitcode.com/csdntest13",
      "tenant_name": null,
      "is_member": null
    },
    "creator_id": 494,
    "forked_from_project": null,
    "item_type": "Project",
    "main_repository_language": ["Text", "#cccccc"],
    "mirror_project_data": null,
    "statistics": null,
    "branch_count": null,
    "tag_count": null,
    "member_count": null,
    "repo_replica_urls": null,
    "open_external_wiki": true,
    "release_count": null
  },
  "added_lines": 19860,
  "removed_lines": 1,
  "subscribed": true,
  "changes_count": "6",
  "latest_build_started_at": null,
  "latest_build_finished_at": null,
  "first_deployed_to_production_at": null,
  "pipeline": null,
  "diff_refs": {
    "base_sha": "0c02dd57f8945791460a141f155dd2f4bd5dea86",
    "head_sha": "8da7a5c35e71deeb0bf1d9ecae70449c574749f2",
    "start_sha": "fb6495834d1bf7a39dfdb44ad25e6f83c7136310"
  },
  "merge_error": null,
  "json_merge_error": null,
  "rebase_in_progress": null,
  "diverged_commits_count": null,
  "merge_request_assignee_list": [],
  "merge_request_reviewer_list": [],
  "user": {
    "can_merge": true
  },
  "merge_request_review_count": 0,
  "merge_request_reviewers_count": 0,
  "notes": 0,
  "unresolved_discussions_count": 0,
  "e2e_issues": [
    {
      "id": 13588,
      "issue_type": 7,
      "linked_issue_type": null,
      "issue_num": "issue100",
      "commit_id": null,
      "merge_request_id": 68253,
      "check_fail_reason": "",
      "check_result": true,
      "issue_link": "https://gitcode.com/One/One/issues/100",
      "created_at": "2024-04-14T20:53:23.772+08:00",
      "mks_id": null,
      "pbi_id": null,
      "pbi_name": null,
      "source": null,
      "issue_project_id": 243377,
      "title": "第boudoirripinings-45个issue",
      "issue_project": null,
      "auto_c_when_mr_merged": false
    }
  ],
  "gate_check": true,
  "head_pipeline_id": null,
  "pipeline_status": "",
  "codequality_status": "success",
  "pipeline_status_with_code_quality": "",
  "from_forked_project": false,
  "forked_project_name": null,
  "can_delete_source_branch": true,
  "required_reviewers": [],
  "omega_mode": false,
  "root_mr_locked_detail": null,
  "source_git_url": "ssh://git@gitcode.com:2222/One/One.git",
  "auto_merge": null
}
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "title",
    "head": "dev",
    "base": "main"
}'
```

## 8. 更新Pull Request信息

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}`

### 参数

| 参数名           | 描述                                                                        | 类型  | 数据类型 |
| ---------------- | --------------------------------------------------------------------------- | ----- | -------- |
| access_token\*   | 用户授权码                                                                  | query | string   |
| owner\*          | 仓库所属空间地址(组织或个人的地址path)                                      | path  | string   |
| repo\*           | 仓库路径(path)                                                              | path  | string   |
| number\*         | 第几个PR，即本仓库PR的序数                                                  | path  | int      |
| title            | 可选。Pull Request 标题                                                     | body  | string   |
| body             | 可选。Pull Request 内容                                                     | body  | string   |
| state            | 可选。Pull Request 状态                                                     | body  | string   |
| milestone_number | 可选。里程碑序号(id)                                                        | body  | int      |
| labels           | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance | body  | string   |
| draft            | 是否设置为草稿                                                              | body  | boolean  |

### 响应

```json
{
  "title": "test_b3->dev",
  "body": "new: 新增文件 test_b3 \n2223333444444",
  "state": "opened",
  "created_at": "2024-03-28T22:23:29.999+08:00",
  "updated_at": "2024-04-14T21:06:52.499+08:00"
}
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "body": "body"
}'
```

## 9. 获取单个Pull Request

### 请求

`GET  https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
{
  "id": 111,
  "html_url": "http://gitcode.com/sytest/paopao/pull/1",
  "number": 1,
  "url": "https://api.gitcode.com/api/v5/repos/sytest/paopao/pulls/1",
  "issue_url": "https://api.gitcode.com/api/v5/repos/sytest/paopao/pulls/1/issues",
  "state": "open",
  "assignees_number": 1,
  "assignees": [
    {
      "id": 2,
      "login": "test",
      "name": "test_web",
      "avatar_url": "http://gitcode.com/sytest/paopao/pull/1.png",
      "html_url": "http://gitcode.com/sytest/paopao/pull/1",
      "assigness": true,
      "code_owner": false,
      "accept": true
    }
  ],
  "testers": [
    {
      "id": 2,
      "login": "test",
      "name": "test_web",
      "avatar_url": "http://gitcode.com/sytest/paopao/pull/1.png",
      "html_url": "http://gitcode.com/sytest/paopao/pull/1",
      "assigness": true,
      "code_owner": false,
      "accept": true
    }
  ],
  "labels": [
    {
      "id": 222,
      "name": "label1",
      "repository_id": 1,
      "created_at": "",
      "updated_at": ""
    }
  ],
  "created_at": "",
  "updated_at": "",
  "closed__at": "",
  "draft": false,
  "merged_at": "",
  "can_merge_check": false,
  "mergeable": true,
  "body": "Description",
  "user": {
    "id": "userId",
    "login": "test"
  },
  "head": {
    "label": "test",
    "ref": "test",
    "sha": "91861a9668041fc1c0ff51d1db66b6297179f5e6",
    "repo": {
      "path": "paopao",
      "name": "paopao"
    }
  },
  "base": {
    "label": "main",
    "ref": "main",
    "sha": "91861a9668041fc1c0ff51d1db66b6297179f5e6",
    "repo": {
      "path": "paopao",
      "name": "paopao"
    }
  },
  "prune_branch": false,
  "mergeable_state": {
        "merge_request_id": 111,
        "state": false,
        "status_without_user_auth": false,
        "conflict_passed": false,
        "branch_missing_passed": true,
        "non_ff_passed": true,
        "mr_state_passed": true,
        "merged_by_user_passed": true,
        "work_in_progress_passed": true,
        "resolve_discussion_passed": true,
        "ci_state_passed": true,
        "merge_by_self_passed": true,
        "can_force_merge": false,
        "approval_reviewers_required_passed": true,
        "approval_approvers_required_passed": true,
        "approval_testers_required_passed": true,
        "merge_request_switch": {
            "review_mode": "approval",
            "merge_method": "merge",
            "only_allow_merge_if_all_discussions_are_resolved": false,
            "disable_merge_by_self": false,
            "only_allow_merge_if_pipeline_succeeds": false,
            "disable_squash_merge": false,
            "squash_merge_with_no_merge_commit": false,
            "approval_required_reviewers_count": 0,
            "approval_required_reviewers_branch": "*",
            "add_notes_after_merged": false,
            "mark_auto_merged_mr_as_closed": false,
            "can_force_merge": false,
            "can_reopen": true
        },
        "reason": {},
        "check_tasks_num": 0
    },
  "ref_pull_requests": [
        {
            "id": 191973,
            "number": 3,
            "state": "merged",
            "title": "dddd"
        }
  ]
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15?access_token=xxxx'
```

## 10. 获取某Pull Request的所有Commit信息

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/commits`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
[
  {
    "sha": "91861a9668041fc1c0ff51d1db66b6297179f5e6",
    "html_url": "https://gitcode.com/sytest/paopao/blob/91861a9668041fc1c0ff51d1db66b6297179f5e6",
    "commit": {
      "author": {
        "login": "test",
        "name": "test",
        "email": "test@test.com",
        "date": "2024-03-28T11:19:33+08:00"
      },
      "committer": {
        "login": "test",
        "name": "test",
        "email": "test@test.com"
      },
      "message": "!5 333 * add 1/2/3/4. * add 1/2/3. "
    },
    "author": {
      "id": "id123",
      "login": "test",
      "name": "test",
      "avatar_url": "https://gitcode/pic.png",
      "html_url": "https://gitcode.com/test"
    },
    "committer": {
      "id": "id123",
      "login": "test",
      "name": "test",
      "avatar_url": "https://gitcode/pic.png",
      "html_url": "https://gitcode.com/test"
    },
    "parents": {
      "sha": "2e208a1e38f6a5a7b0cc3787688067ba082a8bb7",
      "shas": ["2e208a1e38f6a5a7b0cc3787688067ba082a8bb7"]
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/commits?access_token=xxxx'
```

## 11. 创建 Pull Request 标签

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/labels`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| labels\*       | 添加的标签 如: ["feat", "bug"]         | body  | array    |

### 响应

```json
[
  {
    "color": "#008672",
    "name": "help wanted",
    "id": 381445,
    "title": "help wanted",
    "type": null,
    "textColor": "#FFFFFF"
  }
]
```

### Response Code

```text
HTTP status 201 No Content
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/labels?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '["feat"]'
```

## 12. 删除 Pull Request 标签

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/labels/{name}`

### 参数

| 参数名         | 描述                                              | 类型  | 数据类型 |
| -------------- | ------------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                        | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)            | path  | string   |
| repo\*         | 仓库路径(path)                                    | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                        | path  | int      |
| name\*         | 标签名称(批量删除用英文逗号分隔，如: bug,feature) | path  | string   |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/labels/bug?access_token=xxxx'
```

## 13. 处理 Pull Request 测试

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/test`

### 参数

| 参数名         | 描述                                       | 类型  | 数据类型 |
| -------------- | ------------------------------------------ | ----- | -------- |
| access_token\* | 用户授权码                                 | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)     | path  | string   |
| repo\*         | 仓库路径(path)                             | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                 | path  | Integer  |
| force          | 是否强制测试通过（默认否），只对管理员生效 | body  | boolean  |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/test?access_token=xxxx'
```

## 14. 处理 Pull Request 审查

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/review`

### 参数

| 参数名         | 描述                                       | 类型  | 数据类型 |
| -------------- | ------------------------------------------ | ----- | -------- |
| access_token\* | 用户授权码                                 | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)     | path  | string   |
| repo\*         | 仓库路径(path)                             | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                 | path  | Integer  |
| force          | 是否强制测试通过（默认否），只对管理员生效 | body  | boolean  |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request POST 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/review?access_token=xxxx'
```

## 15. 获取某个Pull Request的操作日志

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/operate_logs`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | body  | int      |
| sort           | 按递减(desc)排序，默认：递减           | query | String   |
| page           | 当前的页码:默认为 1                    | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20        | query | int      |

### 响应

```json
[
  {
    "content": "Create mr issue link: **第boudoirripinings-24个issue** #79",
    "id": 274531,
    "action": "add_mr_issue_link",
    "merge_request_id": 70067,
    "created_at": "2024-04-23T11:32:08.522+08:00",
    "updated_at": "2024-04-23T11:32:08.522+08:00",
    "discussion_id": "18a5ab21f57cda175b8eabc2ec829a9e04d4d458",
    "project": "One/One",
    "assignee": null,
    "proposer": null,
    "user": {
      "id": "65f94ab6f21fa3084fc04823",
      "name": "csdntest13",
      "login": "csdntest13",
      "iam_id": "d8b3e018b2364546b946886a669d50fc",
      "nick_name": "csdntest13_gitcode",
      "state": "active",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
      "avatar_path": null,
      "email": "csdntest13@noreply.gitcode.com",
      "name_cn": "csdntest13",
      "web_url": "https://test.gitcode.net/csdntest13",
      "tenant_name": null,
      "is_member": null
    },
    "action_type": "add_mr_issue_link"
  },
  {
    "content": "Create mr issue link: **第boudoirripinings-25个issue** #80",
    "id": 274529,
    "action": "add_mr_issue_link",
    "merge_request_id": 70067,
    "created_at": "2024-04-23T11:32:07.588+08:00",
    "updated_at": "2024-04-23T11:32:07.588+08:00",
    "discussion_id": "9b4b01dbe059dbdc120afd8bdf9fd865d4ea42b1",
    "project": "One/One",
    "assignee": null,
    "proposer": null,
    "user": {
      "id": "65f94ab6f21fa3084fc04823",
      "name": "csdntest13",
      "login": "csdntest13",
      "iam_id": "d8b3e018b2364546b946886a669d50fc",
      "nick_name": "csdntest13_gitcode",
      "state": "active",
      "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/ec/ba/4e7c4661b6154a7dd088d9fe64b4893383a2e318bf362350ce18d44df6ac7e37.png?time=1711533165876",
      "avatar_path": null,
      "email": "csdntest13@noreply.gitcode.com",
      "name_cn": "csdntest13",
      "web_url": "https://test.gitcode.net/csdntest13",
      "tenant_name": null,
      "is_member": null
    },
    "action_type": "add_mr_issue_link"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/operate_logs?access_token=xxxx'
```

## 16. 获取某个 Pull Request 的所有标签

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/labels`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
[
  {
    "id": 18517,
    "color": "#ED4014",
    "name": "bug",
    "repository_id": 198606,
    "url": "",
    "created_at": "2024-02-23",
    "updated_at": "2024-02-23",
    "text_color": "#FFFFFF"
  },
  {
    "id": 383740,
    "color": "#428BCA",
    "name": "performance",
    "repository_id": 198606,
    "url": "",
    "created_at": "2024-04-20",
    "updated_at": "2024-04-20",
    "text_color": "#FFFFFF"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/labels?access_token=xxxx'
```

## 17. 重置 Pull Request 测试 的状态

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/testers`

### 参数

| 参数名         | 描述                                            | 类型  | 数据类型 |
| -------------- | ----------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                      | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)          | path  | string   |
| repo\*         | 仓库路径(path)                                  | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                      | path  | Integer  |
| reset_all      | 是否重置所有测试人，默认：false，只对管理员生效 | body  | boolean  |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/testers?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "reset_all": "true"
}'
```

## 18. 重置 Pull Request 审查 的状态

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/assignees`

### 参数

| 参数名         | 描述                                            | 类型  | 数据类型 |
| -------------- | ----------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                      | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path)          | path  | string   |
| repo\*         | 仓库路径(path)                                  | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数                      | path  | Integer  |
| reset_all      | 是否重置所有审查人，默认：false，只对管理员生效 | body  | boolean  |

### 响应

```text
HTTP status 204 No Content
```

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/assignees?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "reset_all": "true"
}'
```

## 19. pr提交的文件变更信息

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/files.json`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
{
  "code": 0,
  "added_lines": 7,
  "remove_lines": 7,
  "count": 2,
  "diff_refs": {
    "base_sha": "db015522f67b15e868c5f929ff1af4cba9ddd112",
    "start_sha": "db015522f67b15e868c5f929ff1af4cba9ddd112",
    "head_sha": "93cd8303ac07886610d738baf7ddd620f04ee778"
  },
  "diffs": [
    {
      "new_blob_id": "b575bd06b44029dc12771503388d61ea383169cb",
      "statistic": {
        "type": "text_type",
        "path": "1",
        "old_path": "1",
        "new_path": "1",
        "view": false
      },
      "head": {
        "url": "https://pre-raw.gitcode.com/xiaogang/test/raw/93cd8303ac07886610d738baf7ddd620f04ee778/1",
        "commit_id": "93cd8303ac07886610d738baf7ddd620f04ee778"
      },
      "added_lines": 3,
      "remove_lines": 3,
      "content": {
        "text": [
          {
            "line_content": "111",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_1_1",
              "line_num": "1"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_1_1",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "111222",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_2_1",
              "line_num": ""
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_2_1",
              "line_num": "1"
            },
            "type": "new"
          },
          {
            "line_content": " ",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_2_2",
              "line_num": "2"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_2_2",
              "line_num": "2"
            }
          },
          {
            "line_content": "1113",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_3_3",
              "line_num": "3"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_3_3",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "1113333",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_4_3",
              "line_num": ""
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_4_3",
              "line_num": "3"
            },
            "type": "new"
          },
          {
            "line_content": " ",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_4_4",
              "line_num": "4"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_4_4",
              "line_num": "4"
            }
          },
          {
            "line_content": "444",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_5_5",
              "line_num": "5"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_5_5",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "4442423",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_6_5",
              "line_num": ""
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_6_5",
              "line_num": "5"
            },
            "type": "new"
          },
          {
            "line_content": " 5555",
            "old_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_6_6",
              "line_num": "6"
            },
            "new_line": {
              "line_code": "356a192b7913b04c54574d18c28d46e6395428ab_6_6",
              "line_num": "6"
            }
          }
        ]
      }
    },
    {
      "new_blob_id": "1d3e5f08788ce215053e01fd54a76c6c6fbc1ddc",
      "statistic": {
        "type": "text_type",
        "path": "2",
        "old_path": "2",
        "new_path": "2",
        "view": false
      },
      "head": {
        "url": "https://pre-raw.gitcode.com/xiaogang/test/raw/93cd8303ac07886610d738baf7ddd620f04ee778/2",
        "commit_id": "93cd8303ac07886610d738baf7ddd620f04ee778"
      },
      "added_lines": 4,
      "remove_lines": 4,
      "content": {
        "text": [
          {
            "line_content": " 11111",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_1_1",
              "line_num": "1"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_1_1",
              "line_num": "1"
            }
          },
          {
            "line_content": "22222",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_2_2",
              "line_num": "2"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_2_2",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "22222adfsasf",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_3_2",
              "line_num": ""
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_3_2",
              "line_num": "2"
            },
            "type": "new"
          },
          {
            "line_content": " 3333",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_3_3",
              "line_num": "3"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_3_3",
              "line_num": "3"
            }
          },
          {
            "line_content": "",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_4_4",
              "line_num": "4"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_4_4",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_5_4",
              "line_num": "5"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_5_4",
              "line_num": ""
            },
            "type": "old"
          },
          {
            "line_content": "4444",
            "old_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_6_4",
              "line_num": "6"
            },
            "new_line": {
              "line_code": "da4b9237bacccdf19c0760cab7aec4a8359010b0_6_4",
              "line_num": ""
            },
            "type": "old"
          }
        ]
      }
    }
  ]
}
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/15/files.json?access_token=xxxx'
```

## 20. 获取文件内容

### 请求

`GET https://raw.gitcode.com/{owner}/{repo}/raw/{head_sha}/{name}`

> 请求《19. pr提交的文件变更信息》的接口`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/files.json`，拿到 `diffs.head.url` 返回的地址信息 直接浏览器访问即可

## 21. 企业 Pull Request 列表

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/pull_requests`

### 参数

| 参数名         | 描述                            | 类型  | 数据类型 |
| -------------- | ------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                      | query | string   |
| enterprise\*   | 企业的路径(path/login)          | path  | string   |
| repo           | 可选。仓库路径(path)            | query | string   |
| state          | 可选。Pull Request 状态         | query | string   |
| issue_number   | issue全局id                     | query | int      |
| sort           | 可选。排序字段，默认按创建时间  | query | string   |
| direction      | 可选。升序/降序                 | query | string   |
| page           | 当前的页码:默认为 1             | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20 | query | int      |

### 响应

```json
[
  {
    "id": 71020,
    "url": "https://test.gitcode.net/api/v5/repos/test/test/1",
    "html_url": "https://test.gitcode.net/test/test/1",
    "number": 1,
    "state": "merged",
    "assignees_number": 0,
    "testers_number": 0,
    "assignees": [],
    "testers": [],
    "mergeable": null,
    "can_merge_check": true,
    "head": {
      "ref": "main",
      "sha": "d874402d259744a00121c2cff0febc8554339aef",
      "repo": {
        "path": "test",
        "name_space": {
          "path": "repo-dev"
        },
        "assigner": {
          "id": "uuid",
          "login": "Lzm_0916",
          "name": "Lzm_0916"
        }
      }
    },
    "base": {
      "ref": null,
      "sha": null,
      "repo": {
        "path": "test",
        "name_space": {
          "path": "repo-dev"
        }
      }
    },
    "milestone": {
      "created_at": "2024-04-15T18:33:01+08:00",
      "description": "1",
      "due_on": "2024-04-29",
      "number": 73008,
      "repository_id": 249609,
      "state": "active",
      "title": "第二个里程碑",
      "updated_at": "2024-04-15T18:33:01+08:00",
      "url": "https://test.gitcode.net/xiaogang_test/test222/milestones/2"
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/enterprises/xiaogang_test/pull_requests?access_token=xxxx'
```

## 22. 获取Pull Request某条评论

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论 ID                                | path  | string   |

### 响应

```json
{
  "id": 1486664,
  "body": "1111112222222",
  "comment_type": "DiscussionNote",
  "user": {
    "id": "303745",
    "login": "yinlin",
    "name": "yinlin-昵称",
    "type": "User"
  },
  "target": {
    "issue": {
      "id": 478892,
      "title": "1111",
      "number": "478892"
    }
  },
  "created_at": "2024-09-27T14:58:51+08:00",
  "updated_at": "2024-09-27T14:58:51+08:00"
}
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/repos/Hello_worldsss/IK_001_01/pulls/comments/1486664?access_token=xxxx'
```

## 23. 判断Pull Request是否合并

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/merge`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |

### 响应

```json
{
  "message": "Pull Request已经合并"
}
```

or

```json
{
  "error": "Pull Request不存在或未合并"
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/pulls/1/merge?access_token=xxx'
```

## 24. 指派用户审查 Pull Request

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/assignees`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| assignees\*    | 用户的个人空间地址, 以 , 分隔          | body  | string   |

### 响应

```json
{
  "assignees_number": 1
}
```

### Demo

```bash
curl --location -g --request POST 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/pulls/2/assignees?access_token=xxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "assignees": "xiaogang"
}'
```

## 25. 取消用户审查 Pull Request

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/assignees`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| assignees\*    | 用户的个人空间地址, 以 , 分隔          | body  | string   |

### 响应

无

### Demo

```bash
curl --location -g --request DELETE 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/pulls/2/assignees?access_token=xxxx' \
--form 'assignees="xiaogang"'
```

## 26. 编辑评论

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型     | 数据类型 |
| -------------- | -------------------------------------- | -------- | -------- |
| access_token\* | 用户授权码                             | query    | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path     | string   |
| repo\*         | 仓库路径(path)                         | path     | string   |
| id\*           | 评论的ID                               | path     | int      |
| body\*         | 必填。评论内容                         | formData | string   |

### 响应

```json
{}
```

### Demo

```bash
curl --location -g --request PATCH 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/pulls/comments/1495107?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "body": "Duis enim esse"
}'
```

## 27. 删除评论

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/comments/{id}`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| id\*           | 评论的ID                               | path  | int      |

### 响应

```json
{}
```

### Demo

```bash
curl --location -g --request DELETE 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/pulls/comments/1495107?access_token=xxxx'
```

## 28. 替换 Pull Request 所有标签

### 请求

`PUT https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/labels`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| labels\*       | 替换的标签 如: ["feat", "bug"]         | body  | array    |

### 响应

无

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/repos/xiaogang_test/test222/pulls/1/labels?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data '[
    "bug"
]'
```

## 29. 指派用户测试 Pull Request

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/pulls/{number}/testers`

### 参数

| 参数名         | 描述                                   | 类型  | 数据类型 |
| -------------- | -------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                             | query | string   |
| owner\*        | 仓库所属空间地址(组织或个人的地址path) | path  | string   |
| repo\*         | 仓库路径(path)                         | path  | string   |
| number\*       | 第几个PR，即本仓库PR的序数             | path  | int      |
| testers\*      | 用户的个人空间地址, 以 , 分隔          | body  | string   |

### 响应

```json
[
  {
    "id": 43,
    "login": "green",
    "name": "green",
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/be/fb/7b9e393fbd80ca315dec249f2be6e6a7378f591609b6525798bc6d95abedc992.png?time=1712128581171"
  },
  {
    "id": 452,
    "login": "zhanghq2",
    "name": "zhanghq2"
  }
]
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com//api/v5/repos/xiaogang_test/test222/pulls/15/testers?access_token=xxxx' \
--header 'Content-Type: application/json;charset=UTF-8' \
--data '{
    "testers":"green,zhanghq2"
}'
```

## 30. 组织 Pull Request 列表

### 请求

`GET https://api.gitcode.com/api/v5/org/{org}/pull_requests`

### 参数

| 参数名         | 描述                            | 类型  | 数据类型 |
| -------------- | ------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                      | query | string   |
| org\*          | org(path/login)                 | path  | string   |
| state          | 可选。Pull Request 状态         | query | string   |
| issue_number   | issue全局id                     | int   | string   |
| sort           | 可选。排序字段，默认按创建时间  | query | string   |
| direction      | 可选。升序/降序                 | query | string   |
| page           | 当前的页码:默认为 1             | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20 | int   | string   |

### 响应

```json
[
  {
    "id": 71020,
    "url": "https://test.gitcode.net/api/v5/repos/test/test/1",
    "html_url": "https://test.gitcode.net/test/test/1",
    "number": 1,
    "state": "merged",
    "assignees_number": 0,
    "testers_number": 0,
    "assignees": [],
    "testers": [],
    "mergeable": null,
    "can_merge_check": true,
    "head": {
      "ref": "main",
      "sha": "d874402d259744a00121c2cff0febc8554339aef",
      "repo": {
        "path": "test",
        "name_space": {
          "path": "repo-dev"
        },
        "assigner": {
          "id": "uuid",
          "login": "Lzm_0916",
          "name": "Lzm_0916"
        }
      }
    },
    "base": {
      "ref": null,
      "sha": null,
      "repo": {
        "path": "test",
        "name_space": {
          "path": "repo-dev"
        }
      }
    }
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/org/xiaogang_test/pull_requests?access_token=xxxx'
```

## 31. 获取企业 issue 关联的 Pull Requests

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/issues/{number}/pull_requests`

### 参数

| 参数名            | 描述              | 类型  | 数据类型    |
|----------------|-----------------| ----- |---------|
| access_token\* | 用户授权码           | query | string  |
| enterprise\*   | org(path/login) | path  | string  |
| number\*       | issue 全局 id     | path   | Integer |

### 响应

```json
[
    {
        "number": 1,
        "html_url": "https://test.gitcode.net/owner-test/wonderful1/merge_requests/1",
        "url": "https://test.gitcode.net/api/v5/repos/owner-test/wonderful1/pulls/1",
        "close_related_issue": 0,
        "prune_branch": false,
        "draft": false,
        "labels": [],
        "user": {
            "id": "654c61e5560ed95fd216cf31",
            "login": "csdn_fenglh",
            "name": "fenglh",
            "state": "active",
            "email": "",
            "name_cn": "",
            "html_url": "https://test.gitcode.net/csdn_fenglh"
        },
        "assignees": [],
        "testers": [],
        "head": {
            "label": "wonderful1-patch-1",
            "ref": "wonderful1-patch-1",
            "sha": "9931ba43e2c485741192d8e9a0b698fed79100f9",
            "user": {
                "id": "6638af02bbeee41d0fe74c35",
                "login": "malongge5",
                "name": "malongge5",
                "state": "active",
                "email": "malongge5@noreply.gitcode.com",
                "name_cn": "",
                "html_url": "https://test.gitcode.net/malongge5"
            },
            "repo": {
                "id": 686738,
                "full_path": "owner-test/wonderful1",
                "full_name": "owner-test/wonderful1",
                "human_name": "测试 / wonderful1",
                "name": "wonderful1",
                "path": "wonderful1",
                "description": "我的测试代码仓库",
                "owner": {
                    "id": "6638af02bbeee41d0fe74c35",
                    "login": "malongge5",
                    "name": "malongge5",
                    "state": "active",
                    "email": "malongge5@noreply.gitcode.com",
                    "name_cn": "",
                    "html_url": "https://test.gitcode.net/malongge5"
                },
                "assigner": {
                    "id": "6638af02bbeee41d0fe74c35",
                    "login": "malongge5",
                    "name": "malongge5",
                    "state": "active",
                    "email": "malongge5@noreply.gitcode.com",
                    "name_cn": "",
                    "html_url": "https://test.gitcode.net/malongge5"
                },
                "internal": false,
                "html_url": "https://test.gitcode.net/owner-test/wonderful1.git"
            }
        },
        "base": {
            "label": "abc",
            "ref": "abc",
            "sha": "44a8d3e1142468ab2db0fa8c3402a71ccf891572",
            "user": {
                "id": "6638af02bbeee41d0fe74c35",
                "login": "malongge5",
                "name": "malongge5",
                "state": "active",
                "email": "malongge5@noreply.gitcode.com",
                "name_cn": "",
                "html_url": "https://test.gitcode.net/malongge5"
            },
            "repo": {
                "id": 686738,
                "full_path": "owner-test/wonderful1",
                "full_name": "owner-test/wonderful1",
                "human_name": "测试 / wonderful1",
                "name": "wonderful1",
                "path": "wonderful1",
                "description": "我的测试代码仓库",
                "owner": {
                    "id": "6638af02bbeee41d0fe74c35",
                    "login": "malongge5",
                    "name": "malongge5",
                    "state": "active",
                    "email": "malongge5@noreply.gitcode.com",
                    "name_cn": "",
                    "html_url": "https://test.gitcode.net/malongge5"
                },
                "assigner": {
                    "id": "6638af02bbeee41d0fe74c35",
                    "login": "malongge5",
                    "name": "malongge5",
                    "state": "active",
                    "email": "malongge5@noreply.gitcode.com",
                    "name_cn": "",
                    "html_url": "https://test.gitcode.net/malongge5"
                },
                "internal": false,
                "html_url": "https://test.gitcode.net/owner-test/wonderful1.git"
            }
        },
        "id": 191980,
        "iid": 1,
        "project_id": 686738,
        "title": "hhhh",
        "body": "update: 更新文件 README.md ",
        "state": "merged",
        "assignees_number": 0,
        "testers_number": 0,
        "created_at": "2024-11-23T20:51:32+08:00",
        "updated_at": "2024-12-13T16:16:54+08:00",
        "merged_at": "2024-11-25T20:40:19+08:00",
        "closed_at": "",
        "target_branch": "abc",
        "source_branch": "wonderful1-patch-1",
        "source_project_id": 686738,
        "force_remove_source_branch": false,
        "web_url": "https://test.gitcode.net/owner-test/wonderful1/merge_requests/1",
        "merge_request_type": "MergeRequest",
        "review_mode": "approval",
        "added_lines": 2,
        "removed_lines": 2,
        "diff_refs": {
            "base_sha": "91a8edc0db6889fc7309d3306da7b12113e4a73f",
            "head_sha": "9931ba43e2c485741192d8e9a0b698fed79100f9",
            "start_sha": "91a8edc0db6889fc7309d3306da7b12113e4a73f"
        },
        "notes": 0,
        "pipeline_status": "",
        "pipeline_status_with_code_quality": "",
        "source_git_url": "ssh://git@test.gitcode.net:2222/owner-test/wonderful1.git",
        "can_merge_check": true,
        "mergeable": true,
        "locked": false,
        "diff_url": "https://test.gitcode.net/owner-test/wonderful1/merge_requests/1.diff",
        "patch_url": "https://test.gitcode.net/owner-test/wonderful1/merge_requests/1.patch",
        "commits_url": "https://test.gitcode.net/api/v5/repos/owner-test/wonderful1/pulls/1/commits",
        "comments_url": "https://test.gitcode.net/api/v5/repos/owner-test/wonderful1/pulls/1/comments",
        "issue_url": "https://test.gitcode.net/api/v5/repos/owner-test/wonderful1/pulls/1/issues"
    }
]
```

### Demo

```bash
curl --location --request GET 'https://api.gitcode.com/api/v5/enterprises/owner-test/issues/471521/pull_requests?access_token=***'
```
