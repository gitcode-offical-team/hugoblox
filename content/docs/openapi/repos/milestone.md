---
linkTitle: Milestone
title: 里程碑接口文档
weight: 8
sidebar:
  open: false
---

## 1. 获取仓库所有里程碑

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/milestones`

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| state        | 里程碑状态: open, closed, all。默认: open      | query | string   |
| sort         | 排序方式: due_on                               | query | string   |
| direction    | 升序(asc)或是降序(desc)。默认: asc             | query | string   |
| page         | 当前的页码                                     | query | int      |
| per_page     | 每页的数量，最大为 100，默认 20                | query | int      |

### 响应

```json
[
  {
    "closed_issues": 0,
    "created_at": "2024-10-08T10:58:16+08:00",
    "description": "你好中国",
    "due_on": "2024-11-08",
    "number": 4914,
    "open_issues": 0,
    "repository_id": 4066481,
    "state": "active",
    "title": "你好中国",
    "updated_at": "2024-10-08T10:58:16+08:00",
    "url": "https://gitcode.com/dengmengmian/oneapi/milestones/1"
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/milestones?state=open&sort=due_on&direction=asc&page=1&per_page=20&access_token=xxxx'
```

## 2. 获取仓库单个里程碑

### 请求

`GET https://api.gitcode.com/api/v5/repos/{owner}/{repo}/milestones/{number}`

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| number\*     | 里程碑序列号（number）                         | path  | int      |

### 响应

```json
{
  "closed_issues": 0,
  "created_at": "2024-10-08T10:58:16+08:00",
  "description": "你好中国",
  "due_on": "2024-11-08",
  "number": 4914,
  "open_issues": 0,
  "repository_id": 4066481,
  "state": "active",
  "title": "你好中国",
  "updated_at": "2024-10-08T10:58:16+08:00",
  "url": "https://gitcode.com/dengmengmian/oneapi/milestones/1"
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/milestones/4914?access_token=xxxx'
```

## 3. 删除仓库单个里程碑

### 请求

`DELETE https://api.gitcode.com/api/v5/repos/{owner}/{repo}/milestones/{number}`

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| number\*     | 里程碑序列号（number）                         | path  | int      |

### 响应

无

### Demo

```bash
curl --location -g --request DELETE 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/milestones/4914?access_token=xxxx'
```

## 4. 更新仓库里程碑

### 请求

`PATCH https://api.gitcode.com/api/v5/repos/{owner}/{repo}/milestones/{number}`

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| number\*     | 里程碑序列号（number）                         | path  | int      |
| title\*      | 里程碑标题                                     | body  | string   |
| state        | 里程碑状态: open, closed, all。默认: open      | body  | string   |
| description  | 里程碑具体描述                                 | body  | string   |
| due_on\*     | 里程碑的截止日期                               | body  | string   |

### 响应

```json
{
  "closed_issues": 0,
  "created_at": "2024-10-08T10:58:16+08:00",
  "description": "你好中国",
  "due_on": "2024-11-08",
  "number": 4914,
  "open_issues": 0,
  "repository_id": 4066481,
  "state": "active",
  "title": "你好中国",
  "updated_at": "2024-10-08T10:58:16+08:00",
  "url": "https://gitcode.com/dengmengmian/oneapi/milestones/1"
}
```

### Demo

```bash
curl --location -g --request PATCH 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/milestones/4914?access_token=xxxx' \
--data-raw '{
    "title": "title",
    "due_on": "2025-01-01"
}'
```

## 5. 创建仓库里程碑

### 请求

`POST https://api.gitcode.com/api/v5/repos/{owner}/{repo}/milestones`

| 参数名       | 描述                                           | 类型  | 数据类型 |
| ------------ | ---------------------------------------------- | ----- | -------- |
| access_token | 用户授权码                                     | query | string   |
| owner\*      | 仓库所属空间地址（企业、组织或个人的地址path） | path  | string   |
| repo\*       | 仓库路径(path)                                 | path  | string   |
| title\*      | 里程碑标题                                     | body  | string   |
| description  | 里程碑具体描述                                 | body  | string   |
| due_on\*     | 里程碑的截止日期                               | body  | string   |

### 响应

```json
{
  "closed_issues": 0,
  "created_at": "2024-10-08T10:58:16+08:00",
  "description": "你好中国",
  "due_on": "2024-11-08",
  "number": 4914,
  "open_issues": 0,
  "repository_id": 4066481,
  "state": "active",
  "title": "你好中国",
  "updated_at": "2024-10-08T10:58:16+08:00",
  "url": "https://gitcode.com/dengmengmian/oneapi/milestones/1"
}
```

### Demo

```bash
curl --location -g --request POST 'https://api.gitcode.com/api/v5/repos/dengmengmian/oneapi/milestones?access_token=xxxx' \
--header 'Content-Type: application/json' \
--data-raw '{
    "title": "title",
    "due_on": "2025-01-01"
}'
```
