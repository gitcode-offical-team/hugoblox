---
linkTitle: Organizations
title: 组织接口文档
weight: 3
sidebar:
  open: false
---

## 1. 列出用户所属的组织

### 请求

`GET https://api.gitcode.com/api/v5/users/{username}/orgs`

### 参数

| 参数名         | 描述                            | 类型  | 数据类型 |
| -------------- | ------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                      | query | string   |
| username\*     | 用户名(username/login)          | path  | string   |
| page           | 当前的页码:默认为 1             | query | string   |
| per_page       | 每页的数量，默认为20，最大为100 | query | string   |

### 响应

```json
[
  {
    "id": 133039,
    "login": "openharmony",
    "name": "OpenHarmony",
    "avatar_url": null,
    "repos_url": null,
    "events_url": null,
    "members_url": null,
    "description": "OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。",
    "follow_count": 3
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/users/user1/orgs?access_token=xxx'
```

## 2. 列出授权用户所属的组织

### 请求

`GET https://api.gitcode.com/api/v5/users/orgs`

### 参数

| 参数名         | 描述                            | 类型  | 数据类型 |
| -------------- | ------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                      | query | string   |
| page           | 当前的页码:默认为 1             | query | string   |
| per_page       | 每页的数量，默认为20，最大为100 | query | string   |
| admin          | 筛选有管理员权限                | query | boolean  |

### 响应

```json
[
  {
    "id": 133039,
    "login": "openharmony",
    "path": "openharmony",
    "name": "OpenHarmony",
    "avatar_url": null,
    "repos_url": null,
    "events_url": null,
    "members_url": null,
    "description": "OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。",
    "follow_count": 3
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/users/orgs?access_token=xxx'
```

## 3. 获取组织成员详情

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}/members/{username}`

### 参数

| 参数名         | 描述                   | 类型  | 数据类型 |
| -------------- | ---------------------- | ----- | -------- |
| access_token\* | 用户授权码             | query | string   |
| org\*          | 组织的路径(path/login) | path  | string   |
| username\*     | 用户名(username/login) | path  | string   |

### 响应

```json
{
  "id": 133039,
  "path": "openharmony",
  "name": "",
  "url": "",
  "avatar_url": null,
  "user": {
    "id": "64dc3b13b8b9504cec223ab1",
    "login": "theo6789",
    "name": "TheoCui",
    "avatar_url": null,
    "html_url": "https://gitcode.com/theo6789"
  }
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/members/xiapgang?access_token=xxx'
```

## 4. 获取一个组织信息

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}`

### 参数

| 参数名         | 描述                   | 类型  | 数据类型 |
| -------------- | ---------------------- | ----- | -------- |
| access_token\* | 用户授权码             | query | string   |
| org\*          | 组织的路径(path/login) | path  | string   |

### 响应

```json
{
  "id": 6486504,
  "login": "openharmony",
  "name": "OpenHarmony",
  "avatar_url": "",
  "repos_url": "https://api.gitcode.com/openharmony/repos",
  "events_url": "https://api.gitcode.com/openharmony/events",
  "members_url": "https://api.gitcode.com/openharmony/members{/member}",
  "description": "OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。\r\n",
  "enterprise": "openharmony",
  "follow_count": 40819,
  "gitee": {
    "follows": 43454
  }
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/orgs/xiaogang_test?access_token=xxx'
```

## 5. 获取组织项目列表

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}/repos`

### 参数

| 参数名         | 描述                                                   | 类型  | 数据类型 |
| -------------- | ------------------------------------------------------ | ----- | -------- |
| access_token\* | 用户授权码                                             | query | string   |
| org\*          | 组织的路径(path/login)                                 | path  | string   |
| type           | 筛选仓库的类型，可以是 all, public, private。默认: all | query | string   |
| page           | 当前的页码:默认为 1                                    | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20                        | query | int      |

### 响应

```json
[
  {
    "id": 29724198,
    "full_name": "openharmony/.gitee",
    "namespace": {
      "id": 6486504,
      "type": "group",
      "name": "OpenHarmony",
      "path": "openharmony",
      "html_url": "https://gitcode.com/openharmony"
    },
    "path": ".gitee",
    "name": ".gitee",
    "description": "",
    "private": false,
    "public": true,
    "internal": false,
    "fork": false,
    "html_url": "https://gitcode.com/openharmony/.gitee.git",
    "forks_count": 4,
    "stargazers_count": 0,
    "watchers_count": 1,
    "default_branch": "master",
    "open_issues_count": 0,
    "license": null,
    "project_creator": "landwind",
    "pushed_at": "2024-02-06T18:25:26+08:00",
    "created_at": "2023-06-16T10:55:42+08:00",
    "updated_at": "2024-03-29T14:59:46+08:00",
    "status": "开始"
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/repos?access_token=xxx'
```

## 6. 创建组织仓库

### 请求

`POST https://api.gitcode.com/api/v5/orgs/{org}/repos`

| 参数名             | 描述                                                                             | 类型     | 数据类型 |
| ------------------ | -------------------------------------------------------------------------------- | -------- | -------- |
| access_token\*     | 用户授权码                                                                       | query    | string   |
| org\*              | 组织的路径(path/login)                                                           | path     | string   |
| name\*             | 仓库名称                                                                         | body     | string   |
| description        | 仓库描述                                                                         | body     | string   |
| homepage           | 主页                                                                             | body     | string   |
| has_issues         | 允许提Issue与否。默认: 允许(true)                                                | body     | boolean  |
| has_wiki           | 提供Wiki与否。默认: 提供(true)                                                   | body     | boolean  |
| can_comment        | 允许用户对仓库进行评论。默认： 允许(true)                                        | body     | boolean  |
| public             | 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)，注：与private互斥，以public为主 | body     | int      |
| private            | 仓库公开或私有。默认: 公开(false)，注：与public互斥，以public为主。              | body     | boolean  |
| auto_init          | 值为true时则会用README初始化仓库。默认: 不初始化(false)                          | body     | boolean  |
| gitignore_template | Git Ignore模版                                                                   | body     | string   |
| license_template   | License模版                                                                      | body     | string   |
| path               | 仓库路径                                                                         | body     | string   |
| default_branch     | 初始化项目时的默认分支名称。默认: main                                           | formData | string   |

### 响应

```json
{
  "id": 34171993,
  "full_name": "daming_1/test_create_project_2",
  "human_name": "daming/test_create_project_2",
  "url": "https://gitcode.com/api/v5/repos/daming_1/test_create_project_2",

  "path": "test_create_project_2",
  "name": "test_create_project_2",

  "description": "描述",
  "private": false,
  "public": true,
  "namespace": {
    "id": 74962,
    "name": "group1111",
    "path": "group11111",
    "develop_mode": "normal",
    "region": null,
    "cell": "default",
    "kind": "group",
    "full_path": "group11111",
    "full_name": "group1111",
    "parent_id": null,
    "visibility_level": 20,
    "enable_file_control": false,
    "owner_id": null
  },
  "empty_repo": null,
  "starred": null,
  "visibility": "public",
  "owner": null,
  "creator": null,
  "forked_from_project": null,
  "item_type": null,
  "main_repository_language": null,
  "homepage": "http://www.baidi.com"
}
```

### Demo

```bash
curl --location -g --request POST 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/repos?access_token=xxx'
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "test"
}'
```

## 7. 获取企业的一个成员

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/members/{username}`

### 参数

| 参数名         | 描述                   | 类型  | 数据类型 |
| -------------- | ---------------------- | ----- | -------- |
| access_token\* | 用户授权码             | query | string   |
| enterprise\*   | 企业的路径(path/login) | path  | string   |
| username\*     | 用户名(username/login) | path  | string   |

### 响应

```json
{
  "user": {
    "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
    "html_url": "https://gitcode.com/dengmengmian",
    "id": "268",
    "login": "dengmengmian",
    "name": "dengmengmian"
  },
  "url": "https://gitcode.com/dengmengmian",
  "active": true,
  "role": "admin",
  "enterprise": {
    "id": 0,
    "url": "https://gitcode.com/dengmengmian"
  }
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/enterprises/go-tribe/members/dengmengmian?access_token=xxx'
```

## 8. 获取授权用户在一个组织的成员资料

### 请求

`GET https://api.gitcode.com/api/v5/user/memberships/orgs/{org}`

### 参数

| 参数名         | 描述                   | 类型  | 数据类型 |
| -------------- | ---------------------- | ----- | -------- |
| access_token\* | 用户授权码             | query | string   |
| org\*          | 企业的路径(path/login) | path  | string   |

### 响应

```json
{
  "id": 1783195,
  "path": "Go-Tribe",
  "name": "gotribe",
  "url": "https://gitcode.com/Go-Tribe",
  "avatar_url": "https://cdn-img.gitcode.com/bb/eb/b3b4e25b54add3c80961d3ba2e3724d03998eae467c99ab898ea39e48cb1b4f6.png?time1717675394237",
  "user": {
    "id": "650d67fbae6d795139b49b41",
    "login": "dengmengmian",
    "name": "麻凡",
    "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
    "html_url": "https://gitcode.com/dengmengmian"
  },
  "active": true,
  "role": "admin",
  "organization": {
    "id": 1783195,
    "login": "Go-Tribe",
    "name": "gotribe"
  }
}
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/user/memberships/orgs/go-tribe?access_token=xxx'
```

## 9. 列出一个组织的所有成员

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}/members`

### 参数

| 参数名         | 描述                               | 类型  | 数据类型 |
| -------------- | ---------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                         | query | string   |
| org\*          | 企业的路径(path/login)             | path  | string   |
| page           | 当前的页码:默认为 1                | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20    | query | int      |
| role           | 根据角色筛选成员(all/admin/member) | query | string   |

### 响应

```json
[
  {
    "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
    "followers_url": "https://api.gitcode.com/api/v5users/dengmengmian/followers",
    "html_url": "https://gitcode.com/dengmengmian",
    "id": "268",
    "login": "dengmengmian",
    "member_role": "admin",
    "name": "麻凡",
    "type": "User",
    "url": "https://api.gitcode.com/api/v5/dengmengmian"
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/orgs/go-tribe/members?access_token=xxxx&page=1&pre_page=10&role=all'
```

## 10. 列出企业的所有成员

### 请求

`GET https://api.gitcode.com/api/v5/enterprises/{enterprise}/members`

### 参数

| 参数名         | 描述                               | 类型  | 数据类型 |
| -------------- | ---------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                         | query | string   |
| org\*          | 企业的路径(path/login)             | path  | string   |
| page           | 当前的页码:默认为 1                | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20    | query | int      |
| role           | 根据角色筛选成员(all/admin/member) | query | string   |

### 响应

```json
[
  {
    "user": {
      "avatar_url": "https://cdn-img.gitcode.com/ec/fb/430ecf07b9ee91bbbbf341d92a36783d06e69086f82ce8cf5a6406f79f1c9cf4.png",
      "html_url": "https://gitcode.com/dengmengmian",
      "id": "268",
      "login": "dengmengmian",
      "name": "麻凡",
      "url": "https://api.gitcode.com/api/v5/dengmengmian"
    },
    "url": "https://api.gitcode.com/api/v5/enterprises/go-tribe/members/dengmengmian",
    "active": true,
    "role": "admin"
  }
]
```

### Demo

```bash
curl --location -g --request GET 'https://api.gitcode.com/api/v5/enterprises/go-tribe/members?access_token=yuBy&page=1&pre_page=10&role=all'
```

## 11. 移除授权用户所管理组织中的成员

### 请求

`DELETE https://api.gitcode.com/api/v5/orgs/{org}/memberships/{username}`

### 参数

| 参数名         | 描述                   | 类型  | 数据类型 |
| -------------- | ---------------------- | ----- | -------- |
| access_token\* | 用户授权码             | query | string   |
| org\*          | 组织的路径(path/login) | path  | string   |
| username\*     | 用户名(username/login) | path  | string   |

### 响应

```json
{}
```

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/orgs/tiandi/memberships/yinlin?access_token=******'
```

## 12. 列出指定组织的所有关注者

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{owner}/followers`

### 参数

| 参数名         | 描述                                         | 类型  | 数据类型 |
| -------------- | -------------------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                                   | query | string   |
| owner\*        | 仓库所属空间地址(企业、组织或个人的地址path) | path  | string   |
| page           | 当前的页码:默认为 1                          | query | int      |
| per_page       | 每页的数量，最大为 100，默认 20              | query | int      |

### 响应

```json
[
  {
    "id": 496,
    "login": "xiaogang",
    "name": "xiaogang",
    "avatar_url": "https://gitcode-img.obs.cn-south-1.myhuaweicloud.com:443/bc/cd/6bc422546cdf276c147f267030d83a43e927fec67ca66f0b22f7e03556206fa3.jpg",
    "watch_at": "2024-11-13T16:15:53.287+08:00"
  }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/followers' \
--header 'PRIVATE-TOKEN: your_token'
```

## 13. 获取 issue 扩展配置

### 请求

`GET https://api.gitcode.com/api/v5/orgs/{org}/issue/extend/settings`

### 参数

| 参数名         | 描述                              | 类型  | 数据类型 |
| -------------- | --------------------------------- | ----- | -------- |
| access_token\* | 用户授权码                        | query | string   |
| org\*          | 仓库所属空间地址(企业、组织 path) | path  | string   |

### 响应

```json
[
    {
        "type_name": "需求",
        "type_id": 4,
        "type_desc": "定义和描述产品或项目中需要实现的新功能或改进",
        "status": [
            {
                "status_name": "未提单",
                "status_id": 11,
                "status_desc": "需求尚未正式提交，仍处于概念或讨论阶段，未进入系统管理流程",
                "gitcode_issue_status": 0
            },
            ... ...
            {
                "status_name": "修复中",
                "status_id": 20,
                "status_desc": "需求的解决方案在实施过程中发现问题，正在进行修复工作",
                "gitcode_issue_status": 0
            }
        ]
    },
    ... ...
    {
        "type_name": "咨询",
        "type_id": 36138,
        "type_desc": "",
        "status": [
            {
                "status_name": "未提单",
                "status_id": 11,
                "status_desc": "需求尚未正式提交，仍处于概念或讨论阶段，未进入系统管理流程",
                "gitcode_issue_status": 0
            },
            ... ...
            {
                "status_name": "已完成",
                "status_id": 14,
                "status_desc": "需求的所有相关工作已结束，成果已交付，进入归档状态",
                "gitcode_issue_status": 1
            }
        ]
    }
]
```

### Demo

```bash
curl --location 'https://api.gitcode.com/api/v5/orgs/openharmony/issue/extend/settings?access_token=your_token'
```

## 14. 邀请组织成员

### 请求

`POST https://api.gitcode.com/api/v5/orgs/{org}/memberships/{username}`

### 参数

| 参数名         | 描述                                                               | 类型  | 数据类型 |
| -------------- |------------------------------------------------------------------| ----- | -------- |
| access_token\* | 用户授权码                                                            | query | string   |
| org\*          | 仓库所属空间地址(企业、组织或个人的地址path)                                        | path  | string   |
| username\*     | 用户名(username/login)                                              | path  | string   |
| permission     | 成员权限: 拉代码(pull)，推代码(push)，维护者(admin)。默认: push, customized(自定义角色) | body  | string   |
| role_id        | 角色ID, 如果permission传入了customized则需要传入角色id                         | body  | string   |

### 响应

```json
{
  "followers_url": "https://api.gitcode.com/api/v5/users/xiaogang2/followers",
  "html_url": "https://gitcode.com/xiaogang2",
  "id": "65ffca965079ba0d1c00f6f2",
  "login": "xiaogang2",
  "name": "肖刚2",
  "type": "User",
  "url": "https://api.gitcode.com/api/v5/xiaogang2",
  "permissions": {
    "admin": false,
    "customized": true,
    "push": true,
    "pull": true
  }
}
```

### Demo

```bash
curl --location POST 'https://api.gitcode.com/api/v5/orgs/xiaogang_test/memberships/xiaogang2?access_toke=?' \
--data '{
    "permission":"push"
    }'
```


## 15. 修改企业成员权限

### 请求

`PUT https://api.gitcode.com/api/v5/enterprises/{enterprise}/members/{username}`

### 参数

| 参数名            | 描述                                                                | 类型  | 数据类型 |
|----------------|-------------------------------------------------------------------| ----- | -------- |
| access_token\* | 用户授权码                                                             | query | string   |
| enterprise\*   | 仓库所属空间地址(企业、组织或个人的地址path)                                         | path  | string   |
| username\*     | 用户名(username/login)                                               | path  | string   |
| role\*         | 企业角色(viewer:浏览者、tester:测试人、developer:研发、maintainer:维护者、admin:管理员) | body  | string   |

### 响应

```json

{
    "active": true,
    "role": "member",
    "url": "https://api.gitcode.com/api/v5/enterprises/litestabc/members/malongge5",
    "user": {
        "id": 953,
        "login": "malongge5",
        "url": "https://api.gitcode.com/api/v5/malongge5",
        "html_url": "https://gitcode.com/malongge5"
    }
}
```

### Demo

```bash
curl --location --request PUT 'https://api.gitcode.com/api/v5/enterprises/litestabc/members/malongge5?access_token=?' \
--header 'Content-Type: application/json' \
--data-raw '{"role":"developer"}'
```

## 16. 更新授权用户所管理的组织资料

### 请求

`PATCH https://api.gitcode.com/api/v5/orgs/{org}?access_toke=?`

### 参数

| 参数名            | 描述                                                                | 类型  | 数据类型 |
|----------------|-------------------------------------------------------------------| ----- | -------- |
| access_token\* | 用户授权码                                                             | query | string   |
| org\*   | 组织的路径(path/login)                                        | path  | string   |
| name   | 组织名称                                        | form-data  | string   |
| email   | 	组织公开的邮箱地址                                        | form-data  | string   |
| location   | 组织所在地                                        | form-data  | string   |
| description   | 组织简介                                        | form-data  | string   |
| html_url   | 组织站点                                        | form-data  | string   |

### 响应

```json

{
    "email": "123@qq.com",
    "name": "组织",
    "description": "233333",
    "html_url": "www.baidu.com",
    "id": 138108,
    "path": "xiaogang444
}
```

#### 响应字段说明

| 字段                             | 类型    | 说明                                          |
| -----------------------------   | ------- | --------------------------------------------- |
| `id`                        | integer | 组织 Id                        |
| `path`                      | string  | 组织的路径                      |
| `name`                      | string  | 组织名称                       |
| `email`                     | string | 	组织公开的邮箱地址               |
| `location`                  | string| 组织所在地                        |
| `description`               | string   | 组织简介                       |
| `html_url`                  | string | 组织站点                         |

### Demo

```bash
curl --location --request PATCH 'https://api.gitcode.com/api/v5/orgs/test444?access_token=?' \
--form 'name="组织"'
```

## 17. 退出一个组织

### 请求

`DELETE https://api.gitcode.com/api/v5/user/memberships/orgs/{org}?access_toke=?`

### 参数

| 参数名            | 描述                                                                | 类型  | 数据类型 |
|----------------|-------------------------------------------------------------------| ----- | -------- |
| access_token\* | 用户授权码                                                             | query | string   |
| org\*   | 组织的路径(path/login)                                        | path  | string   |

### 响应
无

### Demo

```bash
curl --location --request DELETE 'https://api.gitcode.com/api/v5/user/memberships/orgs/xiaogang_test?access_token=?' \
```
