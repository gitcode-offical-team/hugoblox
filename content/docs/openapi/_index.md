---
linkTitle: OpenAPI
title: OpenAPI
weight: 10
sidebar:
  open: false
---

{{< cards >}}
  {{< card url="guide" title="使用入门" icon="cursor-arrow-ripple" >}}
  {{< card url="users" title="用户账号 Users" icon="user-circle" >}}
  {{< card url="orgs" title="组织 Organizations" icon="user-group" >}}
  {{< card url="repos/" title="仓库 Repositories" icon="briefcase" >}}
  {{< card url="repos/issues" title="任务 Issues" icon="question-mark-circle" >}}
  {{< card url="repos/pulls" title="Pull Requests" icon="arrow-path-rounded-square" >}}
  {{< card url="repos/labels" title="Labels" icon="tag" >}}
  {{< card url="repos/milestons" title="里程碑" icon="map" >}}
  {{< card url="repos/webhooks" title="Webhooks" icon="share" >}}
  {{< card url="oauth" title="OAuth2.0" icon="key" >}}
{{< /cards >}}
