module github.com/HugoBlox/theme-documentation

go 1.15

require (
	github.com/HugoBlox/hugo-blox-builder/modules/blox-plugin-netlify v1.1.2-0.20241104155618-a0f0fcf6bcbe
	github.com/HugoBlox/hugo-blox-builder/modules/blox-tailwind v0.3.1 // indirect
)
replace github.com/HugoBlox/hugo-blox-builder/modules/blox-tailwind => github.com/dumuchenglin123/hugo-blox-builder/modules/blox-tailwind v1.9.4